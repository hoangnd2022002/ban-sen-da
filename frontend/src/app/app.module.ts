import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule } from '@modules/auth/auth.module';
import * as  AuthGuard  from '@modules/auth/guards';
import { DashboardModule } from '@modules/dashboard/dashboard.module';
import { InterceptorHTTPModule } from '@modules/Interceptor/interceoptor.module';
import { LayoutModule } from '@modules/layout/layout.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule,InterceptorHTTPModule,LayoutModule,DashboardModule,AuthModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
