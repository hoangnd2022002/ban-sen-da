import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService, TokenService } from '@modules/auth/services';
import { RoleService } from '@modules/layout/service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class DashboardGuard implements CanActivate {
    constructor(
        public authService : AuthService,
        public tokenSevice : TokenService,
        public roleSv:RoleService,
        public route: Router,
        public http :  HttpClient
    ){};
    canActivate(): Observable<boolean> {
        if(!this.authService.isLogged() ){
            
            this.route.navigate(['/auth/login'])
            return of(false)
        }
        console.log(this.roleSv.isNhanVien+ "||" +this.roleSv.isQuanLy);
        
        if(this.roleSv.isNhanVien || this.roleSv.isQuanLy){
            this.http.get("api/product/free/get-expiration-token")
            .subscribe(
                data => {
                    console.log("auth");
                    
                    console.log(data);
                    if(!data) {
                        this.tokenSevice.deleteToken()
                        this.route.navigate(['/auth/login'])
                    }
                    return of(true)
                }
            )
                
        }else{
            this.route.navigate(['/home'])
            return of(false)
        }
        
        return of(true)
    }
}
