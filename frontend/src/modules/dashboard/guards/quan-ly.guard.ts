import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService, TokenService } from '@modules/auth/services';
import { RoleService } from '@modules/layout/service';
import { Observable, of } from 'rxjs';

@Injectable()
export class QuanLydGuard implements CanActivate {
    constructor(
        public authService : AuthService,
        public roleSv:RoleService,
        public route: Router,
        public http :  HttpClient
    ){};
    canActivate(): Observable<boolean> {
        if( this.roleSv.isQuanLy){            // this.route.navigate(['/error/404'])
            return of(true)
        }else{
            this.route.navigate(['/admin/ban-hang'])
            return of(false)
        }
        
        // return of(true)
    }
}
