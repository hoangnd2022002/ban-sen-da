import { DashboardGuard } from './dashboard.guard';
import { QuanLydGuard } from './quan-ly.guard';

export const guards = [DashboardGuard,QuanLydGuard];

export * from './dashboard.guard';
export * from './quan-ly.guard';
