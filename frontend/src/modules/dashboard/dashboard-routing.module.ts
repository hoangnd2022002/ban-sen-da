/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { DashboardModule } from './dashboard.module';

/* Containers */
import * as dashboardContainers from './containers';

/* Guards */
import * as dashboardGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'Thống kê',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard,],
        component: dashboardContainers.DashboardComponent,
    },
    {
        path: 'product',
        data: {
            title: 'Sản phẩm',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Sản phẩm',
                    active: true,
                    
                },
                
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard],
        component: dashboardContainers.ProductComponent,
    },
    {
        path: 'kiem-duyet/chi-tiet-don/:id',
        data: {
            title: 'Chi tiết đơn hàng',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },{
                    text:"Kiểm duyệt",
                    link:"/admin/kiem-duyet",
                    // active:true
                },{
                    text:"Chi tiết đơn hàng",
                    // link:"/admin/product",
                    active:true
                }
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard],
        component: dashboardContainers.InfoOrder,
    },
    {
        path: 'product/add-product',
        data: {
            title: 'Chi tiết đơn hàng',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },{
                    text:"Sản phẩm",
                    link:"/admin/product",
                    // active:true
                },{
                    text:"Thêm sản phẩm",
                    // link:"/admin/product",
                    active:true
                }
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.AddProductComponent,
    },
    {
        path: 'contact',
        data: {
            title: 'Liên hệ',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },{
                    text:"Liên hệ",
                    // link:"/admin/product",
                    active:true
                }
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard],
        component: dashboardContainers.ContactComponent,
    },
    {
        path: 'product/edit-product/:id',
        data: {
            title: 'Sửa sản phẩm',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },{
                    text:"Sản phẩm",
                    link:"/admin/product",
                    // active:true
                },{
                    text:"Sửa sản phẩm",
                    // link:"/admin/product",
                    active:true
                }
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.AddProductComponent,
    },
    {
        path: 'product/edit-product/:ban-lai/:id',
        data: {
            title: 'Sửa sản phẩm',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },{
                    text:"Sản phẩm",
                    link:"/admin/product",
                    // active:true
                },{
                    text:"Sửa sản phẩm",
                    // link:"/admin/product",
                    active:true
                }
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.AddProductComponent,
    },
    {
        path: 'product/info-product/:id',
        data: {
            title: 'Chi tiết sản phẩm',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },{
                    text:"Sản phẩm",
                    link:"/admin/product",
                    // active:true
                },{
                    text:"Chi tiết sản phẩm",
                    // link:"/admin/product",
                    active:true
                }
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.InfoProductAdmin,
    },
    {
        path: 'ban-hang',
        data: {
            title: 'Bán hàng',
            breadcrumbs: [
                {
                    text: 'Bán Hàng',
                    // link: '/admin',
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard],
        component: dashboardContainers.BanHangComponent,
    },
    {
        path: 'user',
        data: {
            title: 'Người dùng',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Người dùng',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.UserComponent,
    },
    {
        path: 'category',
        data: {
            title: 'Thể loại',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Thể loại',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.CategoryComponent,
    },
    {
        path: 'size',
        data: {
            title: 'Kích thước',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Kích thước',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.SizeComponent,
    },
    {
        path: 'color',
        data: {
            title: 'Màu sắc',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Kích thước',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.ColorComponent,
    },
    {
        path: 'habitat',
        data: {
            title: 'Môi trường sống',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Môi trường sống',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.HabitatComponent,
    },
    {
        path: 'light-aspect',
        data: {
            title: 'Ánh sáng phù hợp',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Ánh sáng phù hợp',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard,dashboardGuards.QuanLydGuard],
        component: dashboardContainers.LightAspectComponent,
    },
    {
        path: 'hoa-don',
        data: {
            title: 'Hóa đơn ',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    link: '/admin',
                },
                {
                    text: 'Hóa đơn',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard],
        component: dashboardContainers.HoaDonComponent,
    },
    {
        path: 'kiem-duyet',
        data: {
            title: 'Kiểm duyệt đơn ',
            breadcrumbs: [
              
                {
                    text: 'Hóa đơn',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [dashboardGuards.DashboardGuard],
        component: dashboardContainers.KiemDuyetComponent,
    },
];

@NgModule({
    imports: [DashboardModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {}
