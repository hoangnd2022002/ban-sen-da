import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-product',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './user.component.html',
    styleUrls: ['user.component.scss'],
})
export class UserComponent implements OnInit {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient
        ) {}
    ngOnInit() {
        this.getRoles()
    }

    public formGroup = this.formBuilder.group({
        idUser:[0],
        name:["",[Validators.required,Validators.pattern("[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ]*")]],
        username:["",[Validators.required,Validators.pattern("[a-zA-Z0-9]*"),Validators.minLength(5),Validators.maxLength(15)]],
        phone:["",[Validators.required,Validators.pattern("[0]{1}[1-9]{8,10}")]],
        email:["",[Validators.required,]],
        sex:[0,[Validators.required]],
        password:["",[Validators.required,Validators.minLength(5),Validators.maxLength(15)]],
        role:[{}],
    })
    public pageNumber : number = 0
    public pageMethod : string ="getUsers"
    public errorAPI : string = ""
    public actionEdit = false
    public role : any= {}
    public loading = true
    public alertTB: any = {show:false,msg:""}
    public tabSelect = 0;
    public alert: any={status:false,index:-1}
    public submited = false;
    public datasUser : any = {content:[]}
    public datasRole : any = []
    public deleteUser : any= {}
    public editPass = false
    public txtSearch : string = ""
    public roleId : any
    get errorsForm(){
        return this.formGroup.controls
    }
    
    editForm(data : any){
        this.formGroup.patchValue(data)
        for(let obj of this.datasRole){
            if(obj.idRole === data?.role.idRole)
            this.formGroup.controls['role'] .setValue(obj)
        }
        this.roleId = this.formGroup.get('role')?.value?.idRole
        this.editPass = false
        this.formGroup.removeControl("password")
        this.actionEdit = true
    }
    delete(){
        console.log(JSON.stringify(this.deleteUser))
        this.http.post<Object[]>(`api/user/delete-user`,this.deleteUser)
        .subscribe(
            datas => {
                this.datasUser = datas
                console.log(datas);
                this.search(this.tabSelect,this.role);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Đã xóa người dùng ",
                    showConfirmButton: false,
                    timer: 1500
                  })
            }
        )
    }
    onSubmit(closeModal : any){
        this.pageMethod="getUsers"
        this.submited = true
        if(this.validated()){
            this.loading = true
            var data = Object.assign(this.formGroup.value)
            var api = `api/user/save-user`
            var msg = "Thêm mới người dùng thành công"
            if(this.actionEdit){
                msg = "Sửa thành công"
                api = `api/user/edit-user`
            }
            else data.role = this.role
            console.log(this.formGroup.value);
            console.log(JSON.stringify(this.formGroup.value));
            this.http.post<any>(api,data)
            .subscribe(
                datas => {
                    this.loading = false
                    console.log(datas);
                    if(datas?.data) {
                        closeModal.click()
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: msg,
                            showConfirmButton: false,
                            timer: 1500
                          })
                        this.search(this.tabSelect,this.role);
                        this.errorAPI = ""
                        this.checked = false
                    }else {
                        this.errorAPI = datas.message
                    }
                }
            )
        }
    }
    showMsg(msg : string){
        this.alertTB.show = true
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.show = false
            this.alertTB.msg = ""
        },5000)
    }
    alertDeleteUser(user : Object){
        this.deleteUser = user
    }
    // getUsersByRole(index : number,role : any){
    //     this.loading = true
    //     this.role =  Object.assign({},role)
    //     this.tabSelect = index
    //     this.http.post<Object[]>(`api/user/get-user-by-role?currentPage=${this.pageNumber}`,role)
    //     .subscribe(
    //         datas => {
    //             this.loading = false
    //             this.datasUser = datas
    //             console.log(datas);
    //         }
    //     )
    // }
    getRoles(){
        this.http.get<any>(`api/user/get-all-role`)
        .subscribe(
            datas => {
                this.datasRole = datas
                console.log(datas);
                this.search(0,datas[0])
            }
        )
    }
    public checked= false;
    resetForm(){
        this.checked= false
        this.formGroup.reset()
        this.actionEdit = false
        this.submited = false
        this.editPassword(null)
    }
    editPassword(input : any ){
        if(input) {
            if(!input.checked) {
                this.editPass =  false 
                this.formGroup.removeControl("password")
                return
            }
        }
        this.editPass = true
        this.formGroup.addControl("password",new FormControl("",[Validators.required,Validators.minLength(5),Validators.maxLength(15)]))
    }
    validated() : boolean{
        return !(
            this.errorsForm?.name?.errors || 
            this.errorsForm?.username?.errors || 
            this.errorsForm?.phone?.errors || 
            this.errorsForm?.email?.errors ||
            this.errorsForm?.password?.errors ||
            this.errorsForm?.sex?.errors
            )
    }
    nextPage(input:any ,totalPages : number ):boolean{
        if(this.pageNumber >= totalPages - 1 )return false
        else this.pageNumber = this.pageNumber*1 + 1
        console.log(this);
        return true
    }
    prePage(input:any ,totalPages : number):boolean{
        if(this.pageNumber - 1 < 0 )return false
        else this.pageNumber = this.pageNumber*1 - 1
        return true
    }
    getPage(input:any ,totalPages : number):boolean{
        if( input.value*1 > totalPages  ||  input.value*1  <= 0   )  {
            input.classList.add("class" , "outline-danger");
            input.value = this.pageNumber*1 + 1
            return false
        }
        else this.pageNumber = input.value - 1
        input.classList.remove("outline-danger");
        return true
    }
    actionPage(callbackActionPage : boolean  ,input : string){
        if(callbackActionPage ) {
            if(this.pageMethod === "getUsers") this.search(this.tabSelect,this.role);
            if(this.pageMethod === "search" ) this.search(this.tabSelect,this.role)
        }
    }
    search(index : number,role : any){
        this.tabSelect = index
        if(index !== this.tabSelect){
            this.pageNumber = 0
        }
        this.loading = true
        this.role =  Object.assign({},role)
        this.http.post<any>(`api/user/get-all-user-by-keyword?currentPage=${this.pageNumber}`,{keyword: this.txtSearch,role:this.role.idRole})
        .subscribe(
            datas => {
                this.datasUser = datas
                this.loading = false
            },
            err => this.loading = false
        )
    }

}
