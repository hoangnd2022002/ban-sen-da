import { HttpClient } from '@angular/common/http';
import {  Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { url } from '@modules/domain';
// import {url}  from 'environments/environment';
import Swal from 'sweetalert2';


@Component({
    selector: 'sb-product',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './product.component.html',
    styleUrls: ['product.component.scss'],
})
export class ProductComponent implements OnInit {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient,
        public router:Router
        ) {}
    formGroup = this.formBuilder.group({
        idProduct:[""],
        productName:["",[Validators.required,Validators.minLength(5),Validators.maxLength(225)]],
        category:[{}, [Validators.required,]],
        moTa:[""],
        productSizeList:[[],[Validators.required]],
    })
    public productSize : any  = []
    public tab = 1
    public pageNumber : number = 0
    public pageMethod : any ="getProducts";
    public img = ""
    public errorAPI :string  = ""
    public alertTB :any = {show: false, msg:""}
    public deleteProDuct:any = {}
    public loading = true
    public loadingAddProduct = false
    public actionEdit = false
    public fileImages : any[] = []
    public showImage : string[]  = []
    public file : any 
    public alert : any= {index:-1, idImage : -1}
    public submited = false
    public categoryDatas : any = {content:[{}]}
    public datas : any ={}
    public listColorChoose : any = []
    public listSizeChoose : any =[]
    public error = false
    get getFormControl(){
        return this.formGroup.controls
    }
    get productSize_model() {
        return Object.assign({},{
            idProductSize:"",
            quantity : 0,
            price : 0,
            color : {},
            product : {},
            size : {},
            // images : "image"
        })
    }

   
    getProductQuantity0(){
        if(this.tab !=2) this.pageNumber = 0
        this.tab = 2
        this.http.get(`api/product/free/get-all-product-het-so-luong?currentPage=${this.pageNumber}&name=${this.txtSearch}`)
        .subscribe(
            data=>{
                this.datas = data
            }
        )
    }
    
    addSizeAndPrice(color : any,productSizearr : any[]){
        let productSize = this.productSize_model
        productSize.size = this.sizeNotChoose(color)
        if(!productSize.size) return
        productSize.color = color
        productSize.quantity = 1
        productSize.price = 1000
        productSizearr.push(productSize)
    }
    sizeNotChoose(color : any){
        let productSize=[]
        for(let cl of this.productSize){
            if(cl.color === color) productSize = cl.productSize
        }
        if(productSize.length < 1) return this.getFormControl['category'].value.sizeList[0]
        let ids = productSize.map((prds:any) => prds.size.idSize)
        for(let size of this.getFormControl['category'].value.sizeList ){
            if(!ids.includes(size.idSize)) {
                console.log(4);
                return size
            }
        }
    }
    colorNotChoose(){
        let ids = this.productSize.map((cl:any) => cl.color.id)
        for(let color of this.getFormControl['category'].value.colorList ){
            if(!ids.includes(color.id)) {
                console.log(4);
                return color
            }
        }
    }
    isSize(size :any,color : any ) : boolean{
        let productSize=[]
        for(let cl of this.productSize){
            if(cl.color === color) productSize = cl.productSize
        }
        for(let prds of productSize) {
            if(prds.size.idSize === size.idSize ) return true
        }
        return false
    }
    isColor(color :any,productSize : any ) : boolean{
        for(let prds of productSize) {
            if(prds.color === color ) return true
        }
        return false
    }
    setColor( productSize :any ){
        productSize.productSize.forEach(
            (prds:any) =>{ prds.color = productSize.color }
        )
    }
    setProductSize(productSizeLst : Array<any>){
        this.getFormControl['category'].value.colorList.forEach((color:any)=>{
            let productSize = [""]
            productSize = []
            productSizeLst.forEach( (prds:any)=>{
                if(color.id === prds.color.id) productSize.push(prds)
            })
            if(productSize.length > 0)
            this.productSize.push({
                color:color,
                productSize:productSize
            })
        })
            
    }
    setPrice(productSize : any , input : any, tbErrror  : any){
        let value = input.value 
        console.log(value);
        
        if(value < 1 || value > 999999999 || !value)
        {
            input.classList.add("outline-danger");
            tbErrror.hidden = false
        }else{
            productSize.price = input.value
            input.classList.remove("outline-danger");
            tbErrror.hidden= true
        }
    }
    setQuantity(productSize : any, input :any,tbErrror : any){
        let value = input.value 
        console.log(value);
        
        if(value < 0 || value > 1000000 || !value)
        {
            input.classList.add("outline-danger");
            tbErrror.hidden = false
        }else{
            productSize.quantity = input.value
            input.classList.remove("outline-danger");
            tbErrror.hidden = true
        }
        
    }
    getProducts(){
        this.pageMethod= "getProducts"
        this.loading = true
        this.http.get<any>(`api/product/free/get-all-product1?currentPage=${this.pageNumber}`)
        .subscribe(
            datas => {
                this.datas = datas
                console.log(datas);

                this.loading = false
            }
        )
    }
    showMsg(msg : string){
        this.alertTB.show = true
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.show = false
            this.alertTB.msg = ""
        },5000)
    }
    alertDeleteProduct(prd :any){
        this.deleteProDuct = Object.assign(prd)
        // delete this.deleteProDuct.type
    }
    delete(){
        console.log(this.deleteProDuct);
        this.deleteProDuct.type =  this.deleteProDuct.type.idType
        this.deleteProDuct.lightAspect =  this.deleteProDuct.lightAspect.idLightAspect
        // this.
        this.http.post(`api/product/delete-product`,this.deleteProDuct )
        .subscribe(
            () => {
                this.search()
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Đã xóa sản phẩm',
                    showConfirmButton: false,
                    timer: 1500
                  })
            },
            err => console.log(err)
        )
    }
   
    public txtSearch=""
    search(){
        if(this.tab != 1) 
        this.pageNumber = 0
        this.tab = 1
        console.log(this.txtSearch);
        this.pageMethod="search"
        this.loading = true
        this.http.post(`api/product/free/get-all-product-by-keyword-and-cate?currentPage=${this.pageNumber}`,{keyword: this.txtSearch,idType: !this.category ? null : this.category.idType})
        .subscribe(
            datas => {
                this.datas = datas
                this.loading = false
                console.log(datas);
            },
            err => this.loading = false
        )
    }
    getNgungBan(){
        if(this.tab != 3) 
        this.pageNumber = 0
        this.tab = 3
        console.log(this.txtSearch);
        this.pageMethod="search"
        this.loading = true
        this.http.post(`api/product/free/get-all-product-ngung-ban-by-keyword-and-cate?currentPage=${this.pageNumber}`,{keyword: this.txtSearch,idType: !this.category ? null : this.category.idType })
        .subscribe(
            datas => {
                this.datas = datas
                this.loading = false
                console.log(datas);
            },
            err => this.loading = false
        )
    }
    fillterProductByCate(){
        this.pageNumber = 0
        if(this.tab == 1) this.search()
        if(this.tab == 2) this.getProductQuantity0()
        if(this.tab == 3 ) this.getNgungBan()
    }
    
    public category : any
    ngOnInit() {
        // get datas category 
        console.log("getCate");
        
        this.http.get<any>(`api/type/free/get-all-type1`)
        .subscribe(
            datas =>{
                this.categoryDatas = datas
                let id = localStorage.getItem("idType")
                console.log("id1",id);
                
                if(id)
                {   
                    console.log("id1",id); 
                    for( const type of datas.data) {
                        if(type.idType == id){
                            this.setCategory(type)
                            return
                        }
                    }
                    this.setCategory(null)
                }
                else {
                    this.setCategory(null)
                }
                
            },
            err => console.log(err)
        )
        // get datas product
        // this.getProducts()
        // this.fillterProductByCate()
    }
    setCategory(category: any){
        // console.log(category);
        this.pageNumber=0
        this.category = category 
        this.fillterProductByCate()
    }
   
    validated() : boolean{
        let errNumber = false
        let imgBoolean = false
        this.showImage.forEach((img:any)=>{
            if(!img) imgBoolean = true
        })
        console.log(imgBoolean);
        this.productSize.forEach((prds1:any)=>{
            prds1.productSize.forEach((prds2:any)=>{
                if(prds2.price < 0 || prds2.quantity <= 0){
                    errNumber  = true
                    this.error = true
                    return
                }
            })
        })
        console.log(errNumber);
        
        return !(
            this.getFormControl?.productName?.errors || 
            // this.getFormControl?.giaBan?.errors || 
            // this.getFormControl?.moTa?.errors
            this.getFormControl?.category?.errors || 
            imgBoolean || errNumber 
            )
    }
   
    deleteImage(){
        this.loading = true
        const index = this.alert.index
        console.log(this.alert);
            this.showImage[index] = ""
            this.fileImages[index] = null
            this.loading = false
        // this.alert.index = 
    }
    deleteProductSize(pros : any , arrPrds : any){
        for(let i = 0 ; i < arrPrds.length ; i++){
            if(arrPrds[i] === pros) arrPrds.splice(i,1)
        }
    }
    deleteColor(color : any , arrColor : any ){
        for(let i = 0 ; i < arrColor.length ; i++){
            if(arrColor[i] === color) arrColor.splice(i,1)
        }
    }
  
    nextPage(input:any ,totalPages : number ):boolean{
        if(this.pageNumber >= totalPages - 1 )return false
        else this.pageNumber = this.pageNumber*1 + 1
        console.log(this);
        return true
    }
    prePage(input:any ,totalPages : number):boolean{
        if(this.pageNumber - 1 < 0 )return false
        else this.pageNumber = this.pageNumber*1 - 1
        return true
    }
    getPage(input:any ,totalPages : number):boolean{
        if( input.value*1 > totalPages  ||  input.value*1  <= 0   )  {
            input.classList.add("class" , "outline-danger");
            input.value = this.pageNumber*1 + 1
            return false
        }
        else this.pageNumber = input.value - 1
        input.classList.remove("outline-danger");
        return true
    }
    actionPage(callbackActionPage : boolean  ,input : string){
        if(callbackActionPage ) {
            if(this.tab == 1) this.search()
            else if(this.tab == 2) this.getProductQuantity0()
            else if(this.tab == 3 ) this.getNgungBan()
        }
    }
    getImage(name : string){
        return `${url.domain}/api/product/free/image/${name}`;
    }
    get getListCateSon(){
        return this.formGroup.controls['categoryDad']?.value?.categorySonlst
    }
    
}
