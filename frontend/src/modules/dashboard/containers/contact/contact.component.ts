import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Count } from '@modules/layout/service';
import Swal from 'sweetalert2';
import { sideNavItems } from '@modules/navigation/data';

@Component({
    selector: 'contact',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './contact.component.html',
    styleUrls: ['contact.component.scss'],
})
export class ContactComponent implements OnInit {
    constructor(
        public http : HttpClient,
        public count:Count
    ) {}
    public orderDetails: any =[]
    public dataContact : any
    public alertTB : any= {showSuccess:false,showError:false,msg:""}
    ngOnInit( ) {
        // this.tabSelect = 4
        this.getContact(this.tab[0])
    }
    public contact :any = {}
    public loading = false
    public tab = [
        {id: 1,name:"Chưa phản hồi",api:{
            get:"api/contact/contact-chua-phan-hoi",
            post:"api/contact/phan-hoi",
            delete:"api/contact/tu-choi-phan-hoi"
        }},
        {id: 2,name:"Đã phản hồi",api:{
            get:"api/contact/contact-da-phan-hoi ",
            // post:"api/contact/dang-giao"
        }},
        {id: 3,name:"Từ chối phản hồi",api:{
            get:"api/contact/contact-tu-choi",
            // post:"api/contact/da-giao",
            // delete:"api/contact/huy-khi-dang-giao"
        }},
        
    ]
    public tabSelect : any;
    getContact(tabSelect : any){
        this.tabSelect = tabSelect
        this.http.get(tabSelect.api.get)
        .subscribe(
            datas=> {
                console.log(datas);
                console.log(tabSelect.api);
                this.dataContact = datas
            }
        )
    } 
    setContact(od:any ){
        this.contact = od
        this.submited = false 
        this.messageShop =""
    }
    public messageShop=""
    selected(tabSelect : any){
        this.getContact(tabSelect)
    }
    public submited= false
    postRequest(api : any,contact : any,closeMd : any){
        console.log(api);
        console.log(contact);
        this.submited = true
        if(this.messageShop){
            contact.messageShop= this.messageShop
            this.http.post(api,contact)
            .subscribe(
                data => {
                    this.count.getCountLienHe(sideNavItems.contact)
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Đã gửi phản hồi lại khách hàng",
                        showConfirmButton: false,
                        timer: 1500
                      })
                    this.getContact(this.tabSelect)
                    closeMd.click()
                },
                err => {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: "Có lỗi!",
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            )
        }
    }
    tuChoi(api : any,contact : any,closeMd : any){
            this.http.post(api,contact)
            .subscribe(
                data => {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: "Đã từ chối hồi lại khách hàng",
                        showConfirmButton: false,
                        timer: 1500
                      })
                    this.getContact(this.tabSelect)
                    closeMd.click()
                },
                err => {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: "Đã gửi phản hồi lại khách hàng",
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            )
    }
    showMsg(success:any,err:any,msg : any){
        this.alertTB.showSuccess = success
        this.alertTB.showError = err
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.showError = false
            this.alertTB.showSuccess = false
            this.alertTB.msg = ""
        },5000)
    }
    // getOrderDetails(order : any){
    //     this.http.post("api/order-xac-nhan/get-orderDetail",order)
    //     .subscribe(
    //         (data:any) => { console.log(data);
    //             this.orderDetails = data
    //          }
    //     )
    // }
}
