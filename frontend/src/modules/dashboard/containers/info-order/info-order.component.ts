import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
    selector: 'info-order',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './info-order.component.html',
    styleUrls: ['info-order.component.scss'],
})
export class InfoOrder implements OnInit {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient,
        public route : ActivatedRoute,
        public router : Router
        ) {}
    public order: any = {}
    public loading = false
    public orderDetail : any= []
    ngOnInit() {
        let idOrder = this.route.snapshot.paramMap.get("id")
        this.getOrder(idOrder)
    }
    getOrder(id : any){
        this.http.get<any>(`api/order-xac-nhan/get-order-by-id/${id}`)
        .subscribe(
            data => {
                // if(!data) this.router.navigate(["/error/404"]);
                this.order = data;
                console.log(data);
                this.getOrderDetail(data)
            }
        )
    }

    getOrderDetail(order : any){
        this.http.post<any>(`api/order-xac-nhan/get-orderDetail`,order)
        .subscribe(
            data => {
                // if(!data) this.router.navigate(["/error/404"]);
                this.orderDetail = data;
                console.log(data);
            }
        )
    }
    // setProductSize(productSizeLst : Array<any>){
    //     let colors = [""]
    //     colors =[]
    //     let color = {id:-1}
    //     console.log(productSizeLst);
        
    //     productSizeLst.forEach((prds:any)=>{
    //         if(color.id !== prds.color.id)  {
    //             color = prds.color
    //             colors.push(prds.color)
    //         }
    //     })
    //     console.log(colors);
        
    //     colors.forEach((cl:any)=>{
    //         let productSize = [""]
    //         productSize = []
    //         productSizeLst.forEach( (prds:any)=>{
    //             if(cl.id === prds.color.id) productSize.push(prds)
    //         })
    //         if(productSize.length > 0)
    //         this.productSize.push({
    //             color:cl,
    //             productSize:productSize
    //         })
    //     })
            
        
    // }

}
