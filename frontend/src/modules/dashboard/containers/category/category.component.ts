import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-product',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './category.component.html',
    styleUrls: ['category.component.scss'],
})
export class CategoryComponent implements OnInit {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient
        ) {}
    ngOnInit() {
        this.getCategories()
        this.getCorlorCtg()
        this.getSizeCtg()

    }
    public err :any = {empty : false,exists:false}
    public loading = true
    public deleteCategory :any = {data : {},child : true}
    public alertTB :any = {show: false, msg:"" }
    public datas : any = {}
    public categoryOld : any = {name:"",button:{click:()=>{}}}
    public listDeleteParen : any= []
    public listDeleteChildl: any = []
    public formColor = this.formBuilder.group({
        id:[""],
        colorName:["",[Validators.required]]
    })
    public formSize = this.formBuilder.group({
        idSize:[""],
        sizeName:["",[Validators.required]]
    })
    getCategories(){
        this.loading = true
        this.http.get<Object[]>(`api/type/free/get-all-type1`)
        .subscribe(
            datas => {
                this.loading = false;
                this.datas = datas
                console.log(this.datas);
            }
        )
    }

    saveCategoryParent(input : any,data : any , button : any,outline : any,close :any){
        
        var api = "api/type/edit-type"
        var msg = "Sửa thành công"
        if(!data) {
            msg = "Thêm mới thành công"
            api =  "api/type/add-type"
            data = {}
        }
        if(this.isExistsCategory(input.value)) {
            this.err.exists = true 
            outline.classList.add("class" , "outline-danger")
            return
        }
        if(!input.value){
            outline.classList.add("class" , "outline-danger")
            this.err.empty = true
            return
        }
        this.resetErr(outline)
        this.loading = true
        data.typeName = input.value 
        console.log(data);
        // delete data.categorySonlst
        this.http.post(api,data)
        .subscribe(
            datas => {
                if(button) button.hidden = true
                this.getCategories()
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: msg,
                    showConfirmButton: false,
                    timer: 1500
                  })
                this.loading = false
                if(close)
                close.click()
                input.value = ""
            },
            // err => this.showMsg("Có lỗi")
        )
    }
    // saveCategoryChild(input : any,dataParent : any, dataChild : any , button : any,outline : any){
    //     dataParent = this.copy(dataParent)
    //     dataChild = dataChild === null ? null : this.copy(dataChild)
    //     var api = "api/category-son/edit-category-son"
    //     var msg = "Sửa thành công"
    //     if(!dataChild) {
    //         var msg = "Thêm mới thành công"
    //         api =  "api/category-son/save-category-son"
    //         dataChild = {}
    //     }
    //     dataChild.categorySonName = input.value 
    //     if(this.isExistsCategory(input.value)) {
    //         this.err.exists = true 
    //         outline.classList.add("class" , "outline-danger")
    //         return
    //     }
    //     if(!input.value){
    //         outline.classList.add("class" , "outline-danger")
    //         this.err.empty = true
    //         return
    //     }
    //     this.resetErr(outline);
    //     this.loading = true
    //     // delete dataParent.categorySonlst
    //     dataChild.categoryDad = dataParent
    //     console.log(JSON.stringify(dataChild));
        
    //     this.http.post(api,dataChild)
    //     .subscribe(
    //         datas => {
    //             if(button) button.hidden = true
    //             this.getCategories()
    //             this.showMsg(msg)
    //             this.loading = false
    //             input.value = null
    //         }
    //     )
    // }
    isExistsCategory(value : string) : boolean{
        for(let  ctg of this.datas.data){
            if(ctg.typeName === value){
                return true
            }
        }
        return false
    }
    resetErr(outline:any){
        outline.classList.remove("class" , "outline-danger")
        this.err.empty = false
        this.err.exists = false
    }
    copy(data : Object){
        return Object.assign({},data)
    }
    alertDeleteCategory(data : any,categoryChild : boolean){
        this.deleteCategory.data = this.copy(data)
    }
    deleteCate(){
        this.loading = true
        var api = "api/type/delete-type"
        // if(this.deleteCategory.child){
        //     api =  "api/category-son/delete-category-son"
        // }
        this.http.post(api,this.deleteCategory.data)
        .subscribe(
            datas => {
                this.getCategories()
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Đã xóa",
                    showConfirmButton: false,
                    timer: 1500
                  })
                this.loading = false
            }
        )
    }
    showMsg(msg : string){
        this.alertTB.show = true
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.show = false
            this.alertTB.msg = ""
        },5000)
    }

    //_____________________________________________________________________color
    public editColorStatus = false
    public dataColor : any= []
    public colorDelete: any ={}
    public category : any = {idCategory:0}
    public colorList : any = []
    public submitColor = false
    onSubmitColor(){
        console.log(this.validateColor);
        
        let api = 'api/color/add-color'
        if(this.editColorStatus  ) api = 'api/color/edit-color'
        console.log(this.formColor.value);
        this.submitColor = true
        if(this.validateColor){
            let color = Object.assign({},this.formColor.value)
            color.category = this.category
            this.http.post<any>(api,color)
            .subscribe(
                (data:any)=>{
                    if(data?.message === "FAIL_NAME_COLOR"){
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Màu này đã tồn tại',
                            showConfirmButton: false,
                            timer: 1500
                          })
                          return
                    }
                    if(this.editColorStatus)
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã sửa màu',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    else Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã thêm màu',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    this.getCorlor();this.resetColor() }
            )
        }
    }
    setCategory(ctg: any,color : boolean){
        this.resetColor()
        this.resetSize()
        this.category = ctg 
        if(color)
        this.getCorlor()
        else this.getSize()
    }
    editColor(ctg : any){
        this.editColorStatus = true
        this.formColor.patchValue(ctg)
    }
    resetColor(){
        this.submitColor = false
        this.formColor.reset()
        this.editColorStatus = false
    }
    getCorlor(){
        this.http.get(`api/color/free/get-all-color-by-cate-dad/${this.category.idCategory}`)
        .subscribe(
            (data:any)=> {
                this.resetColor()
                this.category.colorList = data.data}
        )
    }
    getCorlorCtg(){
        this.http.get(`api/color/free/get-all-color`)
        .subscribe(
            (data:any)=> {
                this.colorList = data.data
            }
        )
    }
    deleteColor(){
        this.http.post(`api/color/delete-color`,this.colorDelete)
        .subscribe(
            data => { 
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Đã xóa màu thành công',
                    showConfirmButton: false,
                    timer: 1500
                  })
                this.getCorlorCtg() ; this.resetColor() }
        )
    }
    get validateColor(){
        let formcoltrol = this.formColor.controls
        return !(formcoltrol["colorName"].errors)
    }
    alertDeleteColor(cl : any){
        this.colorDelete = cl
    }
    //_____________________________________________________________________>SIZE  
    public editSizeStatus = false
    public dataSize : any= []
    public submitSize = false
    public sizeList : any=[]
    public sizeDelete: any ={}
    onSubmitSize(){
        console.log(this.validateSize);
        let api = 'api/size/add-size'
        if(this.editColorStatus  ) api = 'api/size/edit-size'
        console.log(this.formSize.value);
        this.submitSize = true
        if(this.validateSize){
            let size = Object.assign({},this.formSize.value)
            size.category = this.category
            this.http.post<any>(api,size)
            .subscribe(
                (data:any)=>{ 
                    console.log(data);
                    if(data?.message === "FAIL_NAME_SIZE"){
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Kích thước này đã tồn tại',
                            showConfirmButton: false,
                            timer: 1500
                          })
                          return
                    }
                    if(this.editSizeStatus) 
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã sửa kích thước thành công',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    else Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã thêm kích thước thành công',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    this.getSize();
                    this.resetSize() }
            )
        }
    }
    editSize(s : any){
        this.editSizeStatus = true
        this.formSize.patchValue(s)
    }
    resetSize(){
        this.submitSize = false
        this.formSize.reset()
        this.editSizeStatus = false
    }

    getSize(){
        this.http.get(`api/size/free/get-all-size-by-cate-dad/${this.category.idCategory}`)
        .subscribe(
            (data:any)=> {
                this.resetColor()
                this.dataSize = data.data}
        )
    }
    getSizeCtg(){
        this.http.get(`api/size/free/get-all-size`)
        .subscribe(
            (data:any)=> {
                this.sizeList = data.data
                
            }
        )
    }
    deleteSize(){
        console.log(this.sizeDelete);
        
        this.http.post(`api/size/delete-size`,this.sizeDelete)
        .subscribe(
            data => {  
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Đã xóa kích thước thành công',
                    showConfirmButton: false,
                    timer: 1500
                  })
                this.getSize() ; this.resetSize() }
        )
    }
    get validateSize(){
        let formcoltrol = this.formSize.controls
        return !(formcoltrol["sizeName"].errors)
    }
    alertDeleteSize(cl : any){
        this.sizeDelete = cl
    }

    getSizeAndColor(category : any){
        // this.getSizeCtg()
    }
    //_____________________________________________________________________>

    editForm(button : any,input : any){
        if(this.categoryOld?.button) this.categoryOld.button.click()
        input.removeAttribute('disabled')
        console.log(button.children[0]);
        
        button.hidden = false
        this.categoryOld.name = input.value 
        this.categoryOld.button = button.children[0]
    }

    close(button : any, input : any ){
        button.hidden = true 
        input.setAttribute("disabled","")
        input.value = this.categoryOld.name
    }
    clickCheckBoxParent( checkBoxChilds : any , checkBoxParent : any , dataParent : never , dataChild : any  ){
        let value = checkBoxParent.checked
        if(value) {this.listDeleteParen.push(dataParent)}
        else{
            this.listDeleteParen =  this.listDeleteParen.filter((ctg:any) => !(ctg === dataParent) )
        }
        // let index = 0
        // for(let cb of checkBoxChilds ){
        //     cb.checked = value
        //     this.listDeleteChildl = this.listDeleteChildl.filter(ctg => {
        //         for(let ctgChild of dataChild ){
        //             if (ctgChild === ctg) return false
        //         }
        //         return true
        //     })
        //     index ++;
        // }
    }
    // clickCheckBoxChild( checkBoxChilds : any , checkBoxParent : any ,dataParent : never, dataChild : never[] ){
    //     let value = checkBoxParent.checked
    //     if(value) {this.listDeleteParen.push(dataParent)}
    //     else{this.listDeleteParen =  this.listDeleteParen.filter(ctg => !(ctg === dataParent) )}

    //     checkBoxParent.checked = false 
    //     this.listDeleteParen = this.listDeleteParen.filter(ctg => !(dataParent === ctg))
    //     let index = 0;
    //     for(let cb of dataChild ){
    //         if(checkBoxChilds[index].checked) {
    //             if( this.listDeleteChildl.indexOf(cb) < 0 ) this.listDeleteChildl.push(cb)
    //         }else{
    //             this.listDeleteChildl = this.listDeleteChildl.filter(ctg => !(ctg === cb))
    //         }
    //         index ++ ;
    //     }
    // }
    deleteMany(){
        this.loading= true 
        this.http.post(`api/category-dad/delete-many-category`,{parents:this.listDeleteParen,childs:this.listDeleteChildl})
        .subscribe(
            datas => {
                this.getCategories()   
                this.loading= false
            }
        )
    }
    get document(){
        return document
    }
}
