import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Route } from '@angular/router';
import { url } from '@modules/domain';

@Component({
    selector: 'info-product-admin',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './info-product-admin.component.html',
    styleUrls: ['info-product-admin.component.scss'],
})
export class InfoProductAdmin implements OnInit {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient,
        public route : ActivatedRoute
        ) {}
    public product: any = {productSizeList: []}
    public productSize : any = []
    public loading = false
    public seeMore = false
    ngOnInit() {
        let id = this.route.snapshot.paramMap.get("id")
        this.getProduct(id)
    }
    getProduct(id : any){
        this.http.post<any>('api/product/free/get-detail',{idProduct: id})
        .subscribe(
            data => {
                this.product = data.data;
                console.log(data.data);
                
                this.setProductSize(data.data.productSizeList)
            }
        )
    }
    setProductSize(productSizeLst : Array<any>){
        let colors = [""]
        colors =[]
        let color = {id:-1}
        console.log(productSizeLst);
        
        productSizeLst.forEach((prds:any)=>{
            if(color.id !== prds.color.id)  {
                color = prds.color
                colors.push(prds.color)
            }
        })
        console.log(colors);
        
        colors.forEach((cl:any)=>{
            let productSize = [""]
            productSize = []
            productSizeLst.forEach( (prds:any)=>{
                if(cl.id === prds.color.id) productSize.push(prds)
            })
            if(productSize.length > 0)
            this.productSize.push({
                color:cl,
                productSize:productSize
            })
        })
    }
    public url:string ="helo"
    getImage(name : string ){
        return `${url.domain}/api/product/free/image/${name}` 
    }
    onChangeURL(url:SafeUrl){
        // this.url = url
        console.log(url);
        
    }
}
