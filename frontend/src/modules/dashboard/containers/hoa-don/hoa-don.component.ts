import { HttpClient } from '@angular/common/http';
import {  Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
    selector: 'hoa-don',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './hoa-don.component.html',
    styleUrls: ['hoa-don.component.scss'],
})
export class HoaDonComponent implements OnInit {
    constructor(
        public http : HttpClient
    ) {}
    ngOnInit() {
        let date = new Date()
        this.endDate = this.convertDateToStr(date)
        date.setDate(date.getDate()-7)
        this.startDate = this.convertDateToStr(date)
        this.getOrderOnline()
        // this.getOrderOffline()
    }
    public loading = false
    public startDate : any;
    public endDate : any
    public hoaDonHuy : any = {moTa:""}
    public isOnline : any = "online"
    public orders :any = []
    public seeMore = false
    public order= {}
    getOrderOnline(){
        this.isOnline="online"

        this.http.post(`api/order-xac-nhan/get-order-online`,{
            ngayBatDau:this.startDate,
            ngayKetThuc:this.endDate
        })
        .subscribe(
            (data:any)=>{
                console.log(data);
                this.orders = data
                if(data?.length > 0) 
                this.getOrderDetail(data[0])
            }
        )
    }
    public orderDetail = []
    getOrderDetail(od :any ){
        this.order = od
        this.http.post<any>(`api/order-xac-nhan/get-orderDetail`,od)
        .subscribe(
            data => {
                // if(!data) this.router.navigate(["/error/404"]);
                this.orderDetail = data;
                console.log(data);
            }
        )
    }
    convertDateToStr( date : Date ):string{
        const month = date.getMonth() < 9 ? `0${date.getMonth()+1}`: date.getMonth() + 1
        const year = date.getFullYear()
        const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
        return `${year}-${month}-${day}`
    }

    getOrderOffline(){
        this.isOnline = "offline"
        this.http.post(`api/order-xac-nhan/get-order-offline`,{
            ngayBatDau:this.startDate,
            ngayKetThuc:this.endDate
        })
        .subscribe(
            (data:any)=>{
                console.log(data);
                this.orders = data
                if(data?.length > 0) 
                this.getOrderDetail(data[0])
            }
        )
    }
    getOrderXacNhanHuy(){
        this.isOnline = "huyDon"
        this.http.post(`api/order-xac-nhan/get-order-xan-nhan-huy`,{
            ngayBatDau:this.startDate,
            ngayKetThuc:this.endDate
        })
        .subscribe(
            (data:any)=>{
                console.log(data);
                this.orders = data
                if(data?.length > 0) 
                this.getOrderDetail(data[0])
            }
        )
    }
    isDate30m( stringDate : string ){
        const date1 = new Date()
        const date2 = new Date(stringDate)
        date2.setMinutes(date2.getMinutes()+30)
        return date1 < date2
    }
    public submitHuy = false
    huyHoaDon(order : any, modal : any){
        console.log(order);
        
        if(!order?.moTa?.length  ) { this.submitHuy = true ;return}
        this.http.post(`api/order-xac-nhan/xac-nhan-huy-tai-quay`,order)
        .subscribe(
            (data:any)=>{
                console.log(data);
                // this.orders = data
                // if(data?.length > 0) 
                if(data?.message =="ERROR-USER") {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Bạn không phải là người tạo đơn',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return
                }
                if(data?.data ){ 
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã hủy hóa đơn',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    modal.click()
                    this.getMethod()
                }
                else{
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Hủy hóa đơn thất bại',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            }
        )
    }
    // xacNhanHuyHoaDon(order : any){
    //     this.http.post(`api/order-xac-nhan/xac-nhan-huy-tai-quay`,order)
    //     .subscribe(
    //         (data:any)=>{
    //             // console.log(data);
    //             // this.orders = data
    //             // if(data?.length > 0) 
    //             this.getMethod()
    //         }
    //     )
    // }
    khoiPhucHoaDon(order : any){
        this.http.post(`api/order-xac-nhan/huy-bo-huy-tai-quay`,order)
        .subscribe(
            (data:any)=>{
                console.log(data);
                // this.orders = data
                // if(data?.length > 0) 
                this.getMethod()
            }
        )
    }
    getMethod(){
        if(this.isOnline == "online") this.getOrderOnline()
        else if(this.isOnline == "offline") this.getOrderOffline()
        else this.getOrderXacNhanHuy()
    }
    export(od : any){
        this.http.post(`api/report/export-hoa-don`,od)
        .subscribe(
            data=>{ 
                console.log(data);
                
            }
        )
    }
}
