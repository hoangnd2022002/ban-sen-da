import { HttpClient } from '@angular/common/http';
import {  Component, OnDestroy, OnInit,ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { url } from '@modules/domain';
// import {
//     ScannerQRCodeConfig,
//     ScannerQRCodeResult,
//     NgxScannerQrcodeService,
//     NgxScannerQrcodeComponent,
//     ScannerQRCodeSelectedFiles,
//   } from 'ngx-scanner-qrcode';
import { delay } from 'rxjs/operators';
// import  {url}  from 'environments/environment';
import Swal from 'sweetalert2';

@Component({
    selector: 'ban-hang',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './ban-hang.component.html',
    styleUrls: ['ban-hang.component.scss'],
})
export class BanHangComponent implements OnInit,OnDestroy {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient
        ) {}
    public tabSelectedHoaDon = {index:0,cate:{}};
    public tabSelectedCategory = {index:0,cate:{}};
    // public products = [1,2,3]
    public showProduct : any = []
    public loading : boolean = false
    public pageNumber : number = 0
    public txtSearch : string  ="";
    public categoryData : any = {}
    public order : any= []
    public selectOrder : any = {}
    public productQR : any = {}
    public isModal : any = false
    public timeOutQr : any = null
    // public cameraAction : any
    // ?? --- qrcode 
//     public config: ScannerQRCodeConfig = {
//     // fps: 1000,
//     vibrate: 400,
//     // isBeep: true,
//     // decode: 'macintosh',
//     constraints: {
//       audio: false,
//       video: {
//         width: window.innerWidth // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
//       }
//     } 
//   };
    public onEvent(e: any[]): void {
        console.log(e);
        if(this.timeOutQr) clearTimeout(this.timeOutQr)
        this.timeOutQr = setTimeout(()=>this.getProductByQr(e),500);
        
    }
    
    @ViewChild('action') action!: any;
    // @ViewChild('modalSPQR') modalSPQR!: any;
    // public handle(action: NgxScannerQrcodeComponent, fn: string): void {
    //     // Fix issue #27, #29
    //     const playDeviceFacingBack = (devices: any[]) => {
    //       // front camera or back camera check here!
    //       const device = devices.find(f => (/back|rear|environment/gi.test(f.label))); // Default Back Facing Camera
    //       action.playDevice(device ? device.deviceId : devices[0].deviceId);
    //     }
    
    //     if (fn === 'start') {
    //       action.start(playDeviceFacingBack).subscribe((r: any) => console.log(fn, r), alert);
    //     } else {
    //       action.stop().subscribe((r: any) => console.log(fn, r), alert);
    //     }
    // }
    ngAfterViewInit(): void {
        // this.action.pipe(delay(3000)).subscribe(() => {
            // this.action.start()
            setTimeout(()=> this.action.start(),3000)
        // });
    }
    getProductByQr(qrCode:any){
        if(qrCode && qrCode.length > 0)
        this.http.post(`api/product/get-detail-by-qr-code`,{qrCode:qrCode})
        .subscribe(
            (data:any)=>{
                console.log(data);
                this.productQR = this.orderdetail_model
                this.productQR.message = data.message
                this.productQR.productSize = data.data 
                if(data?.message == 'PRODUCT_DETAIL_IS_OUT_OF_STOCK'){
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Sản phẩm này đã hết',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return
                }
                if(data?.message =='PRODUCT_DETAIL_HAS_BEEN_DELETE'){
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Sản phẩm này đã bị xóa khỏi cửa hàng',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return
                }
                if(!this.isModal) {this.isModal = true
                document.getElementById("modalSPQR")?.click()}
                // console.log(this.productQR);
                
            }
        )
    }
    ngOnInit() {
        document.getElementById("camera")?.click()
        this.getCategory()
        this.getCart()
        // setTimeout(()=>this.action.start(),2000)
        if(this.order?.length === 0)
        this.addOrder()
    }
    
    ngOnDestroy(): void {
        setTimeout(()=> this.action.stop(),3000)
        this.saveCart()
    }
    addToCartByQR(){
        this.addToCart(this.productQR.quantity,this.productQR?.productSize)
        document.getElementById("modalSPQR")?.click()
        this.isModal = false
    }
    public moneyKhach = 0
    getCart(){
        let str = this.pareJSON(decodeURIComponent(localStorage.getItem("ban-hang")+""))
        if(!str) {this.addOrder();return}
        let json = JSON.parse(str);
        
        if(json?.length>0) {
            json.forEach((od : any) => {
                od.orderDetailsList.forEach((odt:any)=>{
                    if(odt.productSize.size.price < 1)  odt.productSize.size.price = 1
                })
            });
            this.selectOrder= json[0]
            this.order = json
        }else{
            this.addOrder()
        }
       
        console.log(this.order);
        
    }
    pareJSON(string :any) : string{
        if(!string)  return ""
        return string
    }
    saveCart(){
        localStorage.setItem("ban-hang",encodeURIComponent(JSON.stringify(this.order)))
    }
    
    get getTotalMoney():number{
        return 2
    }
    getDash( odt : any , input : any){
        var  quantity = Number(input.value) 
        if(quantity > odt.productSize.quantity)  {odt.quantity =  odt.productSize.quantity - 1 ; input.value =  odt.productSize.quantity -1}
        if(quantity <= 1) return 
        else odt.quantity = Number(quantity - 1)
        this.saveCart()
    }
    setQuantity( odt : any ,input : any,max : number ){
        var  quantity = input.value 
        if(quantity > max ) {odt.quantity = max ; input.value = max}
        else if(quantity < 1) {odt.quantity = 1;input.value = 1}
        else odt.quantity = Number(quantity*1)
        console.log(this.order);
        this.saveCart()
    }
    getPlus(odt : any ,input : any,max : number){
        var  quantity = input.value 
        if(quantity < 1)  {odt.quantity = 1 ; input.value =  1}
        if(quantity >= max ) return 
        else odt.quantity = Number(quantity*1 + 1)
        this.saveCart()
    }
    get orderdetail_model ()  {
        return Object.assign({},{
            quantity:1,
            product:{},
            productSize:{}
        })
    }
    deleteHDProduct(hd : any){
        this.selectOrder.orderDetailsList= this.selectOrder.orderDetailsList.filter((od:any) => !(od.productSize.idProductSize === hd.productSize.idProductSize ))
        this.saveCart()
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: "Đã xóa sản phẩm khỏi hóa đơn "+this.selectOrder.index,
            showConfirmButton: false,
            timer: 1500
          })
    }
    get order_model ()  {
        let index = 1;
        if( this.order?.length === 0)
        index = 1
        else index = this.order[this.order?.length-1].index + 1 
        return Object.assign({},{
            index:index,
            orderDetailsList:[]
        })
    }
    addToCart(quantity : number, productSize : any){
        const {images,...prds} =  productSize
        let order_detail = this.orderdetail_model
        order_detail.quantity = quantity
        order_detail.product = productSize.product
        order_detail.productSize = {...prds}
        let push = true
        this.selectOrder?.orderDetailsList.forEach( 
            (od : any) => {
                if( od.productSize.idProductSize === productSize.idProductSize )
                {
                    push = false
                    if(od.quantity >= prds.quantity){
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Số lượng đã tới giới hạn có trong cửa hàng',
                            showConfirmButton: false,
                            timer: 1500
                          })
                          od.quantity = prds.quantity
                          return
                    }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Đã thêm sản phẩm',
                            showConfirmButton: false,
                            timer: 1500
                          })
                    }
                    od.quantity = od.quantity + quantity
                }
            }
        )
        if(push) {this.selectOrder.orderDetailsList.push(order_detail)
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Đã thêm sản phẩm',
                showConfirmButton: false,
                timer: 1500
              })
        }
        this.saveCart()
        this.productQR = {}
        
    }
    get toltal() : number{
        let tong = 0;
        if(this.selectOrder.orderDetailsList?.length > 0){
            for(let od of this.selectOrder.orderDetailsList){
                tong += od.quantity * od.productSize.price
            }
        }
        return tong;
    }
    addOrder(){
        this.order.push(this.order_model)
        this.setSelectOrder(this.order[this.order?.length-1])
    }
    setSelectOrder(order:any){
        this.selectOrder = order
    }
    deleteOrder(order:any){
        this.order = this.order.filter((od:any)=> !(order===od) )
        if(this.order.length < 1) this.addOrder()
        else this.setSelectOrder(this.order[this.order?.length-1])
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: "Đã xóa",
            showConfirmButton: false,
            timer: 1500
          })
    }
    public hiddenXemThem = false
    searchPRS(i:any,category : any){
        let isPush = true
        this.pageNumber = 0

        this.tabSelectedCategory.index = i
        this.tabSelectedCategory.cate = category
        this.http.post(`api/type/free/get-product-detail-by-type?pageSize=12&page=${this.pageNumber++}`,
        {
            keyword: this.txtSearch,
            idType: category.idType
        })
        .subscribe(
            (data:any) => {
                const datas = data.data
                if(datas?.length == 12) 
                this.hiddenXemThem = true
                else this.hiddenXemThem = false
                if(isPush){
                    this.showProduct=datas
                }else  this.showProduct = datas
                this.loading = false
                console.log(datas);
            },
            (err:any) => this.loading = false
        )
    }
    search(i:any,category : any){
        let isPush = true
        if(this.tabSelectedCategory.index !== i )
        {
            isPush= false
            this.pageNumber = 0}

        this.tabSelectedCategory.index = i
        this.tabSelectedCategory.cate = category
        this.http.post(`api/type/free/get-product-detail-by-type?pageSize=12&page=${this.pageNumber++}`,
        {
            keyword: this.txtSearch,
            idType: category.idType
        })
        .subscribe(
            (data:any) => {
                const datas = data.data
                console.log("data");
                
                console.log(datas);
                
                if(datas?.length == 12) 
                this.hiddenXemThem = true
                else this.hiddenXemThem = false
                if(isPush){
                    this.showProduct.push(...datas)
                }else  this.showProduct = datas
                this.loading = false
                console.log(datas);
            },
            (err:any) => this.loading = false
        )
    }
    
   
    // public pageNumber : number =0
    getCategory(){
        this.http.get("api/type/free/get-all-type1")
        .subscribe(
            (data:any) => {
                this.categoryData = data
                // console.log("category",data);
                // this.categoryData.content.sort((a:any,b:any)=> {
                //     if(a.idCategory > b.idCategory) return 1
                //     if(a.idCategory < b.idCategory) return -1
                //     return 0
                // });
                this.search(0,data.data[0])
            }
        )
    }
    getImage(name : string){
        return `${url.domain}/image/${name}`;
    }

    // public formGroup = this.formBuilder.group()
    validate(){
        let errMoney = false
        if(this.moneyKhach <= 0 || this.moneyKhach >100000000) errMoney = true
        if(this.selectOrder?.orderDetailsList?.length < 1){
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: "Chưa có sản phẩm trong hóa đơn",
                showConfirmButton: false,
                timer: 1500
              })
        }
        return !(
            errMoney || this.tienThua < 0
            || this.selectOrder?.orderDetailsList?.length < 1
            )
    }

    get tienThua(){
        return this.moneyKhach - this.toltal 
    }
    public submited = false
    thanhToan(){
        this.submited = true
        if(this.validate()){
            this.http.post(`api/order-xac-nhan/ban-tai-quay`,this.selectOrder)
            .subscribe( (data:any)=>{
                if(!data.data) {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: "Thanh toán thất bại. " + data.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                    return
                }
                this.deleteOrder(this.selectOrder)
                this.saveCart()
                this.searchPRS(this.tabSelectedCategory.index ,this.tabSelectedCategory.cate)
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Thanh toán thành công",
                    showConfirmButton: false,
                    timer: 1500
                  })
                this.submited = false
                this.getPDF(data.data)
                this.moneyKhach = 0 

            } )
        }
    }
    get orderDTO (){
        return Object.assign({},{ maHoaDon:0,nguoiBanHang:0,tienKhachTra:0,table1:[{}] })
    }
    get sanPham(){
        return Object.assign({},{ tenHang:"Sen da",soLuong:0,donGia:1000,thanhTien:1000 })
    }
   
    getPDF(order:any){
        let orderDTO = this.orderDTO
        orderDTO.table1=[]
        orderDTO.maHoaDon= order.idOrder 
        orderDTO.nguoiBanHang= order.idUser
        orderDTO.tienKhachTra= this.moneyKhach
        console.log(order);
        
        order.orderDetailsList.forEach((odt:any)=>{
            let sanPham = this.sanPham
            sanPham.donGia = odt.productSize.price 
            sanPham.soLuong = odt.quantity 
            sanPham.tenHang = `${odt.productSize.product.productName }(${odt.productSize.size.sizeName})`
            sanPham.thanhTien = odt.quantity *  odt.productSize.product.productName
            orderDTO.table1.push(sanPham)
        })
        console.log(JSON.stringify(orderDTO));
        console.log(orderDTO);
        
        this.http.post(`api/report/export-hoa-don`,orderDTO,
        {responseType: 'arraybuffer'}
        )
        .subscribe(
            (data:any) => {
                console.log(data);
                var file = new Blob([data], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            },
            (err:any)=>{
                console.log(err.error.text);
                // var pdfAsDataUri = "data:application/pdf;base64,"+err.error.text;
                // window.open(pdfAsDataUri);
                
                // window.open()
                // this.showPDF(err.error.text)
            }
        )
    }
    // showPDF(data:any){
    //     let base64ToArrayBuffer = (base64:any)=> {
    //         var binaryString = window.atob(base64);
    //         var binaryLen = binaryString.length;
    //         var bytes = new Uint8Array(binaryLen);
    //         for (var i = 0; i < binaryLen; i++) {
    //             var ascii = binaryString.charCodeAt(i);
    //             bytes[i] = ascii;
    //         }
    //         return bytes;
    //     }
    //     let arrrayBuffer = base64ToArrayBuffer(data); //data is the base64 encoded string
    //     let blob = new Blob([arrrayBuffer], {type: "application/pdf"});
    //     let link = window.URL.createObjectURL(blob);
    //     window.open(link,'', 'height=650,width=840');
    // }
}
