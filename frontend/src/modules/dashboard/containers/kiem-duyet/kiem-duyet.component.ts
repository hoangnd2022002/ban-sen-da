import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Count } from '@modules/layout/service';
import Swal from 'sweetalert2';
import { sideNavItems } from '@modules/navigation/data';

@Component({
    selector: 'kiem-duyet',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './kiem-duyet.component.html',
    styleUrls: ['kiem-duyet.component.scss'],
})
export class KiemDuyetComponent implements OnInit {
    constructor(
        public http : HttpClient,
        public count:Count
    ) {}
    public orderDetails: any =[]
    public datasOrder : any = {}
    public loading = false
    public pageNumber = 0
    public startDate = ""
    public endDate = ""
    public isDay = false
    public pageMethod = ""
    public alertTB : any= {showSuccess:false,showError:false,msg:""}
    ngOnInit( ) {
        this.tabSelect = this.tab[0]
        this.getOrder()
    }
    public order = {}
    
    public tab = [
        {id: 1,name:"Chưa xác nhận",api:{
            get:`api/order-xac-nhan/cac-don-cho-xac-nhan`,
            post:"api/order-xac-nhan/dang-cbi-hang",
            delete:"api/order-xac-nhan/da-huy",
            status:"CHỜ XÁC NHẬN"
        }},
        {id: 2,name:"Đã xác nhận",api:{
            get:"api/order-xac-nhan/cac-don-dang-chuan-bi",
            post:"api/order-xac-nhan/dang-giao",
            status:"ĐÃ XÁC NHẬN"
        }},
        {id: 3,name:"Đang giao",api:{
            get:"api/order-xac-nhan/cac-don-dang-giao",
            post:"api/order-xac-nhan/da-giao",
            delete:"api/order-xac-nhan/huy-khi-dang-giao",
            status:"ĐANG GIAO"
        }},
        {id: 4,name:"Đã giao",api:{
            get:"api/order-xac-nhan/cac-don-da-giao",
            post:"api/order-xac-nhan/",
            status:"ĐÃ GIAO"
        }},
        {id: 5,name:"Đã nhận",api:{
            get:"api/order-xac-nhan/cac-don-da-nhan",
            post:"api/order-xac-nhan/",
            status:"ĐÃ NHẬN"
        }},
        {id: 6,name:"Đã hủy",api:{
            get:"api/order-xac-nhan/cac-don-da-huy",
            post:"api/order-xac-nhan/",
            status:"ĐÃ HỦY"
        }},
    ]
    public tabSelect : any;
    getOrder(){
        this.http.get(this.tabSelect.api.get+`?currentPage=${this.pageNumber}`)
        .subscribe(
            datas=> {
                console.log(datas);
                console.log(this.tabSelect.api);
                this.datasOrder = datas
            }
        )
    } 
    setOrder(od:any ){
        this.order = od
    }
    getByDate(){
        this.isDay = true 
        this.getMethod()
    }
    getNotDate(){
        this.isDay = false
        this.getMethod()
    }
    selected(tabSelect : any){
        this.tabSelect = tabSelect
        this.pageNumber = 0
        this.getMethod()
    }
    getMethod(){
        if(this.isDay) this.getOrderByDate()
        else this.getOrder()
    }
    convertDateToStr( date : Date ):string{
        const month = date.getMonth() < 9 ? `0${date.getMonth()+1}`: date.getMonth() + 1
        const year = date.getFullYear()
        const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
        return `${year}-${month}-${day}`
    }
    getOrderByDate(){
        this.pageNumber = 0
        this.http.post(`api/order-xac-nhan/get-order-by-day?currentPage=${this.pageNumber}`,{
            ngayBatDau:this.startDate,
            ngayKetThuc:this.endDate,
            keyword:this.tabSelect.api.status
        })
        .subscribe(
            datas=> {
                console.log(datas);

                this.datasOrder.data = datas
            }
        )
    }
    postRequest(api : any,order : any){
        console.log(api);
        console.log(order);
        
        this.http.post(api,order)
        .subscribe(
            (data:any) => {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Đã chuyển trạng thái đơn hàng",
                    showConfirmButton: false,
                    timer: 1500
                  })
                if(data.message==='Hết hàng')
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Đơn có sản phẩm bị hết",
                    showConfirmButton: false,
                    timer: 1500
                })
                console.log(data);
                
                this.getMethod()
                if(api === this.tab[0].api.post && data.message!=='Hết hàng') {
                    this.count.gresetCountKiemDuyetn(sideNavItems.kiemDuyet)
                    this.http.post(`api/report/export-hoa-don-online`,{maHoaDon:order.idOrder},
                    {responseType: 'arraybuffer'})
                    .subscribe(
                        (dt:any) => {
                            console.log(dt);
                            // this.showPDF()   
                            var file = new Blob([dt], {type: 'application/pdf'});
                            var fileURL = URL.createObjectURL(file);
                            window.open(fileURL);                     
                        },
                        
                        err=>{
                            console.log(err);
                            
                            // this.showPDF(err.error.text)
                        }
                    )
                }
            },
            err => {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Có lỗi",
                    showConfirmButton: false,
                    timer: 1500
                  })
            }
        )
    }
    showPDF(data:any){
        let base64ToArrayBuffer = (base64:any)=> {
            var binaryString = window.atob(base64);
            var binaryLen = binaryString.length;
            var bytes = new Uint8Array(binaryLen);
            for (var i = 0; i < binaryLen; i++) {
                var ascii = binaryString.charCodeAt(i);
                bytes[i] = ascii;
            }
            return bytes;
        }
        let arrrayBuffer = base64ToArrayBuffer(data); //data is the base64 encoded string
        let blob = new Blob([arrrayBuffer], {type: "application/pdf"});
        let link = window.URL.createObjectURL(blob);
        window.open(link,'', 'height=650,width=840');
    }
    showMsg(success:any,err:any,msg : any){
        this.alertTB.showSuccess = success
        this.alertTB.showError = err
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.showError = false
            this.alertTB.showSuccess = false
            this.alertTB.msg = ""
        },5000)
    }
    // getOrderDetails(order : any){
    //     this.http.post("api/order-xac-nhan/get-orderDetail",order)
    //     .subscribe(
    //         (data:any) => { console.log(data);
    //             this.orderDetails = data
    //          }
    //     )
    // }
    nextPage(input:any ,totalPages : number ):boolean{
        if(this.pageNumber >= totalPages - 1 )return false
        else this.pageNumber = this.pageNumber*1 + 1
        console.log(this);
        return true
    }
    prePage(input:any ,totalPages : number):boolean{
        if(this.pageNumber - 1 < 0 )return false
        else this.pageNumber = this.pageNumber*1 - 1
        return true
    }
    getPage(input:any ,totalPages : number):boolean{
        if( input.value*1 > totalPages  ||  input.value*1  <= 0   )  {
            input.classList.add("class" , "outline-danger");
            input.value = this.pageNumber*1 + 1
            return false
        }
        else this.pageNumber = input.value - 1
        input.classList.remove("outline-danger");
        return true
    }
    actionPage(callbackActionPage : boolean  ){
        if(callbackActionPage ) {
            this.getMethod()
        }
    }
}
