import { DashboardComponent } from './dashboard/dashboard.component';
import { LightComponent } from './light/light.component';
import { StaticComponent } from './static/static.component';
import { ProductComponent } from './product/product.component';
import { CategoryComponent } from './category/category.component';
import { UserComponent } from './user/user.component';
import { BanHangComponent } from './ban-hang/ban-hang.component';
import { HoaDonComponent } from './hoa-don/hoa-don.component';
import { KiemDuyetComponent } from './kiem-duyet/kiem-duyet.component';
import { InfoProductAdmin } from './info-product-admin/info-product-admin.component';
import { InfoOrder } from './info-order/info-order.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ContactComponent } from './contact/contact.component';
import { SizeComponent } from './size/size.component';
import { ColorComponent } from './color/color.component';
import { HabitatComponent } from './habitat/habitat.component';
import { LightAspectComponent } from './light-aspect/light-aspect.component';
export const containers = [
    ProductComponent
    ,DashboardComponent
    , StaticComponent
    ,LightComponent
    ,CategoryComponent
    ,UserComponent
    ,BanHangComponent
    ,HoaDonComponent
    ,KiemDuyetComponent
    ,InfoProductAdmin
    ,InfoOrder
    ,AddProductComponent
    ,ContactComponent
    ,SizeComponent
    ,ColorComponent
    ,HabitatComponent
    ,LightAspectComponent
];

export * from './dashboard/dashboard.component';
export * from './static/static.component';
export * from './light/light.component';
export * from './product/product.component';
export * from "./category/category.component";
export * from  "./user/user.component";
export * from  "./ban-hang/ban-hang.component";
export * from "./hoa-don/hoa-don.component"
export * from "./kiem-duyet/kiem-duyet.component"
export * from "./info-product-admin/info-product-admin.component"
export * from "./info-order/info-order.component"
export * from "./add-product/add-product.component"
export * from "./contact/contact.component"
export * from "./size/size.component"
export * from "./color/color.component"
export * from "./habitat/habitat.component"
export * from "./light-aspect/light-aspect.component"

