import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-dashboard',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './dashboard.component.html',
    styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
    constructor(
        public http : HttpClient,
    ) {}
    ngOnInit() {
        let date = new Date()
        this.endDate = this.convertDateToStr(date)
        date.setDate(date.getDate()-1)
        this.startDate = this.convertDateToStr(date)
        this.getThongKe()
    
    }
    public startDate =""
    public endDate = ""
    public thongke : any= {}
    getThongKe(){
        this.http.post('api/thong-ke/search-theo-ngay',{startDate:this.startDate,endDate:this.endDate})
        .subscribe(
            data=>{  
                this.thongke = data
                console.log(data);
            },
            err=>{
                console.log("err");
                
            }
        )
    }
    convertDateToStr( date : Date ):string{
        const month = date.getMonth() < 9 ? `0${date.getMonth()+1}`: date.getMonth() + 1
        const year = date.getFullYear()
        const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
        return `${year}-${month}-${day}`
    }
    public bieuDo : any={data:[]}
    
}



