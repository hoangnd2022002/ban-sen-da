import { HttpClient } from '@angular/common/http';
import {  Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Count } from '@modules/layout/service';
import Swal from 'sweetalert2';
import { sideNavItems } from '@modules/navigation/data';
import { url } from '@modules/domain';

@Component({
    selector: 'sb-add-product',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './add-product.component.html',
    styleUrls: ['add-product.component.scss'],
})
export class AddProductComponent implements OnInit {
    constructor(
        public formBuilder : FormBuilder,
        public http : HttpClient,
        public route : ActivatedRoute,
        public router: Router,
        public count : Count
        ) {}
    formGroup = this.formBuilder.group({
        idProduct:[""],
        productName:["",[Validators.required,Validators.minLength(5),Validators.maxLength(225)]],
        category:[{}, [Validators.required,]],
        moTa:[""],
        habitatsList:["",[Validators.required]],
        lightAspect:["",[Validators.required]],
        productSizeList:[[],[Validators.required]],
    })
    // public 
    public productSize : any  = []
    public pageNumber : number = 0
    public pageMethod : any ="getProducts";
    public img = ""
    public errorAPI :string  = ""
    public alertTB : any = {show: false, msg:""}
    public deleteProDuct: any = {}
    public loading = true
    public loadingAddProduct = false
    public actionEdit = false
    public fileImages : any[] = []
    public showImage : string[]  = []
    public file : any 
    public alert: any = {index:-1, productSize : {imagePath:""}}
    public submited = false
    public categoryDatas : any = {content:[{}]}
    public datas : any ={}
    public listColorChoose : any = []
    public listSizeChoose : any =[]
    public product : any = { idProduct: null, status: 1 }
    public error = false
    public productSizeDelete : any = []
    get getFormControl(){
        return this.formGroup.controls
    }
    get productSize_model() {
        return Object.assign({},{
            idProductSize:"",
            quantity : 0,
            price : 0,
            color : {},
            imagePath:"",
            product : {},
            size : {},
            status:1
            // images : "image"
        })
    }

    onSubmit(){
        console.log("sunmit");
        this.error = false
        this.loadingAddProduct = true
        var api = "api/product/save-product"
        if(this.actionEdit) api = "api/product/edit-product"
        if(this.product.status == 2) api = "api/product/ban-lai-product"
        if(!this.submited) {this.submited = true}
        console.log("validate" + this.validated());
        // console.log(this.formGroup.controls.moTa);
        localStorage.setItem("idType",this.getFormControl.category.value.idType)
        
        if(this.validated()){
            this.setHabitat();
            let formGroup = Object.assign({},this.formGroup.value)
            let productSize = [""]
            productSize = []
            this.productSize.forEach((prds1:any)=>{
                for(let prds2 of prds1.productSize){ productSize.push(prds2) }
            })
            formGroup.productSizeList  = Object.assign([], productSize)
            // let formData = new FormData()
            // this.fileImages.forEach(item => formData.append("file",item))
            const { ["category"]: parentValue, ...noChild } = formGroup;
            const { ["colorRepository"]:colorRepository,["stt"]:stt,["colorList"]:colorList,["sizeRepository"]:sizeRepository,["sizeList"]:sizeList, ...childWithout } = parentValue;
            const withoutThird = { ...noChild, ["type"]: childWithout.idType };
            withoutThird.productSizeList.push(...this.productSizeDelete)
            // formData.append("entity",JSON.stringify(withoutThird))
            console.log(withoutThird);
            this.http.post<any>(api,withoutThird)
            .subscribe(
                data => {
                    console.log(data)
                    if(data.message === 'FAIL_PRODUCT_NAME'){
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Tên sản phẩm đã tồn tại',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        return
                    }
                    this.loadingAddProduct = false
                    if(this.actionEdit)  {Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã sửa sản phẩm',
                        showConfirmButton: false,
                        timer: 1500
                      })
                      this.count.getCountHetSL(sideNavItems.product)
                    }
                    else Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã thêm sản phẩm',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    this.router.navigate(["/admin/product"])
                },
                err => console.log(err)
            )
            console.log("submited!");
        }else this.loadingAddProduct = false
    }
    // getImg(prds:any){
    //     console.log(prds);
        
    //     this.http.get("api/product/free/image/"+prds?.imagePath)
    //     .subscribe(
    //         data=> prds.imagePath = data,
    //         data=> console.log(data.body)
            
    //     )

    // }
    createProductSize(color: any){
        if(!color) {
            return
        }
        // this.productSizeDelete.
        // this.listColorChoose[0] = this.getFormControl['category'].value.colorList[0]
        let productSize = this.productSize_model
        productSize.color = color
        productSize.size = this.sizeNotChoose(color)
        productSize.color = color
        productSize.quantity = 1
        productSize.price = 1000
        this.productSize.push({ 
            productSize: [productSize],
            color : color
        })
        console.log(this.productSize);
        
    }
    addSizeAndPrice(color : any,productSizearr : any){
        let productSize = this.productSize_model
        productSize.size = this.sizeNotChoose(color)
        if(!productSize.size) {
            for( const prds of productSizearr ){
                if(prds.status = 2) {
                    prds.status = 1; 
                    return
                }
            }
            return
        }
        productSize.color = color
        productSize.quantity = 1
        productSize.price = 1000
        productSizearr.push(productSize)
    }
    sizeNotChoose(color : any){
        let productSize=[]
        for(let cl of this.productSize){
            if(cl.color === color) productSize = cl.productSize
        }
        if(productSize.length < 1) this.sizes.data[0]
        let ids = productSize.map((prds:any) => prds.size.idSize)
        for(let size of this.sizes.data ){
            if(!ids.includes(size.idSize)) {
                return size
            }
        }
    }
    colorNotChoose(){
        console.log("productSiz");
        
        console.log(this.productSize);
        
        let ids = this.productSize.map((cl:any) => cl.color.id)
        for(let color of this.colors.data ){
            if(!ids.includes(color.id)) {
                // console.log(4);
                return color
            }
        }
    }
    // get ------------------------------------
    public listHabitat : any = []
    public listLightAspect : any = []
    public listLHabitatChoose : any = []
    public colors : any = {data:[]}
    public sizes : any = {data:[]}
    init(){
        this.getHabitas()
        this.getLightAspect()
        this.getColor()
    }
    getHabitas(){
        this.http.get('api/habitat/free/get-all-habitat1')
        .subscribe(
            data => {
                console.log("habitat");
                console.log(data);
                this.listHabitat = data
            }
        )
    }
    addHabitatToProduct(habitat : any){
        if(!this.checkHabitat(habitat))
        this.listLHabitatChoose.push(habitat)
        else {
            this.listLHabitatChoose = this.listLHabitatChoose.filter( (hbt : any)=> !(hbt === habitat) )
        }
        // this.getFormControl.habitatsList.value.set
    }
    checkHabitat(habitat : any){
        let idHabitat = this.listLHabitatChoose.map( (hbt:any) => hbt.idHabitats )
        if( idHabitat.includes(habitat.idHabitats) ) return true 
        return false
    }
    getLightAspect(){
        this.http.get('api/light-aspect/free/get-all-light-aspect1')
        .subscribe(
            data => {
                console.log("light");
                console.log(data);
                this.listLightAspect = data
            }
        )
    }
    getColor(){
        this.http.get("api/color/free/get-all-color")
        .subscribe(
            data => {
                console.log("clolor");
                console.log(data);
                this.getSize()
                this.colors = data
            }
        )
    }
    getSize(){
        this.http.get("api/size/free/get-all-size")
        .subscribe(
            data => {
                this.sizes = data
                this.createProductSize(this.colors.data[0])
                this.http.get<any>(`api/type/free/get-all-type1`)
                .subscribe(
                    datas =>{
                        this.categoryDatas = datas
                        console.log(datas);
                        this.formGroup.controls.category.setValue(datas?.data[0])
                        if(this.product.idProduct) {
                            this.http.post(`api/product/free/get-detail?status=${this.product.status}`,this.product)
                            .subscribe(
                                (product:any)=> {
                                    console.log(product);
                                    this.editForm(product.data)
                                    let i = 0
                                }
                            )
                        }
                    },
                    err => console.log(err)
        )
            }
        )
    }
    setHabitat(){
        this.getFormControl.habitatsList.setValue(this.listLHabitatChoose)
    }
    // get ________________________________________
    isSize(size :any,color : any ) : boolean{
        let productSize=[]
        for(let cl of this.productSize){
            if(cl.color === color) productSize = cl.productSize
        }
        for(let prds of productSize) {
            if(prds.size.idSize === size.idSize ) return true
        }
        return false
    }
  
    isColor(color :any,productSize : any ) : boolean{
        for(let prds of productSize) {
            if(prds.color === color ) return true
        }
        return false
    }
    setColor( productSize :any ){
        productSize.productSize.forEach(
            (prds:any) =>{ prds.color = productSize.color }
        )
        
    }
    public errorMsg =""
    setCategory(){
        
        // this.productSize = []
        // let color = this.getFormControl?.category?.value?.colorList[0]
        // if(!color){ this.errorMsg = `Danh mục ${this.getFormControl?.category?.value.categoryName} chưa có màu!`;return}
        // if(!this.getFormControl?.category?.value?.sizeList[0]){ this.errorMsg = `Danh mục ${this.getFormControl?.category?.value.categoryName} chưa có kích thước!`;return}
        // this.errorMsg = ""
        // this.createProductSize(color)
    }
    setProductSize(productSizeLst : Array<any>){
        this.colors?.data.forEach((color:any)=>{
            let productSize = [""]
            productSize = []
            productSizeLst.forEach( (prds:any)=>{
                if(color.id === prds.color.id) {
                    let isPush = true 
                    this.sizes.data.forEach(
                        (size : any)=> { 
                            if(size?.idSize == prds?.size?.idSize)
                            { 
                                prds.size = size
                                if(!this.isExitsProductSize(prds,productSize))
                                prds.status = 1
                                else isPush = false
                            }
                        }
                    )
                    if(isPush)
                    productSize.push(prds)
                }
            })
            if(productSize.length > 0)
            this.productSize.push({
                color:color,
                productSize:productSize
            })
        })
       
    }
    isExitsProductSize(prods: any,listProductSize : any){
        console.log(this.productSize);
        
        for( const prd of listProductSize ){
            // console.log(prd);
            if( prods?.size?.idSize === prd?.size?.idSize  ) return true
        }
        return false
    }
    setPrice(productSize : any , input : any, tbErrror  : any){
        let value = input.value 
        console.log(value);
        
        if(value < 1 || value > 999999999 || !value)
        {
            input.classList.add("outline-danger");
            tbErrror.hidden = false
        }else{
            productSize.price = input.value
            input.classList.remove("outline-danger");
            tbErrror.hidden= true
        }
    }
    setQuantity(productSize : any, input :any,tbErrror : any){
        let value = input.value 
        console.log(value);
        
        if(value < 0 || value > 1000000 || !value)
        {
            input.classList.add("outline-danger");
            tbErrror.hidden = false
        }else{
            productSize.quantity = input.value
            input.classList.remove("outline-danger");
            tbErrror.hidden = true
        }
        
    }
    getProducts(){
        this.pageMethod= "getProducts"
        this.loading = true
        this.http.get<any>(`api/product/free/get-all-product1?currentPage=${this.pageNumber}`)
        .subscribe(
            datas => {
                this.datas = datas
                console.log(datas);
                this.loading = false
            }
        )
    }
    showMsg(msg : string){
        this.alertTB.show = true
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.show = false
            this.alertTB.msg = ""
        },5000)
    }
    // resetForm(){
    //     this.productSize = []
    //     this.formGroup.reset()
    //     this.formGroup.controls['productSizeList'].setValue([])
    //     this.getFormControl['category'].setValue(this.categoryDatas?.content[0])
    //     this.createProductSize(this.getFormControl['category'].value.colorList[0])
    //     this.fileImages = []
    //     this.showImage = []
    //     this.actionEdit = false
    //     this.submited = false
    // }
    editForm(data : any){
        this.submited = false
        this.showImage = []
        this.fileImages = []
        var product= Object.assign({},data)
        // product.status = 1
        this.productSize = []
        console.log(this.categoryDatas);
        
        this.categoryDatas?.data?.forEach((ctg : any) =>  { if(ctg.idType === product?.type?.idType) product.category = ctg })
        this.formGroup.patchValue(product)
        this.getFormControl.lightAspect.setValue(product.lightAspect.idLightAspect)
        this.listLHabitatChoose.push(...this.listHabitat.data.filter(
            (hbt:any)=> {
                for(const hbt2 of product.habitatsList){
                    if(hbt.idHabitats === hbt2.idHabitats) return hbt2
                }
            }
        ))
        this.setProductSize(product.productSizeList)
    }
    
    setFiles(event : any,prods : any,index : number){
        console.log(prods);
        
        console.log(event.target.files);
        this.fileImages[index] = (event.target.files[0])
        console.log(this.fileImages);
        
        this.getFileRender(index,prods).readAsDataURL(event.target.files[0])
    }
 
    getFileRender(index : number,prds : any){
        var fileRender = new FileReader();
        fileRender.onload = e => {
            this.file = e.target?.result
            // this.file = this.file.substring(this.file.indexOf(",")+1,this.file.length);
            prds.imagePath = this.file.substring(this.file.indexOf(",")+1,this.file.length);
            // this.showImage[index] = this.file
        }
        return fileRender;
    }
    countHidenButtonDash(productSize : any){
        let count = 0;
        for( const prds  of  productSize){
            if(prds.status == 1) count ++;
        }
        return count;
    }
    ngOnInit() {
        // get datas category 
        this.product.idProduct = this.route.snapshot.paramMap.get("id")
        this.product.status = this.route.snapshot.paramMap.get("ban-lai") === "ban-lai" ? 2 : 1
        this.init()
        console.log(this.product);
        if(this.product.idProduct)
        this.actionEdit = true

        
    }
    
    validated() : boolean{
        let errNumber = false
        let imgBoolean = false
        // this.showImage.forEach((img:any)=>{
        //     if(!img) imgBoolean = true
        // })
        // if(this.showImage.length < 1) imgBoolean = true
        this.productSize.forEach((prds1:any)=>{
            prds1.productSize.forEach((prds2:any)=>{
                if( !prds2.imagePath ) imgBoolean = true
                if(prds2.price < 0 || prds2.quantity < 0){
                    errNumber  = true
                    this.error = true
                    return
                }
                
            })
        })
        console.log("img"+imgBoolean);
        console.log(errNumber);
        
        return !(
            this.getFormControl?.productName?.errors || 
            this.getFormControl?.category?.errors || 
            this.getFormControl?.lightAspect?.errors || 
            imgBoolean || errNumber || this.listLHabitatChoose.length < 1

            )
    }
    alertDeleteImage(index : number, prds : any){
        this.alert.index = index;
        this.alert.productSize = prds
    }
    alertDeleteProduct(product : any){
        this.deleteProDuct = product
    }
    deleteImage(){
        this.loading = true
        const index = this.alert.index
        // console.log(this.alert);
            this.alert.productSize.imagePath = ""
            this.loading = false
        // this.alert.index = 
    }
    deleteProductSize(pros : any , arrPrds : any){
        for(let i = 0 ; i < arrPrds.length ; i++){
            if(arrPrds[i] === pros) {
                if(arrPrds[i].idProductSize){
                    arrPrds[i].status = 2
                    this.productSizeDelete.push(arrPrds[i])
                }
                // else
                arrPrds.splice(i,1)
            }
        }
    }
    deleteColor(color : any , arrColor : any ){
        for(let i = 0 ; i < arrColor.length ; i++){
            if(arrColor[i] === color) {
                // arrColor[i].color.status = 2
                arrColor[i]?.productSize.forEach(
                    (prds:any)=> {
                        if(prds.idProductSize) {
                            prds.status = 2
                            this.productSizeDelete.push(prds)
                        }
                    }
                )
                arrColor.splice(i,1)
            }
        }
        // arrColor.p
    }
    // convertToBlob(bytes : any,index:number) : any {
    //     var url = `data:image/png;base64, ${bytes}`
    //     return  fetch(url).then(data => {
    //         data.blob().then( data =>{
                
    //             this.getFileRender(index).readAsDataURL(data); 
    //             this.fileImages.push(new File([data],"image.png"))
    //             console.log(this.fileImages);
    //             console.log(this.showImage);

    //         })
    //     });
    // }
    
    nextPage(input:any ,totalPages : number,callbackData : any ):boolean{
        if(this.pageNumber >= totalPages - 1 )return false
        else this.pageNumber = this.pageNumber*1 + 1
        console.log(this);
        return true
    }
    prePage(input:any ,totalPages : number):boolean{
        if(this.pageNumber - 1 < 0 )return false
        else this.pageNumber = this.pageNumber*1 - 1
        return true
    }
    getPage(input:any ,totalPages : number):boolean{
        if( input.value*1 > totalPages  ||  input.value*1  <= 0   )  {
            input.classList.add("class" , "outline-danger");
            input.value = this.pageNumber*1 + 1
            return false
        }
        else this.pageNumber = input.value - 1
        input.classList.remove("outline-danger");
        return true
    }
    
    // get getListCateSon(){
    //     return this.formGroup.controls['categoryDad']?.value?.categorySonlst
    // }
    // getImage(name :string ){
    //     return `${url.domain}/api/product/free/image/${name}`
    // }
}
