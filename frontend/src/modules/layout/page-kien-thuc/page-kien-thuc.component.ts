import { HttpClient } from '@angular/common/http';
import {
    ChangeDetectionStrategy,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';

@Component({
    selector: 'page-kien-thuc',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-kien-thuc.component.html',
    styleUrls: ['page-kien-thuc.component.scss'],
})
export class PageKienThucComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient
    ) {}
    ngOnInit() {

    }
    ngOnDestroy() {
    }
   

}
