import { NgModule } from "@angular/core";
import {RouterModule, Routes } from "@angular/router";

import { LayoutModule } from "./layout.module";
import { PageCartComponent } from "./page-cart/page-cart.component";
import { PageHomeComponent } from "./page-home/page-home.component";
import { PageInfoProductComponent } from "./page-info-product/page-info-product.component";
import { PageListProductComponent } from "./page-list-product/page-list-product.component";
import { PagePayComponent } from "./page-pay/page-pay.component";
import { PageProfileComponent } from "./page-profile/page-profile.component";
import { AuthGuard } from "@modules/auth/guards"; 
import { PageSuccessHomeComponent } from "./page-success/page-success.component";
import { PageErrorHomeComponent } from "./page-error/page-error.component";
import { PageContactComponent } from "./page-contact/page-contact.component";
import { PageKienThucComponent } from "./page-kien-thuc/page-kien-thuc.component";
export const ROUTER : Routes = [
    { path:"",redirectTo:"home"},
    { path:"home",component: PageHomeComponent},
    { path:"info-product/:idCate/:id",component: PageInfoProductComponent},
    { path:"cart",component: PageCartComponent, canActivate : [AuthGuard]},
    { path:"pay",component: PagePayComponent,canActivate: [AuthGuard]},
    { path:"list-product/:idCate",component: PageListProductComponent,canActivate: []},
    { path:"list-product",component: PageListProductComponent,canActivate: []},
    { path:"profile",component: PageProfileComponent,canActivate: [AuthGuard]},
    { path:"vnpay/success",component: PageSuccessHomeComponent,canActivate: [AuthGuard]},
    { path:"vnpay/error",component: PageErrorHomeComponent,canActivate: [AuthGuard]},
    { path:"contact",component: PageContactComponent,canActivate: []},
    { path:"kien-thuc",component: PageKienThucComponent,canActivate: []},
]

@NgModule({
    imports: [LayoutModule, RouterModule.forChild(ROUTER)],
    exports: [RouterModule],
})
export class LayoutRouterModule {}

