import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LayoutFooterComponent } from "./header-footer/layout-footer/layout-footer.component";
import { LayoutHeaderComponent } from "./header-footer/layout-header/layout-header.component";
import { LayoutComponent } from "./header-footer/layout/layout.component";
import {  PageHomeComponent } from "./page-home/page-home.component";
import { PageInfoProductComponent } from "./page-info-product/page-info-product.component";
import { ProductListComponent } from "./header-footer/component/product-list/product-list.component";

import * as service from "./service"
import { PageCartComponent } from "./page-cart/page-cart.component";
import { PagePayComponent } from "./page-pay/page-pay.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthGuard } from "@modules/auth/guards";
import { PageListProductComponent } from "./page-list-product/page-list-product.component";
import { PageProfileComponent } from "./page-profile/page-profile.component";
import { PageSuccessHomeComponent } from "./page-success/page-success.component";
import { PageErrorHomeComponent } from "./page-error/page-error.component";
import { PageContactComponent } from "./page-contact/page-contact.component";
import { PageKienThucComponent } from "./page-kien-thuc/page-kien-thuc.component";

@NgModule({
    imports:[
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule 
    ],
    declarations:[
        LayoutHeaderComponent,
        LayoutFooterComponent,
        PageHomeComponent,
        LayoutComponent,
        PageInfoProductComponent,
        ProductListComponent,
        PageCartComponent,
        PagePayComponent,
        PageListProductComponent,
        PageProfileComponent,
        PageSuccessHomeComponent,
        PageErrorHomeComponent,
        PageContactComponent,
        PageKienThucComponent
    ],
    exports:[],
    providers:[ ...service.service , AuthGuard ]
})

export class LayoutModule {}