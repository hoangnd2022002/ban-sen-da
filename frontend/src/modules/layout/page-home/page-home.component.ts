import { HttpClient } from '@angular/common/http';
import {
    ChangeDetectionStrategy,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { url } from '@modules/domain';
// import {url}  from 'environments/environment';

@Component({
    selector: 'page-home',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-home.component.html',
    styleUrls: ['page-home.component.scss'],
})
export class PageHomeComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient
    ) {}
    ngOnInit() {
       this.getProducts(1)
       this.getCategorys()
    //    addEventListener("scroll", (event) => {console.log(event);
    //    });
      
    }
    ngOnDestroy() {
    }
    public datasCatgories : any = []
    public loading = false
    public datasProduct : any 
    public pageNumber : number = 0
    getProducts(pageNumber : number){
        this.pageNumber = pageNumber
        this.loading= true
        this.http.get<any>(`api/product/free/get-all-product1?pageSize=12&currentPage=${this.pageNumber-1}`)
        .subscribe(
            (datas:any) => {
                this.loading = false
                this.datasProduct = datas
                let arrPage = []
                for(let i = 1; i <= this.datasProduct.totalPages; i++ ){
                    arrPage.push(i)
                }
                this.datasProduct.totalPages = arrPage
                console.log(this.datasProduct);
                
            }
        )
    }
    
    getCategorys(){
        this.http.get<Object[]>("api/type/free/get-all-type1")
        .subscribe(
            (datas:any) => {
                this.loading = false
                this.datasCatgories = datas
                // datas.content.sort((a:any,b:any)=>{
                //     if(a.idCategory < b.idCategory) return -1
                //     if(a.idCategory > b.idCategory) return 1
                //     return 0
                // })
                console.log(this.datasCatgories);
            }
        )
    }
    getImage(name : string){
        return `${url.domain}/api/product/free/image/${name}`;
    }

}
