import { HttpClient } from '@angular/common/http';
import {
    ChangeDetectionStrategy,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';

@Component({
    selector: 'page-error',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-error.component.html',
    styleUrls: ['page-error.component.scss'],
})
export class PageErrorHomeComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient
    ) {}
    ngOnInit() {
    //    $(".lazy").lazy();
        localStorage.removeItem("ban-hang")
    }
    ngOnDestroy() {
    }
   

}
