import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { CountCart } from '../service';

@Component({
    selector: 'page-success',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-success.component.html',
    styleUrls: ['page-success.component.scss'],
})
export class PageSuccessHomeComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public router: Router,
        public countCart : CountCart
    ) {}
    public timeOut = 10000
    public count = 10
    ngOnInit() {
        this.getOrderDetail()
    //    $(".lazy").lazy();
        // setTimeout(()=>{
        //     this.router.navigate(["/profile"])
        // },this.timeOut)
        this.datHang(this.order)
    }
    public order : any={}

    getOrderDetail(){
        let stringOrder = localStorage.getItem("pay-order")
        console.log("pay-order : "+ stringOrder);
        
        let order
        try {
            order = JSON.parse( stringOrder === null ? "" : stringOrder + "" );
        } catch (error) {
            
        }
        if( !order || order?.orderDetailsList?.length < 0 || order === ""  ){

            alert("Chưa có gì để thanh toán!")
            this.router.navigate(["/cart"])
        }
        this.order = order
    }

    datHang(order : any){
        console.log(order);
        this.http.post(`api/order-xac-nhan/cho-xac-nhan/vn-pay`,order)
        .subscribe(
            data => {
                console.log(data);
                localStorage.removeItem("order")
                localStorage.removeItem("pay-order")
                location.reload
                this.countCart.getCountCart()
                // this.router.navigate(["/profile"])
            }
        )
    }

    ngOnDestroy() {
    }
   

}
