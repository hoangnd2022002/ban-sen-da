import {  Injectable } from "@angular/core";


@Injectable()
export class RoleService {
    get roleId() : string{
        return decodeURIComponent(localStorage.getItem("role")+"")
    }

    get isQuanLy() : boolean{
        return JSON.parse(this.roleId)?.ids?.includes(1)
    }
    get isNhanVien() : boolean{
        return JSON.parse(this.roleId)?.ids?.includes(2)
    }
    get isNguoiDung() : boolean{
        return JSON.parse(this.roleId)?.ids?.includes(3)
    }
    set saveRole(role : any){
        localStorage.setItem("role",encodeURIComponent(JSON.stringify({ids:role})+""))
    }
}