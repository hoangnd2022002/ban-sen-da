import { CartService } from "./cart.service"
import { CountCart } from "./count-cart.service"
import { Count } from "./count.service "
import { ImageService } from "./image.service"
import { RoleService } from "./role.service"

export const service = [ImageService,CartService,RoleService,CountCart,Count]

export * from "./image.service"
export * from "./cart.service" 
export * from "./role.service" 
export * from "./count-cart.service"
export * from "./count.service "