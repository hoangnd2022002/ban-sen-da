import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";


@Injectable()
export class CartService{
    constructor(
        public http:HttpClient
    ){}

    getCart() : Observable<any>{
        return this.http.get(`api/order-xac-nhan/get-cart`)
    }
    postCart(data: any): Observable<any>{
        return this.http.post(`api/order-xac-nhan/them-vao-gio`,data)
    }

    deleteCart(id : number){
        return this.http.delete(`api/order-xac-nhan/delete-product-in-cart/${id}`)
    }

    update(odt : any ):Observable<any>{
        return this.http.put("api/order-xac-nhan/update-quantity",odt)
    }
}
