import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";


@Injectable()
export class CountCart {
    constructor(
        public http:HttpClient
    ){

    }
    public count = 0
    
    getCountCart(){
            this.http.get(`api/order-xac-nhan/so-luong-gio-hang-theo-user`)
            .subscribe(
                (data:any)=>{
                    this.countCart = data > 99 ? '99+' : data
                    console.log( "count"+ data);
                }
            )
    }
    set countCart(nb:number){
        this.count = nb
    }
    get countCart(){
        return this.count
    }
}