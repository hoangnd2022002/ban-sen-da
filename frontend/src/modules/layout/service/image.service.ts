import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { url } from "@modules/domain";
// import  {url}  from "environments/environment";


@Injectable()
export class ImageService {
    constructor(public http : HttpClient){
        
    }
    getImagesByIdProduct(id: number) {
        return this.http.get<any[]>(`api/product/free/products/${id}/image`)
    }
    getByUrl(id : any ) {
        return `${url.domain}/api/product/free/products/ima/${id}`;
    }
}