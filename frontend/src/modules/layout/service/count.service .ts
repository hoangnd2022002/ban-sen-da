import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SideNavItem } from "@testing/mocks";


@Injectable()
export class Count {
    constructor(
        public http:HttpClient
    ){

    }
    public countKiemDuyet = 0
   
    gresetCountKiemDuyetn(navItem : SideNavItem){
           
            this.http.get(`api/order-xac-nhan/so-luong-chua-xac-nhan`)
            .subscribe(
                (data:any)=>{
                    navItem.count = data > 99 ? '99+' : data
                }
            )
    }
    getCountLienHe(navItem : SideNavItem){
        this.http.get(`api/contact/so-luong-chua-phan-hoi`)
        .subscribe(
            (data:any)=>{
                navItem.count = data > 99 ? '99+' : data
            }
        )
    }
    getCountHetSL(navItem : SideNavItem){
        this.http.get(`api/order-xac-nhan/so-luong-da-het-hang`)
        .subscribe(
            (data:any)=>{
                navItem.count = data > 99 ? '99+' : data
            }
        )
    }
   
    get getCountKiemDuyet(){
        return this.countKiemDuyet
    }
}