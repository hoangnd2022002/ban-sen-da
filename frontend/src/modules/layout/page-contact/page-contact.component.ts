import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@modules/auth/services';

@Component({
    selector: 'page-contact',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-contact.component.html',
    styleUrls: ['page-contact.component.scss'],
})
export class PageContactComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public router: Router,
        public auth:AuthService,
        public formBuild : FormBuilder
    ) {}
    public timeOut = 10000
    public count = 10
    public user : any= {}
    ngOnInit() {
       if(this.auth.isLogged()){
        this.http.get(`api/user/get-user`)
        .subscribe(
            (user:any) => {
                console.log(user);
                this.user = user.data
            }
        )
       }
    }

    public order : any={}
    public formGroup=this.formBuild.group({
        messageUser:["",Validators.required]
    })
    onSubmit(){
        this.submited = true
        if(!this.formGroup.controls.messageUser.errors)
        this.http.post(`api/contact/add-contact`,this.formGroup.value)
        .subscribe(
            data=>{
                console.log(data);
                
                this.submited = false
                this.showMsg("Đã gửi phản hổi")
            }
        )
    }
    public submited= false
    // public ress=""
    ngOnDestroy() {
    }
    public msg=""
    showMsg(msg : string){
        this.msg= msg
        
        const timeout = setTimeout(()=>{
            this.msg = ""
        },5000)
    }
   

}
