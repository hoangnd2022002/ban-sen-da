import { HttpClient } from '@angular/common/http';
import {
    ChangeDetectionStrategy,
    Component,
    Input,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { url } from '@modules/domain';
// import {url} from 'environments/environment';
// import  domain  from 'environments/environment';

@Component({
    selector: 'product-list',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './product-list.component.html',
    styleUrls: ['product-list.component.scss'],
})
export class ProductListComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public router : Router
    ) {}
    @Input() products : any[] = []
    @Input() col : number = 3
    ngOnInit() {
        console.log("prd");
        console.log(this.products);
       
    }
    ngOnDestroy() {
    }
    getImage(name : string){
        return `${url.domain}/api/product/free/image/${name}`;
    }
    getFrom(lstProductSize : any[]){
        let value = lstProductSize[0]?.price ;
        lstProductSize.forEach(prds => {
            if(prds?.price < value  ) value = prds?.price
        })
        return value
    }
    getTo(lstProductSize : any[]){
        let value = lstProductSize[0]?.price ;
        lstProductSize.forEach(prds => {
            if(prds?.price > value  ) value = prds?.price
        })
        return value
    }
    redirect(product:any){
        // this.router.navigate(['/info-product', product.category?.idCategory ,product?.idProduct ])
        this.router.navigateByUrl(`/info-product/${product.category?.idCategory}/${product?.idProduct}`)
    }

}
