import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';

@Component({
    selector: 'input-search',
    templateUrl: './input-search.component.html',
    styleUrls: ['input-search.component.scss'],
})
export class InputSearchComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient
    ) {}
    public datas : Object = {}
    getProduct(){
        this.http.get<any>(`api/product/free/get-all-product1`)
        .subscribe(
            datas => this.datas = datas
        )
    }
    ngOnInit() {
       
    }
    ngOnDestroy() {
    }
}
