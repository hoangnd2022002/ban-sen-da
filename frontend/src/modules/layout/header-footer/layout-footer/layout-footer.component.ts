import {
    ChangeDetectionStrategy,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';

@Component({
    selector: 'layout-footer',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './layout-footer.component.html',
    styleUrls: ['layout-footer.component.scss'],
})
export class LayoutFooterComponent implements OnInit, OnDestroy {
    
    constructor(
    ) {}
    ngOnInit() {
       
    }
    ngOnDestroy() {
    }
}
