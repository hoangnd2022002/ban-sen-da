import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';

@Component({
    selector: 'layout',
    templateUrl: './layout.component.html',
    styleUrls: ['layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient
    ) {}
    public datas : Object = {}
    getProduct(){
        this.http.get<any>(`api/product/free/get-all-product1`)
        .subscribe(
            datas => this.datas = datas
        )
    }
    ngOnInit() {
       
    }
    ngOnDestroy() {
    }
}
