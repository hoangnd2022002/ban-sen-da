import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
// import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@modules/auth/services';
import { CountCart, RoleService } from '@modules/layout/service';
import Swal from 'sweetalert2';
@Component({
    selector: 'layout-header',
    templateUrl: './layout-header.component.html',
    styleUrls: ['layout-header.component.scss'],
})
export class LayoutHeaderComponent implements OnInit, OnDestroy {
    
    constructor(
        public roleSv : RoleService,
        public auth : AuthService,
        public http: HttpClient,
        public count : CountCart,
        public formBuild:FormBuilder
    ) {}
    ngOnInit() {
        if(this.auth.isLogged())
        this.count.getCountCart()
        this.getCategorys()
    }
    public categories = []
    getCategorys(){
        this.http.get<Object[]>("api/type/free/get-all-type1")
        .subscribe(
            (datas:any) => {
                this.categories = datas.data
                console.log(this.categories);
            }
        )
    }
    public formGroup = this.formBuild.group({
        oldPass:["",Validators.required],
        password:["",Validators.required]
    })
    validate(){
        return !(
            this.formGroup.controls.password.errors 
            ||  this.formGroup.controls.oldPass.errors 
        )
    }

    public errMkcu= false
    public submited = false
    doiMK(){
        this.submited = true
        if(this.validate()){
            this.http.post(`api/user/free/change-pass`,this.formGroup.value)
            .subscribe(
                (data:any)=>{
                    console.log(data);
                    if(data.message === "FAIL_USER") this.errMkcu = true 
                    else{
                        this.errMkcu=false
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Đã đổi mật khẩu',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                }
            )
        }
    }   
    public timeOut :any;
    public loading = false
    public listSearch=[]
    search(txtSearch:string){
        this.loading = true
        if(this.timeOut) clearTimeout(this.timeOut)
        this.timeOut = setTimeout(()=>{
            txtSearch = txtSearch.trim();
            if(txtSearch.length > 0){
                this.http.post('api/product/free/get-all-product-by-keyword',{keyword:txtSearch})
                .subscribe(
                    (data:any) => {
                        console.log("search");
                        
                        console.log(data);
                        this.listSearch = data.content
                    },
                    ()=>{},
                    ()=>  this.loading= false
                )
            }else {
                this.loading= false
                this.listSearch = []}
        },1000)
    }
    clearsearch(input:any){
        this.listSearch=[]
        input.value =""
    }

    getFrom(lstProductSize : any[]){
        let value = lstProductSize[0]?.price ;
        lstProductSize.forEach(prds => {
            if(prds?.price < value  ) value = prds?.price
        })
        return value
    }
    getTo(lstProductSize : any[]){
        let value = lstProductSize[0]?.price ;
        lstProductSize.forEach(prds => {
            if(prds?.price > value  ) value = prds?.price
        })
        return value
    }
    ngOnDestroy() {
    }
}
