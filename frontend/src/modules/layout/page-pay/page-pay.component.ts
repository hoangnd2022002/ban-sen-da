import { HttpClient } from '@angular/common/http';
import json from  "assets/json/local.json"
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    selector: 'page-pay',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-pay.component.html',
    styleUrls: ['page-pay.component.scss'],
})
export class PagePayComponent implements OnInit, OnDestroy {

    constructor(
        public http : HttpClient,
        public route : ActivatedRoute,
        public router : Router,
        public formBuild : FormBuilder
    ) {}
    public formGroup = this.formBuild.group({
        idOrder :[""],
        thanh_pho:["",[Validators.required]],
        huyen:["",[Validators.required]],
        xa:["",[Validators.required]],
        phone:["",[Validators.required,Validators.pattern("[0]{1}[1-9]{8,10}")]],
        moTa:[""],
        name:[""]
    })
    public shipToltal  = 0
    public thanh_pho = []
    public huyen = []
    public xa = []
    public order = {orderDetailsList:[],idOrder:""}
    public submited = false
    public service = [] 
    ngOnInit() {
        this.getOrderDetail()
        this.getThanhPho()
    }
    ngOnDestroy() {
    }
    getThanhPho(){
        fetch("https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/province",{
            method:"get",
            headers:{
                "Content-Type": "application/json",
                "Token":"60ec3693-e4cb-11ed-ab31-3eeb4194879e"
            }
        })
        .then(
            data=>data.json()
        )
        .then(
            data => this.thanh_pho = data.data
        )
    }
    getHuyen(){
        console.log(this.thanh_pho);
        this.formGroup.controls.huyen.reset()
        this.formGroup.controls.xa.reset()
        fetch("https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/district",{
            method:"post",
            "headers":{
                "Content-Type": "application/json",
                "Token":"60ec3693-e4cb-11ed-ab31-3eeb4194879e",
            },
            body:JSON.stringify({
                province_id:this.getFormControl("thanh_pho").ProvinceID
            })
            
        })
        .then(
            data=>data.json()
        )
        .then(
            data => this.huyen = data.data
        )
    }
    
    getDichVu(){
        console.log(this.getFormControl("huyen"));
        this.formGroup.controls.xa.reset()
        fetch("https://dev-online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/available-services",{
            method:"post",
            "headers":{
                "Content-Type": "application/json",
                "Token":"60ec3693-e4cb-11ed-ab31-3eeb4194879e",
            },
            body:JSON.stringify({
                shop_id: 124067,
                from_district:1639,
                to_district:this.getFormControl("huyen").DistrictID
            })
        })
        .then(
            data=>data.json()
        )
        .then(
            data => console.log(data)
        )
    }
    getXa(){
        this.getDichVu()
        console.log(this.getFormControl("huyen"));
        this.formGroup.controls.xa.reset()
        fetch("https://dev-online-gateway.ghn.vn/shiip/public-api/master-data/ward?district_id=",{
            method:"post",
            "headers":{
                "Content-Type": "application/json",
                "Token":"60ec3693-e4cb-11ed-ab31-3eeb4194879e",
            },
            body:JSON.stringify({
                district_id:this.getFormControl("huyen").DistrictID
            })
        })
        .then(
            data=>data.json()
        )
        .then(
            data => this.xa = data.data
        )
    }
    // getPhiVanChuyen(){
    //     fetch("https://dev-online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/fee",{
    //         method:"post",
    //         "headers":{
    //             "Content-Type": "application/json",
    //             "Token":"60ec3693-e4cb-11ed-ab31-3eeb4194879e",
    //             "ShopId": "124067",
    //         },
    //         body:JSON.stringify({
    //             from_district_id:1639,
    //             service_id:53320,
    //             service_type_id:null,
    //             to_district_id:this.getFormControl("huyen").DistrictID,
    //             to_ward_code:this.getFormControl("xa").WardCode,
    //             height:50,
    //             length:20,
    //             weight:200,
    //             width:20,
    //             insurance_value:this.getTotal,
    //             coupon: null
    //         })
    //     })
    //     .then(
    //         data=>{
    //             return data.json()
    //         }
    //     )
    //     .then(
    //         data => {
    //             console.log(data);
    //             this.shipToltal = data.data.total
    //         }
    //     )
    // }
    getOrderDetail(){
        let stringOrder = localStorage.getItem("order")
        let order
        try {
            order = JSON.parse( decodeURIComponent(stringOrder === null ? "" : stringOrder + "") );
        } catch (error) {
            
        }
        if( !order || order?.orderDetailsList?.length < 0 || order === ""  ){

            alert("Chưa có gì để thanh toán!")
            this.router.navigate(["/cart"])
        }
        order.orderDetailsList.forEach((odt:any) => {
            this.http.get<any>(`api/order-xac-nhan/get-order-detail-by-id/${odt.idOrderDetail}`)
            .subscribe(
                data => {
                    console.log(data);
                    odt = data
                }
            )
        })
        this.setForm(order)
        this.order = order
    }
    setForm(order :any ){
        this.formGroup.controls["phone"].setValue(order.user.phone)
        this.formGroup.controls["name"].setValue(order.user.name)
        this.formGroup.patchValue(order)
    }
    get getTotal(){
        let tong = 0
        this.order?.orderDetailsList.forEach((odt:any) => {
            tong += odt?.quantity * odt.productSize?.price
        })
        return tong
    }
    public errAmount = false
    
    datHang(){
        console.log(this.getFormControl("xa"));
        
        const {
            ["idOrder"]:idOrder,
            ["orderDetailsList"]:orderDetailsList,

        } = this.order
        let ghiChu = this.formGroup.controls["moTa"].value
        let diaChi = this.getFormControl("xa").WardName+", " + this.getFormControl("huyen").DistrictName+", " + this.getFormControl("thanh_pho").ProvinceName
        let order = {
            diaChi:diaChi,
            moTa:"Số điện thoại: " + this.formGroup.controls["phone"].value +",Ghi chú: "+ ghiChu ? ghiChu : 'Không có',
            idOrder:idOrder,
            orderDetailsList:orderDetailsList
        }
        console.log(JSON.stringify(order));
        console.log(order);
        console.log(this.validate());
    
        if(this.validate() && this.methodPay==="NH" ){
            this.http.post(`api/order-xac-nhan/cho-xac-nhan`,order)
            .subscribe(
                data => {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đặt hàng thành công ',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    console.log(data);
                    localStorage.removeItem("order")
                    location.reload
                    this.router.navigate(["/profile"])
                }
            )
            return
        }
        let amount = 0
        order.orderDetailsList.forEach((odt:any)=>{
            amount += odt.productSize.price * odt.quantity
        })
        console.log(amount);
        
        if(this.validate() && this.methodPay==="VDT" && amount >= 10000 ){
            this.errAmount = false
            
            this.http.post(`api/vnpay`,{
                "amount":amount,
                "idOrder":order.idOrder,
                "orderDetailsList":order.orderDetailsList
            })
            .subscribe(
                (data:any) => {
                    console.log(data);
                    // localStorage.removeItem("order")
                    localStorage.setItem("pay-order",JSON.stringify(order))
                    location.reload
                    window.location = data.url
                    // this.router.navigate(["/profile"])
                }
            )
        }else this.errAmount =true
    }
    getFormControl( value : string ){
        return this.formGroup.controls[value].value
    }
    getFormError(name : any){
        return this.formGroup.controls[name].errors
    }
    public methodPay = "NH"
    validate(){
        this.submited = true
      
        return !(
            this.getFormError("thanh_pho") ||
            this.getFormError("huyen") ||
            this.getFormError("xa") ||
            this.getFormError("phone") 
        )
    }
}
