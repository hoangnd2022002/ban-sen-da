import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from '../service';
import { url } from '@modules/domain';
// import {url} from 'environments/environment';
// import { domain } from 'environments/environment';

@Component({
    selector: 'page-list-product',
    templateUrl: './page-list-product.component.html',
    styleUrls: ['page-list-product.component.scss'],
    
})
export class PageListProductComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public route : ActivatedRoute,
        public cartService : CartService,
        public router : Router
    ) {}
    ngOnInit() {
        this.idCate = this.route.snapshot.paramMap.get('idCate')
        this.getCategorys()
        this.getHabitats()
        this.getLightAspect()
        this.route.params.subscribe(val => {
            this.idCate = val.idCate
            this.getProduct(1);
        });
        // this.filterPro()
    }
    public idCate : any
    public products :any = {content:[]}
    public categories : any= []
    public loading = true
    public txtSearch=""
    public valueMin : any= 0
    public valueMax : any= 100000
    public habitats :any = []
    public lightAspect: any = []
    public habitatChoose : any = []
    public lightAspectChoose : any  = {}
    ngOnDestroy() {
    }
    public pageNumber=1
    // getImage(id : number){
    //     return `${url.domain}/api/product/free/products/ima/${id}`;
    // }
    validate(){
        if(this.valueMin*1 > this.valueMax*1 ){
            let vl = this.valueMax 
            this.valueMax = this.valueMin 
            this.valueMin = vl
        }
        if(this.valueMax < 0)
        return false 
        if(this.valueMin < 0)
        return false
        return true
    }
    setMin(min : any){
        if( min >= 0) this.valueMin = min
        else this.valueMin = 0
    }
    setMax(max : any){
        if(max >= 0) this.valueMax = max
        else this.valueMax = 0
    }
    reset(){
        this.getProduct(1);
    }
    boLoc(){
        this.valueMax = null
        this.valueMin = null
        this.lightAspectChoose = null 
        this.habitatChoose = []
        this.idCate = null
        // this.router.navigate(["/list-product"])

        this.getProduct(1)
    }
    getProduct(pageNumber : number){
        if(this.validate()) {
            this.pageNumber = pageNumber
            console.log("idcate" , this.idCate);
            
            this.http.post(`api/product/free/get-all-product-by-price?currentPage=${pageNumber-1}&pageSize=12`,
                {
                    // keyword:this.txtSearch,
                    keyword1: this.valueMin,
                    keyword2: this.valueMax,
                    idType: this.idCate ? this.idCate : null,
                    idLightAspect : this.lightAspectChoose?.idLightAspect ?  this.lightAspectChoose?.idLightAspect : null,
                    idHabitat: this.habitatChoose.length > 0 ?  this.habitatChoose.map( (hbt : any)=> hbt.idHabitats ) : null
                }
            )
            .subscribe(
                (data:any) => {
                    console.log(data);
                    this.products=data
                    let arrPage = []
                    for(let i = 1; i <= this.products.totalPages; i++ ){
                        arrPage.push(i)
                    }
                    this.products.totalPages = arrPage
                }
            )}
    }
    // resetFilter()
    // filterPro(pageNumber : any){
    //     this.pageNumber = pageNumber
    //     this.http.post(`api/product/free/get-all-product-by-price?currentPage=${pageNumber-1}&pageSize=1`,
    //         {
    //             keyword:this.txtSearch,
    //             keyword1: this.valueMin ,
    //             keyword2: this.valueMax,
    //             idCategory: this.idCate
    //         }
    //     )
    //     .subscribe(
    //         (data:any) => {
    //             console.log(data);
    //             this.products = data
    //             let arrPage = []
    //             for(let i = 1; i <= this.products.totalPages; i++ ){
    //                 arrPage.push(i)
    //             }
    //             this.products.totalPages = arrPage
    //         }
            
    //     )
    // }
    public listSearch : any= []
    public timeOut : any
    search(txtSearch:string){
        this.loading = true
        if(this.timeOut) clearTimeout(this.timeOut)
        this.timeOut = setTimeout(()=>{
            txtSearch = txtSearch.trim();
            if(txtSearch.length > 0){
                this.http.post('api/product/free/get-all-product-by-keyword',{keyword:txtSearch})
                .subscribe(
                    (data:any) => {
                        // console.log(data);
                        this.listSearch = data.content
                        
                    },
                    ()=>{},
                    ()=>  this.loading= false
                )
            }else {
                this.loading= false
                this.listSearch = []}
        },1000)
    }
    getCategorys(){
        this.http.get<Object[]>("api/type/free/get-all-type1")
        .subscribe(
            (datas:any) => {
                this.loading = false
                this.categories = datas.data
                console.log(this.categories);
                // this.categories.sort((a:any,b:any)=> {
                //     if(a.idCategory > b.idCategory) return 1
                //     if(a.idCategory < b.idCategory) return -1
                //     return 0
                // });
                
                if(!this.idCate) this.idCate = this.categories[0].idType
            }
        )
    }
    setLightAspect(light :any ){

        if(light !== this.lightAspectChoose) this.lightAspectChoose = light 
        else this.lightAspectChoose = null
        this.getProduct(this.pageNumber)
    }
    setCategory(id :any ){
        if(id != this.idCate) this.idCate = id 
        else this.idCate = null
        this.getProduct(this.pageNumber)
    }
    setHabitatChoose(habitat : any) {
        let push = true
        this.habitatChoose = this.habitatChoose.filter(
            (hbt:any)=> {
                if(hbt === habitat) {
                    push = false ; return false}
                else return true
            }
        )
        if(push) this.habitatChoose.push(habitat)
        this.getProduct(this.pageNumber)
    }
    isHabitatsChoose(habitat : any){
        for(let hbt of this.habitatChoose ){
            if(hbt === habitat) return true
        }
        return false
    }
    getHabitats(){
        this.http.get("api/habitat/free/get-all-habitat1")
        .subscribe(
            data => {
                console.log(data);
                
                this.habitats = data}
        )
    }
    getLightAspect(){
        this.http.get("api/light-aspect/free/get-all-light-aspect1")
        .subscribe(
            data => {
                console.log(data);
                
                this.lightAspect = data}
        )
    }
}
