import { HttpClient } from '@angular/common/http';
import {
    ChangeDetectionStrategy,
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { url } from '@modules/domain';
import Swal from 'sweetalert2';

@Component({
    selector: 'page-profile',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-profile.component.html',
    styleUrls: ['page-profile.component.scss'],
})
export class PageProfileComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public formBuild : FormBuilder
    ) {}
    ngOnInit() {
        this.getOrder(this.tabKD[0]);
        this.getUser()
    
    }
    public tab = "HD"
    public alertTB = {showSuccess:false,showError:false,msg:""}
    ngOnDestroy() {
    }
    public tabKD = [
        {id: 1,name:"Chưa xác nhận",api:{
            get:"api/order-xac-nhan/lay-order-chua-xac-nhan-user",
            // post:"api/order-xac-nhan/dang-cbi-hang",
            delete:"api/order-xac-nhan/da-huy"
        }},
        {id: 2,name:"Đang chuẩn bị hàng",api:{
            get:"api/order-xac-nhan/lay-order-da-xac-nhan-user ",
            // post:"api/order-xac-nhan/dang-giao"
        }},
        {id: 3,name:"Đang giao",api:{
            get:"api/order-xac-nhan/lay-order-dang-giao-user ",
            post:"api/order-xac-nhan/da-giao"
        }},
        {id: 4,name:"Đã giao",api:{
            get:"api/order-xac-nhan/lay-order-da-giao-user",
            post:"api/order-xac-nhan/da-nhan"
        }},
        {id: 5,name:"Đã nhận",api:{
            get:"api/order-xac-nhan/lay-order-da-nhan-user",
            post:"api/order-xac-nhan/"
        }},
        {id: 6,name:"Đã hủy",api:{
            get:"api/order-xac-nhan/lay-order-da-huy-user",
            // post:"api/order-xac-nhan/"
        }},
    ]
    public tabSelect : any;
    public order = {}
    public datasOrder = [{orderDetailList:[]}]
    getOrder(tabSelect : any){
        this.tabSelect = tabSelect
        this.http.get(tabSelect.api.get)
        .subscribe(
            (datas:any )=> {
                console.log(datas);
                console.log(tabSelect.api);
                this.datasOrder = datas
                datas.forEach(
                    (od:any)=>{
                         this.getOrderDetails(od)
                    }
                )
            }
        )
    } 
    selected(tabSelect : any){
        this.getOrder(tabSelect)
    }
    
    postRequest(api : string,order : any){
        console.log(api);
        
        console.log(order);
        this.http.post(api,order)
        .subscribe(
            
            data => {
                console.log(data);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Đã nhận đơn hàng này',
                    showConfirmButton: false,
                    timer: 1500
                  })
                this.getOrder(this.tabSelect)
            },
            err => {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Có lỗi!',
                    showConfirmButton: false,
                    timer: 1500
                  })
            }
        )
    }
    showMsg(success:boolean,err:boolean,msg : string){
        this.alertTB.showSuccess = success
        this.alertTB.showError = err
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.showError = false
            this.alertTB.showSuccess = false
            this.alertTB.msg = ""
        },5000)
    }
    public orderDetails = {}
    getOrderDetails(order : any){
        this.http.post("api/order-xac-nhan/get-orderDetail",order)
        .subscribe(
            (data:any) => { console.log(data);
                order.orderDetailList = data
             }
        )
    }

    //======================================================================== user password
    public submited = false
    public mkCu = ""
    public mkMoi=""
    public xacNhanMk = ""
    public  sexUser = 0
    forgotPassword(){
        this.submited = true
        if( this.mkCu !== this.mkMoi 
            && this.mkMoi === this.xacNhanMk 
            && this.mkMoi.length > 0 
            // && this.
        )
        this.http.get(`api/user/free/change-pass` , {} )
    }
    public nameUser="Tên người dùng"
    public formUser = this.formBuild.group({
        idUser:[""],
        name:["",[Validators.required,Validators.pattern("[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ]*")]],
        username:["",[Validators.required,Validators.pattern("[a-zA-Z0-9]*"),Validators.minLength(5),Validators.maxLength(15)]],
        phone:["",[Validators.required,Validators.pattern("[0]{1}[1-9]{8,10}")]],
        email:["",[Validators.required,]],
        sex:[0,[Validators.required]],
        password:["",[Validators.required,Validators.minLength(5),Validators.maxLength(15)]],
        role:[{}],
    })
    public editPass = false
    getUser(){
        this.http.get(`api/user/get-user`)
        .subscribe(
            (user:any) => {
                console.log("user");
                
                console.log(user);
                
                this.formUser.patchValue(user.data)
                console.log(this.formUser.value);
                this.sexUser = user.data.sex
                this.nameUser = user.data.name
            }
        )
    }
    formValueUser(name: string){
        return this.formUser.controls[name]?.value
    }
    public  errorAPI =""
    submitUser(){
        console.log("submit");
        console.log(this.formUser.value);
        
        if(!this.validatedUser()) return
        let formUser = this.formUser.value 
        if(!this.editPass) formUser.password = null
        this.http.post(`api/user/edit-user`,formUser)
        .subscribe(
            (user:any) => {
                if(user?.data){
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đã cập nhật thông tin',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    this.errorAPI = ""
                    this.sexUser = user.data.sex
                }else{
                    this.errorAPI = user.message
                }
            }
        )
    }
    get errorsForm(){
        return this.formUser.controls
    }
    validatedUser() : boolean{
        return !(
            this.formUser?.controls?.name?.errors || 
            this.formUser?.controls?.username?.errors || 
            this.formUser?.controls?.phone?.errors || 
            this.formUser?.controls?.email?.errors ||
            // this.formUser?.controls?.password?.errors ||
            this.formUser?.controls?.sex?.errors
            )
    }
    getImage(name:string){
        return `${url.domain}/image/${name}`
    }
}

