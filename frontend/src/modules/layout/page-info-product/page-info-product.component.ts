import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CartService, CountCart } from '../service';
import { url } from '@modules/domain';
import Swal from 'sweetalert2';
// import  {url}  from 'environments/environment';

@Component({
    selector: 'page-info-product',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './page-info-product.component.html',
    styleUrls: ['page-info-product.component.scss'],
})

export class PageInfoProductComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public route : ActivatedRoute,
        public cartService : CartService,
        public formBuild : FormBuilder,
        public count:CountCart
    ) {}
    // public form
    public sizeChoose :any = {idSize:0}
    public seeMore = false
    public colorChoose : any= {id : 0}
    public loading = false
    public alertTB : any={showSuccess: false,showError:false,msg:""}
    public error = false
    // public productSize = {}
    public category : any= {colorList:[{id:-1}],sizeColor:[{idSize:-1}]}
    ngOnInit() {
        var idProduct = this.route.snapshot.paramMap.get('id');
        var idCate =  this.route.snapshot.paramMap.get('idCate');
        this.getColor()
        this.getSize()
        this.route.params.subscribe(val => {
            this.getProduct(val.id)
            // this.getProducts(val.idCate);
            // this.getImg(val.id)
            this.colorChoose = {id:-1}
            this.sizeChoose = {idSize:-1}
            window.scrollTo({
                top: 0,
                behavior: `smooth`
              })
        });
        window.scrollTo({
            top: 0,
            behavior: `smooth`
          })
        this.getProduct(idProduct)
        this.getProducts(idCate);
        // this.getImg(idProduct)
  
    }
    public images : any[] = []
    public product :any = {productSizeList:[{price:0}]}
    public products :any = {}
    public colors : any = {}
    public sizes : any = {}
    getProduct(id : any){
        this.http.post<any>('api/product/free/get-detail',{idProduct: id})
        .subscribe(
            data => {
                this.product = data.data;
                console.log(data);
                // this.getCategory(data.data.category)
            }
        )
    }
    getColor(){
        this.http.get("api/color/free/get-all-color")
        .subscribe(
            data=>{ 
                this.colors = data
            }
        )
    }
    getSize(){
        this.http.get("api/size/free/get-all-size")
        .subscribe(
            data=>{ 
                this.sizes = data
            }
        )
    }
    get productSize() : any{
        let result ={quantity:0}
            this.product?.productSizeList.forEach((prd : any) => {
            if( this.colorChoose?.id === prd?.color?.id){
                if(this.sizeChoose.idSize === prd.size.idSize)
                result = prd
            }
        })
        return result
    }
    get getPricefromTo(){
        let min = this.product?.productSizeList[0]?.price
        let max = this.product?.productSizeList[0]?.price
        this.product?.productSizeList.forEach((prd : any) => {
            if(prd.price < min && prd.quantity > 0) min = prd.price
            if(prd.price > max && prd.quantity > 0) max = prd.price
        })
        return{min: min , max:max}
    }
    hidenColor ( color : any ) : boolean{
        let result = false
        // console.log(this.product?.productSizeList);
        let i = 0
        this.product?.productSizeList.forEach((prd : any) => {
            if(this.sizeChoose?.idSize === prd.size?.idSize){
                if( prd.color.id === color.id){
                    if(prd.quantity > 0){
                        result = false 
                        return
                    }else result = true
                }
                else if(i === this.product?.productSizeList?.length -1 ) result = true 
            }else if(this.sizeChoose?.idSize > 0 && i === this.product?.productSizeList?.length -1 ) result = true 
            i++
        })
        // console.log("result",result,color,i);
        
        let idColor = this.product.productSizeList.map((prs:any)=> prs?.color?.id )
        
        if(!idColor.includes(color?.id)) return true
        return result
    }
    // isQuantity()
    hidenSize ( size : any ) : boolean{
        let result = false
        let i = 0
        this.product?.productSizeList.forEach((prd : any) => {
            if(this.colorChoose?.id === prd.color?.id ){
                if( prd.size.idSize === size.idSize){
                    if(prd.quantity > 0){
                        result = false
                        return
                    }else result = true
                }else if(i === this.product?.productSizeList?.length -1) result = true 
            }else if(this.colorChoose?.id > 0 && i === this.product?.productSizeList?.length -1 ) result = true 
            i++
        })
        let idSize = this.product.productSizeList.map((prs:any)=> prs?.size?.idSize )
        if(!idSize.includes(size?.idSize)) return true
        return result
    }
    // getCategory(category : any){
    //     this.http.post("api/category-dad/free/get-by-id",category)
    //     .subscribe(
    //         (datas:any) => 
    //             {
    //                 // console.log("category");
    //                 // console.log(datas.data);
    //                 // if(datas.data) {
    //                 //     this.colorChoose = datas.data?.colorList[0]
    //                 //     this.sizeChoose = datas.data?.sizeList[0]
    //                 // }
    //                 this.category = datas.data}
    //     )
    // }
    getProducts(id : any){
        this.http.get<any>(`api/product/free/find-product-by-idcatedad/${id}?pageSize=4`)
        .subscribe(
            datas => {
                console.log(datas);
                this.products = datas
            }
        )
    }
    getImg(id : any ){
        this.http.get<any[]>(`api/product/free/products/${id}/image`)
        .subscribe(
            ( datas ) => {
                this.images = datas
                console.log(datas);
            },
        )
    }
    ngOnDestroy() {
    }
    getDash(input : any, max:any){
        var  quantity = Number(input.value) 
        if(quantity > max ) {input.value = max;return}
        if(quantity == 0) input.value = 1
        if(quantity <= 1) input.value = 1 
        else input.value = Number(quantity - 1)
    }
    getQuantity(input : any,max : number){
        var  quantity = input.value 
        if(quantity < 1){ input.value = 1;return}
        if(quantity >= max ){ input.value = max;return}
        if(quantity == 0) input.value = 1
        else input.value = Number(quantity*1)
    }
    getPlus(input : any,max : number){
        var  quantity = input.value 
        if(quantity < 1) {input.value = 1}
        if(quantity >= max ) {input.value = max; return}
        if(quantity == 0) input.value = 1
        else input.value = Number(quantity*1 + 1)
    }
    // getImage(name : string){
    //     return `${url.domain}/api/product/free/image/${name}`;
    // }
    
    setSize(size : any){
        if(size !== this.sizeChoose)
        this.sizeChoose = size
        else this.sizeChoose = {idSize:-5}
        let idImg= this.productSize?.idProductSize
        document.getElementById("idImg"+idImg)?.click()
    }
    setSolor(color : any){
        if(color !== this.colorChoose)
        this.colorChoose = color
        else this.colorChoose = {id:-5}
        let idImg= this.productSize?.idProductSize
        document.getElementById("idImg"+idImg)?.click()
    }
    public errorSL = false
    postCart(input : any){
        if(this.productSize.quantity < 1) {
            this.error = true 
            return
        }else this.error = false 
        if(input.value <= 0) this.errorSL = true
        else this.errorSL = false
        let product = Object.assign({quantity:0},this.productSize)
        product.quantity = input.value
        
        this.cartService.postCart(product)
        .subscribe(
            () =>{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Đã thêm vào giỏ',
                    showConfirmButton: false,
                    timer: 1500
                })
                this.count.getCountCart()
                // input.value = 1
            },
            err => {
                if(err.status != 401)
                    Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Không thể thêm vì quá số lượng',
                    showConfirmButton: false,
                    timer: 1500
                    })
                else Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Chưa đăng nhập',
                    showConfirmButton: false,
                    timer: 1500
                    })
            }
        
        )
    }
    showMsg(success:boolean,err:boolean,msg : string){
        this.alertTB.showSuccess = success
        this.alertTB.showError = err
        this.alertTB.msg = msg
        const timeout = setTimeout(()=>{
            this.alertTB.showError = false
            this.alertTB.showSuccess = false
            this.alertTB.msg = ""
        },5000)
    }

}
