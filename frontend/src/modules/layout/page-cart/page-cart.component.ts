import { HttpClient } from '@angular/common/http';
import {
    Component,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService, CountCart } from '../service';
import { url } from '@modules/domain';
// import  {url}  from 'environments/environment';

@Component({
    selector: 'page-cart',
    templateUrl: './page-cart.component.html',
    styleUrls: ['page-cart.component.scss'],
})
export class PageCartComponent implements OnInit, OnDestroy {
    
    constructor(
        public http : HttpClient,
        public route : ActivatedRoute,
        public router : Router,
        public cartService : CartService,
        public count:CountCart
    ) {}
    ngOnInit() {
        this.cartService.getCart()
        .subscribe(
            data => {
                if(data?.length > 0)
                this.createOrder(data[0].order)
                if(data) 
                this.orderDetails = data
                console.log(data);
                
            }
        )
    }
    public error : any= {status:false ,msg : ""}
    public order : any = {}
    public timeOut :any
    public images : any[] = []
    public deleteOrderDetail :any
    public orderDetails :any = []

    getCart(){
        this.cartService.getCart()
        .subscribe(
            data=>{
                console.log(data);
                if(data) 
                this.orderDetails = data
            }
        )
    }
   
    createOrder (order : any){
        this.order = order
        this.order.orderDetailsList = []
    }
    delete(){
        this.order.orderDetailsList= this.order.orderDetailsList.filter ( (odt:any) => !(odt.idOrderDetail === this.deleteOrderDetail.idOrderDetail) )
        this.cartService.deleteCart(this.deleteOrderDetail.idOrderDetail)
        .subscribe(
            data => {
                console.log("success")
                this.orderDetails = this.orderDetails.filter( (odt:any)=>{
                    if(odt.idOrderDetail === this.deleteOrderDetail.idOrderDetail) return false
                    return true
                })
                this.count.getCountCart()
            },
            err => {
            }
        )
    }
    datHang(){
        if(this.order.orderDetailsList.length < 1 ) {
            this.error.msg = "Chưa có sản phẩm nào được chọn!"
            this.error.status=true
            return
        }
        this.error.status=false
        let order = Object.assign ({},this.order)
        order.orderDetailsList.forEach( (odt:any)=> { odt.order = {} ; odt.productSize.images = {} } )
        localStorage.setItem("order",encodeURIComponent(JSON.stringify(order)))
        // decodeURIComponent
        this.router.navigate(["/pay"])
    }

    update( quantity : any , odt : any ){
        clearTimeout(this.timeOut)
        let odtUpdate = Object.assign({quantity:0},odt)
        odtUpdate.quantity = quantity
        this.timeOut = setTimeout(()=>{
            console.log("update quantity");
            
            this.cartService.update(odtUpdate)
            .subscribe(
                _=>{console.log( _); },
            )
        },2000)
    }
    ngOnDestroy() {
    }

    sort(action:string){
        if(action === 'TANG')
        this.orderDetails.sort((a:any,b:any)=>{
            if(a.productSize.price < b.productSize.price) return -1
            if(a.productSize.price > b.productSize.price) return 1
            return 0
        })
        if(action === "GIAM")
        this.orderDetails.sort((a:any,b:any)=>{
            if(a.productSize.price < b.productSize.price) return 1
            if(a.productSize.price > b.productSize.price) return -1
            return 0
        })
    }

    getDash(input : any,odt : any){
        if(  input.value < 0) input.value = 1
        if(odt.productSize.quantity < input.value) input.value = odt.productSize.quantity
        var  quantity = Number(input.value) 
        if(quantity <= 1) return 
        else input.value = Number(quantity - 1)
        odt.quantity = input.value
        this.update(input.value,odt)
    }
    alertDelete(odt : any){
        this.deleteOrderDetail=odt
    }
    getQuantity(input : any,odt : any){
        if(  input.value*1 <= 0) {input.value = 1; input.value = 1}
        if(odt.productSize.quantity < input.value) input.value = odt.productSize.quantity
        var  quantity = Number(input.value) 
        odt.quantity = quantity
        this.update(input.value,odt)
    }
    getPlus(input : any,max : number,odt : any){
        if(  input.value < 0) input.value = 1
        if(odt.productSize.quantity < input.value) input.value = odt.productSize.quantity
        var  quantity = input.value 
        if(quantity >= max ) return 
        else input.value = Number(quantity*1 + 1)
        odt.quantity = input.value
        this.update(input.value,odt)
    }
    getImage(name : string){
        return `${url.domain}/image/${name}`;
    }

    get getTotalMoney() : number{
        let total = 0;
        if(this.order?.orderDetailsList?.length > 0 )
        for(let odt of this.order?.orderDetailsList ){
            total += odt.productSize.price * odt.quantity 
        }
        return total;
    }
    setOrder(cbAll:any = document.getElementById("cball") ,cb : any , odt : any){
        console.log( this.order);
        // let cbAll = document.getElementById("cball")
        console.log(cbAll);
        
        if(cb?.checked){
            this.order.orderDetailsList.push(odt)
        }else{
            this.order.orderDetailsList = this.order.orderDetailsList.filter((odtft:any) => !(odt=== odtft))
        }
        if(this.order.orderDetailsList.length === this.orderDetails.length) cbAll.checked = true 
        else cbAll.checked = false
        console.log( this.order.orderDetailsList);
        
    }
    setOrder1(cb : any , odt : any){
        console.log( this.order);
        // let cbAll = document.getElementById("cball")
        
        if(cb?.checked){
            this.order.orderDetailsList.push(odt)
        }else{
            this.order.orderDetailsList = this.order.orderDetailsList.filter((odtft:any) => !(odt=== odtft))
        }
        console.log( this.order.orderDetailsList);
    }

    checkBoxAll(cb : any  ){
        let cbAll = document.getElementsByName('odt')
        let index=0
        cbAll.forEach((cba:any) =>{
            cba.checked = cb?.checked
            this.setOrder1(cba,this.orderDetails[index])
            index++;
        })
    }
  
}
