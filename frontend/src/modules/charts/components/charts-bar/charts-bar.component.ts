import { HttpClient } from '@angular/common/http';
import {
    AfterViewInit,
    // ChangeDetectionStrategy,/
    Component,
    ElementRef,
    Input,
    OnInit,
    ViewChild,
} from '@angular/core';
import { Chart } from 'chart.js';

@Component({
    selector: 'sb-charts-bar',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './charts-bar.component.html',
    styleUrls: ['charts-bar.component.scss'],
})
export class ChartsBarComponent implements OnInit, AfterViewInit {
    @ViewChild('myBarChart') myBarChart!: ElementRef<HTMLCanvasElement>;
    @Input() public chart!: Chart;

    constructor(
        public http: HttpClient
    ) {}
    ngOnInit() {
        this.getBieuDo()
    }
    public datas :any = []
    get label () {
        let labelrs = [""]
        labelrs=[] 
        this.datas?.forEach((data:any)=>{
            labelrs.push( "Tháng "+ data.thang)
        })
        // if(this.datas.length>0)
        // this.chart.update()
        return labelrs
    }
    get dataRs () :number[] {
        let datas = [0]
        datas=[] 
        this.datas?.forEach((data:any)=>{
            datas.push(data.tongTien)
        })
        // if(this.datas.length>0)
        // this.chart.update()
        return datas
    }
    getBieuDo(){
        this.http.get('api/thong-ke/search-theo-nam')
        .subscribe(
            (data:any)=>{  
                this.datas.length = 0
                this.datas.push(...data.data)
                // this.chart.update()
                console.log(data);
                this.chart = new Chart(this.myBarChart.nativeElement, {
                    type: 'bar',
                    data: {
                        labels: this.datas?.map( (dt:any)=> { 
                            return dt.thang
                        }),
                        datasets: [
                            {
                                label: 'Revenue',
                                backgroundColor: 'rgba(2,117,216,1)',
                                borderColor: 'rgba(2,117,216,1)',
                                data: this.datas?.map( (dt:any)=> { 
                                    return dt.tongTien
                                }),
                            },
                        ],
                    },
                    options: {
                        scales: {
                            xAxes: [
                                {
                                    time: {
                                        unit: 'month',
                                    },
                                    gridLines: {
                                        display: false,
                                    },
                                    ticks: {
                                        // maxTicksLimit: 6,
                                    },
                                },
                            ],
                            yAxes: [
                                {
                                    ticks: {
                                     
                                        maxTicksLimit: 5,
                                    },
                                    gridLines: {
                                        display: true,
                                    },
                                },
                            ],
                        },
                        legend: {
                            display: false,
                        },
                    },
                });
            }
        )
        console.log(this.datas);
        
    }
    ngAfterViewInit() {
        
    }
}
