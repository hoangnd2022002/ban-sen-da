import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '@modules/auth/services';
import { map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { url } from '@modules/domain';
// import  { url }  from 'environments/environment';

@Injectable()
export class InterceptorHTTP implements HttpInterceptor {
  constructor(public route: Router){}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var tokenSV  = new TokenService()
    // console.log(tokenSV.getToken$());
    
    const urld = url.domain;
    const apiReq = req.clone({ 
      
      url: `${urld}/${req.url}` ,
      headers:new HttpHeaders({
        // 'Content-Type':  'application/json',
        'Authorization': `Bearer ${tokenSV.getToken$()}`,
        "Access-Control-Allow-Origin":"*",
      })
      
    });
    return next.handle(apiReq).pipe(
      tap(()=> {},
      err => {
        console.log(err);
        
        if(err instanceof HttpErrorResponse){
          if(err.status === 401){
            tokenSV.deleteToken()
            this.route.navigate(['/auth/login'])
          }
        }
      }
      )
    );
  }
}
