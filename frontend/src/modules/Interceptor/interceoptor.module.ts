import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { InterceptorHTTP } from "./IntercepterHttp";


@NgModule({
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: InterceptorHTTP,
        multi: true,
      }
    ]
})

export class InterceptorHTTPModule{}