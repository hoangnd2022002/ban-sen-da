import { SideNavItems, SideNavSection } from '@modules/navigation/models';

export const sideNavSectionsQuanLy: SideNavSection[] = [
    {text: 'Bán hàng',items: ['banHang',"hoaDon","kiemDuyet"],},
    {text: 'Thống kê',items: ['dashboard'],},
    {text: 'Quản lý sản phẩm',items: ['product','category','size','lightAspect','habitat','color'],},
    { text: 'Quản lý người dùng',items: ['user','contact'],}
];
export const sideNavSectionsNhanVien: SideNavSection[] = [
    {text: 'Bán hàng',items: ['banHang',"hoaDon","kiemDuyet"],},
    // {text: 'Thống kê',items: ['dashboard'],},
    // {text: 'Quản lý sản phẩm',items: ['product','category'],},
    { text: 'Quản lý người dùng',items: ['contact'],}
];
export const sideNavItems: SideNavItems = {
    dashboard: {
        icon: 'bi bi-clipboard-data',
        text: 'Dashboard',
        count:0,
        link: '/admin',
    },
    hoaDon: {
        icon: 'bi bi-card-checklist',
        text: 'Hóa đơn',
        count:0,
        link: '/admin/hoa-don',
    },
    banHang: {
        icon: 'bi bi-shop-window',
        text: 'Bán hàng',
        count:0,
        link: '/admin/ban-hang',
    },
    product: {
        icon: 'bi bi-bag-plus',
        text: 'Sản phẩm',
        count:0,
        link: '/admin/product',
    },
    contact: {
        icon: 'bi bi-telephone-inbound',
        text: 'Liên hệ',
        count:0,
        link: '/admin/contact',
    },
    category:{
        icon:"bi bi-tag",
        text:"Thể loại",
        count:0,
        link: '/admin/category',
    },
    size:{
        icon:"bi bi-tag",
        text:"Kích thước",
        count:0,
        link: '/admin/size',
    },
    color:{
        icon:"bi bi-tag",
        text:"Màu sắc",
        count:0,
        link: '/admin/color',
    },
    habitat:{
        icon:"bi bi-tag",
        text:"Môi trường sống",
        count:0,
        link: '/admin/habitat',
    },
    lightAspect:{
        icon:"bi bi-tag",
        text:"Ánh sáng",
        count:0,
        link: '/admin/light-aspect',
    },
    user: {
        icon: 'bi bi-people',
        text: 'Người dùng',
        count:0,
        link: '/admin/user',
    },
    kiemDuyet: {
        icon: 'bi bi-box2',
        text: 'Kiểm duyệt',
        count:0,
        link: '/admin/kiem-duyet',
    },
   
    // pages: {
    //     icon: 'book-open',
    //     text: 'Pages',
    //     submenu: [
    //         {
    //             text: 'Authentication',
    //             submenu: [
    //                 {
    //                     text: 'Login',
    //                     link: '/auth/login',
    //                 },
    //                 {
    //                     text: 'Register',
    //                     link: '/auth/register',
    //                 },
    //                 {
    //                     text: 'Forgot Password',
    //                     link: '/auth/forgot-password',
    //                 },
    //             ],
    //         },
    //         {
    //             text: 'Error',
    //             submenu: [
    //                 {
    //                     text: '401 Page',
    //                     link: '/error/401',
    //                 },
    //                 {
    //                     text: '404 Page',
    //                     link: '/error/404',
    //                 },
    //                 {
    //                     text: '500 Page',
    //                     link: '/error/500',
    //                 },
    //             ],
    //         },
    //     ],
    // },
    // charts: {
    //     icon: 'chart-area',
    //     text: 'Charts',
    //     link: '/charts',
    // },
    // tables: {
    //     icon: 'table',
    //     text: 'Tables',
    //     link: '/tables',
    // },
};
