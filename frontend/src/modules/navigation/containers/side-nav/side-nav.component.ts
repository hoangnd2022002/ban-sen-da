import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '@modules/auth/services';
import { Count } from '@modules/layout/service';
import { SideNavItems, SideNavSection } from '@modules/navigation/models';
import { NavigationService } from '@modules/navigation/services';
import { Subscription } from 'rxjs';

@Component({
    selector: 'sb-side-nav',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './side-nav.component.html',
    styleUrls: ['side-nav.component.scss'],
})
export class SideNavComponent implements OnInit, OnDestroy {
    @Input() sidenavStyle!: string;
    @Input() sideNavItems!: SideNavItems;
    @Input() sideNavSections!: SideNavSection[];

    subscription: Subscription = new Subscription();
    routeDataSubscription!: Subscription;

    constructor(public navigationService: NavigationService, public userService: UserService,public http:HttpClient, public count: Count) {}
    public chuaNhan = 0
    ngOnInit() {
        this.count.gresetCountKiemDuyetn(this.sideNavItems.kiemDuyet)
        this.count.getCountLienHe(this.sideNavItems.contact)
        this.count.getCountHetSL(this.sideNavItems.product)
        // this.sideNavItems.kiemDuyet.count = this.count.getCountKiemDuyet
        console.log(this.sideNavItems);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
