import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-forgot-password',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './forgot-password.component.html',
    styleUrls: ['forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
    constructor(public formBuild : FormBuilder,
        public http : HttpClient,
        public router : Router
        ) {}
    ngOnInit() {}
    public formGroup = this.formBuild.group({
        email:["",Validators.required]
    })
    public maXacNhan = ""
    public formGroupResetPass = this.formBuild.group({
        passNew:["",[Validators.required,Validators.minLength(5)]],
        verification:["",[Validators.required]]
    })
    public loading = false
    public nhapMaXacNhan = false
    public submited= false
    onSubmit(){
        this.nhapMaXacNhan = true
        this.submit = true
        if(this.validate){
            console.log("submit");
            
            this.loading = true
            this.http.post("api-email/send-mail",this.formGroup.value)
            .subscribe(
                ()=>{
                    console.log("Success");
                    this.resetPass = true
                },
                ()=>{},
                ()=>{this.loading = true}
            )
        }
    }
    get validate() : boolean{
        return !this.formGroup.controls["email"].errors
    }
    public resetPass = false
    public submit = false
    public xacNhanMa(){
        if(this.maXacNhan){
            console.log("submit");
            
            this.loading = true
            this.http.post("api-email/send-mail",this.formGroup.value)
            .subscribe(
                ()=>{
                    console.log("Success");
                    this.resetPass = true
                    this.submit = false
                },
                ()=>{},
                ()=>{this.loading = true}
            )
        }
    }
    public resetpass(){
        this.submited = true
        if(this.validateResetPass){
            console.log("submit");
            let formGroup = Object.assign({},this.formGroupResetPass.value )
            formGroup.email= this.formGroup.controls?.email?.value
            this.loading = true
            console.log(formGroup);
            
            this.http.post("api-email/pass-change",formGroup)
            .subscribe(
                (data:any)=>{
                    console.log(data);
                    if(data?.data){
                        this.resetPass = true
                        this.router.navigate(["/auth/login"])
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Đã đổi mật khẩu thành công',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                    else {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: data.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                },
                ()=>{},
                ()=>{this.loading = true}
            )
        }
    }
    get formControl(){
        return this.formGroupResetPass.controls
    }
    get validateResetPass(){
        let err = this.formGroupResetPass.controls
        return !(
            err.passNew.errors 
            || err.verification.errors
        )
    }
}
