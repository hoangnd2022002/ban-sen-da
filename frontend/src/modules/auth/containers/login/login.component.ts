import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TokenService } from '@modules/auth/services';
import { RoleService } from '@modules/layout/service';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-login',
    templateUrl: './login.component.html',
    styleUrls: ['login.component.scss'],
})
export class LoginComponent implements OnInit {
    constructor(
        public formBuilder: FormBuilder,
        public http : HttpClient,
        public tokenSV : TokenService,
        public router : Router,
        public roleSv : RoleService
    ) {}
    public formGroup = this.formBuilder.group({
        username:["",[Validators.required,Validators.pattern("[a-zA-Z0-9]*"),Validators.minLength(5),Validators.maxLength(15)]],
        password : ["",[Validators.required,Validators.minLength(5),Validators.maxLength(15)]],
    })
    public submited = false 
    ngOnInit() {}
    get errorsForm(){
        return this.formGroup.controls
    }
    public saiMk=false
    login(){
        console.log(this.formGroup.value);
        
        this.submited = true
        if(this.validated()){
            this.http.post<any>("auth/sign-in",this.formGroup.value)
            .subscribe(
                data => {
                    console.log(data);
                    
                    this.tokenSV.setToken(data.accessToken)
                    this.roleSv.saveRole = data.role
                    this.router.navigate(["/home"])
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đăng nhập thành công',
                        showConfirmButton: false,
                        timer: 1500
                      })
                    this.saiMk= false
                },
                err => {
                    this.saiMk= true
                }
                
            )
        }
    }


    validated() : boolean{
        return !(
            this.errorsForm?.username?.errors || 
            this.errorsForm?.password?.errors 
            )
    }

}



