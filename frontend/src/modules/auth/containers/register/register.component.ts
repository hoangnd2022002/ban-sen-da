import { HttpClient } from '@angular/common/http';
import {  Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
    selector: 'sb-register',
    templateUrl: './register.component.html',
    styleUrls: ['register.component.scss'],
})
export class RegisterComponent implements OnInit {
    constructor(
        public http:HttpClient,
        public formBuilder: FormBuilder,
        public route : Router
        ) {}
    ngOnInit() {}
    public errConfirm = false
    public submited = false
    public errorAPI : any
    public formGroup = this.formBuilder.group({
        name:["",[Validators.required,Validators.pattern("[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ]*")]],
        username:["",[Validators.required,Validators.pattern("[a-zA-Z0-9]*"),Validators.minLength(5),Validators.maxLength(15)]],
        phone:["",[Validators.required,Validators.pattern("[0]{1}[1-9]{9}")]],
        email:["",[Validators.required]],
        sex:["0",Validators.required],
        password:["",[Validators.required,Validators.minLength(5),Validators.maxLength(15)]],
        role:[{idRole : 3}],
    })
    register(input : any){
        this.submited = true
        console.log(this.validate(input));
        console.log(this.formGroup.value);
        if(this.validate(input)){
            this.http.post(`api/user/free/register-user`,this.formGroup.value)
            .subscribe(
                (data:any) => {
                    console.log(data);
                    this.errorAPI = data.message
                    // abc1@gmail.com
                    if(data.message === "SUCCESS_ADD_USER")
                    {
                        this.route.navigate(["/auth/login"])  
                        Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Đăng kí thành công',
                        showConfirmButton: false,
                        timer: 1500
                        })         
                    }    
                }
            )
        }
        // this.http.post(`api/user/`)
    }
    validate( comfirm_password : any ) : boolean{
        let confirm = comfirm_password.value === this.errorsForm["password"].value
        if(confirm){ 
            comfirm_password.classList.remove("class" , "outline-danger")
            this.errConfirm = false
        }else{
            comfirm_password.classList.add("class" , "outline-danger")
            this.errConfirm = true
        }
        return !(
            this.errorsForm["name"].errors ||
            this.errorsForm["username"].errors ||
            this.errorsForm["phone"].errors ||
            // this.errorsForm["sex"].errors ||
            this.errorsForm["password"].errors ||
            // this.errorsForm["role"].errors ||
            this.errorsForm["email"].errors ||
            !confirm
        )
    }
    get errorsForm(){
        return this.formGroup.controls
    }
}
