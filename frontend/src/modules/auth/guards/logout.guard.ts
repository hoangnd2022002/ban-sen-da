import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService, TokenService } from '../services';

@Injectable()
export class LogoutGuard implements CanActivate {
    constructor(
        public authService : AuthService,
        public route: Router,
        public tokenService: TokenService

    ){};
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):Observable <boolean> {
        if(this.authService.isLogged()){
            this.tokenService.deleteToken()
            this.route.navigate(['/home'])
            return of(false)
        }
        this.route.navigate(['/auth/login'])
        return of(true);
    }
}
