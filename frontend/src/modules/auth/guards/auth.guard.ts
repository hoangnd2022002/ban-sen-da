import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService, TokenService } from '../services';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        public authService : AuthService,
        public route: Router,
        public http: HttpClient,
        public tokenSevice : TokenService
    ){};
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):Observable <boolean> {
        console.log(route);
        
        if(!this.authService.isLogged()){
            console.log(this.authService.isLogged());
            this.route.navigate(['/auth/login'])
            return of(false)
        }
        this.http.get("api/product/free/get-expiration-token")
        .subscribe(
            data => {
                console.log(data);
                if(!data) {this.tokenSevice.deleteToken()
                this.route.navigate(['/auth/login'])}
                return of(data)
            }
        )
        return of(true);
    }
}
