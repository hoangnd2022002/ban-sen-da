import { AuthGuard } from './auth.guard';
import { LogoutGuard } from './logout.guard';

export const guards = [AuthGuard,LogoutGuard];

export * from './auth.guard';
export * from './logout.guard';
