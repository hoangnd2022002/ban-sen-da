/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AuthModule } from './auth.module';

/* Containers */
import * as authContainers from './containers';

/* Guards */
import * as authGuards from './guards';
import { AuthGuard } from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login',
    },
    {
        path: 'login',
        canActivate: [],
        component: authContainers.LoginComponent,
        data: {
            title: 'Sen đá Liên Hoa - Đăng nhập',
        } as SBRouteData,
    },
    {
        path: 'logout',
        canActivate: [authGuards.LogoutGuard],
        component: authContainers.LoginComponent,
        data: {
            title: 'Sen đá Liên Hoa - Đăng nhập',
        } as SBRouteData,
    },
    {
        path: 'register',
        canActivate: [],
        component: authContainers.RegisterComponent,
        data: {
            title: 'Sen đá Liên Hoa - Đăng nhập',
        } as SBRouteData,
    },
    {
        path: 'forgot-password',
        // canActivate:[authGuards.AuthGuard],

        component: authContainers.ForgotPasswordComponent,
        data: {
            title: 'Sen đá Liên Hoa - Quên mật khẩu',
        } as SBRouteData,
    },
];

@NgModule({
    imports: [AuthModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
