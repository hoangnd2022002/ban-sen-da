import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

// import { User } from '../models';


@Injectable()
export class TokenService {
    constructor() {
    }

    setToken(token : any) {
        localStorage.setItem("token",token);
    }

    getToken$(): string | null {
        return localStorage.getItem("token");
    }

    checkToken():boolean{
        return typeof(this.getToken$()) === "string";
    }

    deleteToken(){
        localStorage.removeItem("token")
    }
}
