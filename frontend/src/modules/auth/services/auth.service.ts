import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { TokenService } from './token.service';

@Injectable({providedIn: "root"})
export class AuthService {
    constructor(
        public tokenSevice: TokenService  
    ) {}

    getAuth$(): Observable<{}> {
        return of({});
    }
    isLogged():boolean{
        return this.tokenSevice.checkToken();
    }

}
