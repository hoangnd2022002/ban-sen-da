package com.example.duantn.repository;

import com.example.duantn.entity.ProductSize;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository

public interface ProductSizeRepository extends JpaRepository<ProductSize,Integer> {

    @Query(value = "SELECT * FROM product_size where product_size.id_product=:product and product_size.id_size=:size and product_size.id_color=:color and product_size.status=1",nativeQuery = true)
    Optional<ProductSize> checkTonTaiTrongProSize(int product, int size,int color);

    @Query(value = "SELECT ps FROM ProductSize ps where ps.idProductSize=:idProductSize")
    Optional<ProductSize> checkProductSize(int idProductSize);


    @Query(value = "SELECT ps  from ProductSize ps WHERE ps.product.idProduct=:idProduct and ps.status=1")
    List<ProductSize> searchProductAndSizeStatus1(int idProduct);



    @Query(value = "SELECT ps  from ProductSize ps join OrderDetail od on od.productSize.idProductSize= ps.idProductSize join Orders o " +
            " on o.idOrder= od.order.idOrder WHERE o.idOrder=:idOrder ")
    List<ProductSize> searchProductAndSizeByOrderAllStatus(int idOrder);
    @Query(value = "SELECT ps FROM ProductSize ps where ps.size.idSize=:idSize and ps.status=1")
    List<ProductSize> searchProductSizeTheoSize(int idSize);

    @Query(value = "SELECT ps FROM ProductSize ps where ps.color.id=:idColor and ps.status=1")
    List<ProductSize> searchProductSizeTheoColor(int idColor);

//    @Query(value = "SELECT ps FROM ProductSize ps where ps.type.idType=:idType and ps.status=1")
//    List<ProductSize> searchProductSizeTheoType(int idType);

    @Query(value = "SELECT ps FROM ProductSize ps join Product p on ps.product.idProduct=p.idProduct join LightAspect la on p.lightAspect.idLightAspect=la.idLightAspect where la.idLightAspect=:idLight and ps.status=1")
    List<ProductSize> findProductSizesByLightAspect(int idLight);
    @Query(value = "SELECT ps FROM ProductSize ps join Product p on ps.product.idProduct=p.idProduct join LightAspect la on p.lightAspect.idLightAspect=la.idLightAspect where la.idLightAspect=:idLight and ps.status=1")
    List<ProductSize> findProductSizesByLightAspectPage(Pageable pageable,int idLight);

    @Query(value = "SELECT ps FROM ProductSize ps join Product p on ps.product.idProduct=p.idProduct join Type la on p.type.idType=la.idType where la.idType=:idType and ps.status=1 and p.productName like concat('%',:searchName,'%') and p.status=1 ")
    List<ProductSize> findProductSizesByTypePage(Pageable page,int idType,String searchName);

    @Query(value = "SELECT ps FROM ProductSize ps join Product p on ps.product.idProduct=p.idProduct join Type la on p.type.idType=la.idType where la.idType=:idType and ps.status=1")
    List<ProductSize> findProductSizesByType(int idType);

    @Query(value = "SELECT ps FROM ProductSize ps join Product p on ps.product.idProduct=p.idProduct join HabitatsProduct hp on p.idProduct=hp.product.idProduct join Habitats h on h.idHabitats=hp.habitats.idHabitats where h.idHabitats=:habitats and ps.status=1")
    List<ProductSize> findProductSizesByHabitat(int habitats);

    @Query(value = "SELECT ps FROM ProductSize ps join Product p on ps.product.idProduct=p.idProduct join HabitatsProduct hp on p.idProduct=hp.product.idProduct join Habitats h on h.idHabitats=hp.habitats.idHabitats where h.idHabitats=:habitats and ps.status=1")
    List<ProductSize> findProductSizesByHabitatPage(Pageable page,int habitats);


    @Query(value = "SELECT id_product_size,price,quantity,status,id_product,id_size,id_color,image,qrcode " +
            " FROM product_size where product_size.id_product=:idProduct and status=1",nativeQuery = true)
    List<ProductSize> searchProductAndSizeStatus1TEST(int idProduct);

    @Query(value = "SELECT id_product_size,price,quantity,status,id_product,id_size,id_color,CONCAT('http://localhost:8080/image/',image)as image,qrcode " +
            " FROM product_size where product_size.id_product=:idProduct and status=1",nativeQuery = true)
    List<ProductSize> searchProductAndSizeStatus1AnhLocal(int idProduct);
    @Query(value = "SELECT id_product_size,price,quantity,status,id_product,id_size,id_color,CONCAT('http://localhost:8080/image/',image)as image,qrcode " +
            " FROM product_size where product_size.id_product=:idProduct and status=2",nativeQuery = true)
    List<ProductSize> searchProductAndSizeStatus2AnhLocal(int idProduct);
    @Query(value = "SELECT * FROM product_size where id_product=:product and status=2",nativeQuery = true)
    List<ProductSize> searchProductAndSizeStatus2(int product);

    @Query(value = "SELECT ps FROM ProductSize ps JOIN Product p on p.idProduct=ps.product.idProduct " +
            "              where ps.status=1 and " +
            "              p.productName LIKE CONCAT('%' ,:keyword, '%')")
    List<ProductSize> getAllPSStatus1( Pageable page,String keyword);


    @Query(value = "select ps from ProductSize ps where ps.qrCode=:qrCode and ps.status=1")
    ProductSize getProductSizeByQrCode(String qrCode);

    @Query(value = "SELECT * FROM product_size where id_product=:product ",nativeQuery = true)
    List<ProductSize> searchProductAndSizeStatus(int product);
    @Transactional
    @Modifying
    @Query(value = "update ProductSize ps set ps.quantity=:quantity where ps.idProductSize = :idProductSize")
    void updateQuantity(int quantity,int idProductSize);

    @Transactional
    @Modifying
    @Query(value = "update ProductSize ps set ps.quantity=ps.quantity + :quantity where ps.idProductSize = :idProductSize")
    void updateQuantityTraHang(int quantity,int idProductSize);

    @Query(value = "select  p.quantity from ProductSize p where p.idProductSize=:id")
    Integer getQuantityByID(int id);


    @Query(value = "select  p.price from ProductSize p where p.idProductSize=:id")
    Float getPriceByID(int id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE ProductSize p set p.status=2 where p.product.idProduct=:product ")
    void changeStatusByProduct(int product);

    @Query(value = "select count (ps.idProductSize) from ProductSize ps where ps.status=1 and ps.product.idProduct=:id")
    Integer demSoLuongProductSize(int id);



}
