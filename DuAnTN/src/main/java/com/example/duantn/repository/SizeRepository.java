package com.example.duantn.repository;

import com.example.duantn.entity.Size;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface SizeRepository extends JpaRepository<Size,Integer> {


//    List<Size> getSizeFromType(int id);

    @Query(value = "select s from Size s where  s.status = 1")
    List<Size>  getAllSizeByStatus1();

    @Query(value = "select s.sizeName from Size s where s.sizeName = :size")
    String checkTrung(String size);



//    @Query(value = "SELECT * FROM datn_test.product_size where id_product=:product and id_size=:size",nativeQuery = true)
//    Optional<ProductSize> checkTonTaiTrongProSize(int product, int size);
//
//    @Query(value = "SELECT * FROM datn_test.product_size where id_product=:product and status=1",nativeQuery = true)
//    List<ProductSize> searchProductAndSizeStatus1(int product);
//    @Query(value = "SELECT * FROM datn_test.product_size where id_product=:product and status=2",nativeQuery = true)
//    List<ProductSize> searchProductAndSizeStatus2(int product);
//    @Query(value = "SELECT * FROM datn_test.product_size where id_product=:product ",nativeQuery = true)
//    List<ProductSize> searchProductAndSizeStatus(int product);
//
//
//
//    @Modifying
//    @Transactional
//    @Query(value = "UPDATE ProductSize p set p.status=2 where p.product.idProduct=:product ")
//    void changeStatusByProduct(int product);



}
