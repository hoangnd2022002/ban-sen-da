package com.example.duantn.repository;

import com.example.duantn.entity.OrderDetail;
import com.example.duantn.entity.Orders;
import com.example.duantn.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Integer> {
    @Modifying
    @Transactional
    @Query("update Orders o set o.status= :status where o.idOrder = :id")
    void updateOrderStatus(String status, int id);

    @Query("select od from Orders od where od.status NOT LIKE 'BÁN TẠI QUẦY' and od.status NOT LIKE 'HỦY TẠI QUẦY' and  od.createDate between :ngayBatDau and :ngayKetThuc order by od.idOrder desc ")
    List<Orders> getOrderOnline(Timestamp ngayBatDau,Timestamp ngayKetThuc);
    @Query("select od from Orders od where od.status LIKE 'HỦY TẠI QUẦY' and  od.createDate between :ngayBatDau and :ngayKetThuc order by od.idOrder desc ")
    List<Orders> getOrderXacNhanHuy(Timestamp ngayBatDau,Timestamp ngayKetThuc);
    @Query("select od from Orders od where od.status =:keyword and  od.createDate between :ngayBatDau and :ngayKetThuc ")
    Page<List<Orders>> getOrderByDay(Pageable page,String keyword,Timestamp ngayBatDau,Timestamp ngayKetThuc);
    @Query("select od from Orders od where od.status LIKE 'BÁN TẠI QUẦY'and  od.createDate between :ngayBatDau and :ngayKetThuc order by od.idOrder desc ")
    List<Orders> getOrderOffline(Timestamp ngayBatDau,Timestamp ngayKetThuc);

    @Query("select od from Orders od where od.status LIKE 'ĐÃ GIAO'")
    List<Orders> getOrderDAGIAO();

    @Query("select od from OrderDetail od where od.order.idOrder=:idOrder")
    List<OrderDetail> listOdByO(int idOrder);

    @Query("select od.user from Orders od where od.idOrder=:idOrder")
    User getUserFromOrder(int idOrder);
    @Query("select od from Orders od where od.idOrder=:idOrder and od.user.idUser=:idUser and od.status='GIỎ HÀNG'")
    Orders getOrderGioHangFromUser(int idOrder,int idUser);
    Orders getOrdersByIdOrder(Integer idorder);
    Page<Orders> getOrderByStatus(String status, Pageable page);
    Orders getByStatusAndUser(String status, User uesr);

    @Query("select count(od) from Orders od where od.createDate between :startDate AND :endDate and od.status ='ĐÃ NHẬN'")
    Integer getOrdersByDANHAN(Timestamp startDate, Timestamp endDate);
    @Query("select count(od) from Orders od where od.createDate between :startDate AND :endDate and od.status ='ĐANG GIAO'")
    Integer getOrdersByDANGGIAO(Timestamp startDate, Timestamp endDate);
    @Query("select count(od) from Orders od where od.createDate between :startDate AND :endDate and od.status ='ĐÃ GIAO'")
    Integer getOrdersByDAGIAO(Timestamp startDate, Timestamp endDate);
    @Query("select count(od) from Orders od where od.createDate between :startDate AND :endDate and od.status ='ĐÃ HỦY'")
    Integer getOrdersByDAHUY(Timestamp startDate, Timestamp endDate);
    @Query("select count(od) from Orders od where od.createDate between :startDate AND :endDate and od.status ='BÁN TẠI QUẦY'")
    Integer getOrdersByBANTAIQUAY(Timestamp startDate, Timestamp endDate);
    @Query("select SUM (od.totalMoney) from Orders od where od.createDate between :startDate AND :endDate and (od.statusMoney='TRẢ TRƯỚC' OR od.status='BÁN TẠI QUẦY' OR od.status='CHỜ HỦY TẠI QUẦY' OR od.status='ĐÃ GIAO' OR od.status='ĐÃ NHẬN')")
    Float doanhThu(Timestamp startDate, Timestamp endDate);
    @Query("select od from Orders od where od.createDate between :startDate AND :endDate and (od.statusMoney='TRẢ TRƯỚC' or od.status='BÁN TẠI QUẦY'OR od.status='CHỜ HỦY TẠI QUẦY' OR od.status='ĐÃ GIAO' OR od.status='ĐÃ NHẬN')")
    List<Orders> getOrdersDaBan(Timestamp startDate, Timestamp endDate);
    @Query("select od from Orders od where od.user.idUser=:id and od.status=:status")
    List<Orders> getOrdersByUser(int id,String status);

    @Query("select o.user from Orders o where o.idOrder=:idOrder")
    User getUserByOrder(int idOrder);

    @Query(value = "SELECT  YEAR(o.createDate) ,  MONTH(o.createDate) , o.totalMoney  FROM Orders o  WHERE  o.createDate >= :startDate  GROUP BY  YEAR(o.createDate), MONTH(o.createDate) ORDER BY YEAR(o.createDate) DESC, MONTH(o.createDate) DESC")
    List<Orders> thongKeThang();

    @Query(value = "SELECT SUM (o.totalMoney) FROM Orders o where YEAR (o.createDate) = :nam and MONTH (o.createDate)=:thang and (o.statusMoney='TRẢ TRƯỚC' OR o.status='CHỜ HỦY TẠI QUẦY' or o.status='BÁN TẠI QUẦY' OR o.status='ĐÃ GIAO' OR o.status='ĐÃ NHẬN')")
    Float thongKeTheoNam(int nam,int thang);

    @Query(value = "select count (o.idOrder) from Orders o where o.status=:status")
    Integer soLuongTheoTrangThai(String status);

}