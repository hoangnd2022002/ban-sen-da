package com.example.duantn.repository;

import com.example.duantn.entity.Role;
import com.example.duantn.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}