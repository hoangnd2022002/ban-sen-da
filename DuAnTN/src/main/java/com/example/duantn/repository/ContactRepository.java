package com.example.duantn.repository;

import com.example.duantn.entity.Contact;
import com.example.duantn.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact,Integer> {

    @Query(value = "SELECT c.user from Contact c where c.idContact=:id")
    User getUserByContact(int id);

    @Query(value = "SELECT c from Contact c where c.status=:status")
    List<Contact> getContactTheoStatus(Pageable page,String status);
    @Query(value = "SELECT count (c.idContact) from Contact c where c.status=:status")
    Integer soLuongContactTheoStatus(String status);
    
}
