package com.example.duantn.repository;


import com.example.duantn.entity.Habitats;
import com.example.duantn.entity.OrderDetail;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.Habitats;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HabitatRepository extends JpaRepository<Habitats, Integer> {

    @Query(value = "SELECT p from Habitats p  where p.status = 1 ORDER BY p.idHabitats desc ")
    List<Habitats> getHabitatsStatus1();

    @Query(value = "SELECT p from Habitats p join HabitatsProduct hp on p.idHabitats=hp.habitats.idHabitats where hp.product.idProduct=:idProduct and p.status = 1 ORDER BY p.idHabitats desc ")
    List<Habitats> getHabitatsByProduct(int idProduct);
    @Query(value = "select h.idHabitats from Habitats h where h.status = 1")
    List<Integer> getIdHabitats();
    @Query(value = "SELECT p from Habitats p  where p.status = 2")
    List<Habitats> getHabitatsStatus2();

    @Query(value = "SELECT p.habitatsName from Habitats p where p.habitatsName = :name and p.status = 1" )
    String findNameHabitatsByName(String name);

    @Query(value = "SELECT p from Product p JOIN HabitatsProduct hp on hp.product.idProduct=p.idProduct join Habitats h on h.idHabitats=hp.habitats.idHabitats where h.idHabitats = :id and p.status = 1" )
    List<Product> getProductByHabitats(Pageable page, int id);

    @Query(value = "SELECT u FROM Habitats u where u.habitatsName LIKE %:keyword% and u.status=1 ")
    Habitats searchHabitats(String keyword);

    @Query(value = "select s.habitatsName from Habitats s where s.habitatsName=:habitat and s.status=1")
    String checkTrung(String habitat);
    
}
