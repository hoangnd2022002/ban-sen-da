package com.example.duantn.repository;

import com.example.duantn.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "SELECT u from User u where u.role.idRole = :id and u.status=1")
    List<User> getUserByIDRole(int id);

    @Query(value = "SELECT u.username from User u where u.username = :username and u.status=1" )
    String checkUsername(String username);

    @Query(value = "SELECT u.email from User u where u.email =:email and u.status=1")
    String checkEmail(String email);
    @Modifying
    @Transactional
    @Query(value = "update User u set u.expirationDate=:expirationDate,u.UUID=:UUID where u.email =:email and u.status=1")
    void setUUID(String email, Timestamp expirationDate, String UUID);

    @Query(value = "SELECT u from User u where u.email =:email and u.status=1")
    User findUserByEmail(String email);



    @Query(value = "SELECT u.phone from User u where u.phone =:phone and u.status=1" )
    String checkPhone(String phone);

    @Query(value = "SELECT p from User p  where p.status = 1 ORDER BY p.idUser DESC ")
    List<User> getUsersStatus1();

    @Query(value = "SELECT p from User p  where p.status = 2")
    List<User> getUsersStatus2();

    @Modifying
    @Transactional
    @Query(value = "update User u set u.username =:username , u.email = :email , u.sex = :sex , u.phone = :phone , u.name = :name ,u.role.idRole=:role WHERE  u.idUser = :idUser " )
    void saveUserWithoutPass(@Param("username") String username ,@Param("email") String email ,@Param("sex") byte sex ,@Param("phone") String phone
                            ,@Param("name") String name,@Param("role") Integer role ,@Param("idUser") Integer idUser );

    @Query(value = "SELECT u FROM User u where u.name LIKE %:keyword% and u.status=1 and u.role.idRole = :idRole order by u.idUser desc ")
    List<User> searchUser(String keyword,int idRole);

    @Query(value = "SELECT u FROM User u where u.username = :username and u.status=1")
    User searchUserByUsername(String username);


}