package com.example.duantn.repository;

import com.example.duantn.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {



    @Query(value = "select p from Product p where p.type.idType=:id and p.status = 1")
    List<Product> getProductsByIDTypeS1(int id);

    @Query(value = "select p from Product p where p.lightAspect.idLightAspect=:id and p.status = 1")
    List<Product> getProductsByIDLightAspectS1(int id);



    @Query(value = "SELECT p.productName from Product p where p.productName = :name and p.status = 1"  )
    String findNameProductByName(String name);
    @Query(value = "SELECT p from Product p where p.productName = :name and p.status = 1"  )
    Product findProductByName(String name);


    @Query(value = "SELECT p from Product p  where p.status = 1 order by p.idProduct desc ")
    List<Product> getProductsStatus1();

    @Query(value = "SELECT DISTINCT p from Product p  join ProductSize ps on p.idProduct=ps.product.idProduct where ps.quantity=0 and ps.status=1 and p.productName like concat('%',:name,'%') and p.type.idType =:idType")
    List<Product> getProductHetSoLuong(String name , Integer idType);

    @Query(value = "SELECT DISTINCT p from Product p  join ProductSize ps on p.idProduct=ps.product.idProduct where ps.quantity=0 and ps.status=1 and p.status=1 and p.productName like concat('%',:name,'%') ")
    List<Product> getProductHetSoLuongVer2(String name);

    @Query(value = "SELECT p from Product p  where p.status = 2")
    List<Product> getProductsStatus2();



    @Query(value = "SELECT u FROM Product u where u.productName LIKE %:keyword% and u.status=1  order by u.idProduct desc ")
    List<Product> searchProduct(String keyword);

    @Query(value = "SELECT u FROM Product u where u.productName LIKE %:keyword% and u.type.idType=:idType and u.status=1  order by u.idProduct desc ")
    List<Product> searchProductAndType(String keyword,int idType);

    @Query(value = "SELECT u FROM Product u where u.productName LIKE %:keyword%  and u.status=1  order by u.idProduct desc ")
    List<Product> searchProductNoType(String keyword);

    @Query(value = "SELECT u FROM Product u where u.productName LIKE %:keyword% and u.type.idType=:idType and u.status=2  order by u.idProduct desc ")
    List<Product> searchProductAndTypeNgungBan(String keyword,int idType);

    @Query(value = "SELECT u FROM Product u where u.productName LIKE %:keyword%  and u.status=2  order by u.idProduct desc ")
    List<Product> searchProductAndTypeNgungBanNoType(String keyword);

    @Query(value = "SELECT u FROM Product u where u.productName LIKE %:keyword% and u.status=1  ")
    List<Product> searchProductNotCate(String keyword);

    @Query(value = "SELECT count(distinct u.product.idProduct)FROM ProductSize u where u.quantity=0 and u.status=1  ")
    Integer listProduct();


    @Query(value = "SELECT u from Product u where u.type.idType = :id and u.productName LIKE %:keyword%")
    List<Product> searchProductInType(String keyword, int id);

    @Query(value = "select distinct pd.* from product pd\n" +
            "                join habitats_product hp on hp.id_product = pd.id_product\n" +
            "                join product_size pds on pds.id_product = pd.id_product \n" +
            "                where  pd.id_light_aspect like :idAlight\n" +
            "                and pd.id_type like :idType\n" +
            "                and (\n" +
            "\t\t\t\t\tselect count(id_habitats) from habitats_product where id_product = pd.id_product\n" +
            "\t\t\t\t\tand id_habitats in :idHabitat\n" +
            "\t\t\t\t) >= :countHabitat\n" +
            "                and pd.status = 1\n" +
            "\t\t\t\tand pds.price between :keyword1 and :keyword2 and pds.status = 1 and pds.price between :keyword1 and :keyword2 ",nativeQuery = true)
    List<Product> search(Float keyword1, Float keyword2, String idType,int countHabitat,List idHabitat, String idAlight);
//    @Query(value = "select distinct pd.* from product pd \n" +
//            "    join habitats_product hp on hp.id_product = pd.id_product\n" +
//            "    join product_size pds on pds.id_product = pd.id_product \n" +
//            "    where  pd.id_light_aspect like :idAlight \n" +
//            "    and pd.id_type like :idType" ,nativeQuery = true)
//    List<Product> search( String idType, String idAlight);
//    @Query(value = "select distinct pd.* from product pd \n" +
//            "    join habitats_product hp on hp.id_product = pd.id_product\n" +
//            "    join product_size pds on pds.id_product = pd.id_product \n" +
//            "    where  pd.id_light_aspect like :idAlight \n" +
//            "    and pd.id_type like :idType" +
//            "    and hp.id_habitats in :idHabitat\n",nativeQuery = true)
//    List<Product> search( String idType,List idHabitat, String idAlight);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and p.type.idType = :idType and ps.price between :keyword1 and :keyword2 ")
    List<Product> searchProductByPriceCoType(Float keyword1, Float keyword2, Integer idType);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and p.type.idType = :idType ")
    List<Product> searchProductByPriceTH2(Integer idType);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and p.type.idType = :idType and ps.price >= :keyword1 ")
    List<Product> searchProductByPriceTH3(Integer idType,Float keyword1);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and p.type.idType = :idType and ps.price <= :keyword2 ")
    List<Product> searchProductByPriceTH4(Integer idType, Float keyword2);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and ps.price <= :keyword2 ")
    List<Product> searchProductByPriceTH5( float keyword2);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and ps.price >= :keyword1 ")
    List<Product> searchProductByPriceTH6(float keyword1);
    @Query(value = "select distinct p from Product p JOIN ProductSize ps ON p.idProduct=ps.product.idProduct where p.status=1 and ps.price between :keyword1 and :keyword2 ")
    List<Product> searchProductByPriceTH7(float keyword1, float keyword2);
    @Modifying
    @Transactional
    @Query(value = "update Product p set p.moTa=:mota,p.productName=:productName, p.status=1,p.type.idType=:idType,p.lightAspect.idLightAspect=:idLightAspect where p.idProduct=:idProduct")
    void updateMotaTenAnhSangType(String mota , String productName, int idType, int idLightAspect, int idProduct);

    @Modifying
    @Transactional
    @Query(value = "update Product p set p.status=2 where p.idProduct=:id")
    void  updateStatus(int id);

    //
//    @Query(value = "SELECT u FROM Product u where u.status=1 order by u.giaBan DESC  ")
//    List<Product> searchProductByPriceGiamDan();
//    @Query(value = "SELECT u FROM Product u where u.status=1 order by u.giaBan ASC ")
//    List<Product> searchProductByPriceTangDan();

}