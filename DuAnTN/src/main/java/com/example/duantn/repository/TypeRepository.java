package com.example.duantn.repository;

import com.example.duantn.entity.Product;
import com.example.duantn.entity.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TypeRepository extends JpaRepository<Type, Integer> {

    @Query(value = "SELECT p from Type p  where p.status = 1 ORDER BY p.idType desc ")
    List<Type> getTypeStatus1();

    @Query(value = "SELECT p from Type p  where p.status = 2")
    List<Type> getTypeStatus2();

    @Query(value = "SELECT p.typeName from Type p where p.typeName = :name and p.status = 1" )
    String findNameTypeByName(String name);

    @Query(value = "SELECT p from Product p where p.type.idType = :id and p.status = 1" )
    List<Product> getProductByType(Pageable page,int id);

    @Query(value = "SELECT u FROM Type u where u.typeName LIKE %:keyword% and u.status=1 ")
    List<Type> searchType(String keyword);

    @Query(value = "select s.typeName from Type s where s.typeName=:type and s.status=1")
    String checkTrung(String type);


}