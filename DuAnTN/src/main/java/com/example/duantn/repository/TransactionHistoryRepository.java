package com.example.duantn.repository;

import com.example.duantn.entity.Size;
import com.example.duantn.entity.TransactionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory,Integer> {
    @Query(value = "select tran from TransactionHistory tran where tran.responseCode='00'")
    List<TransactionHistory> findAllStatus00();
}
