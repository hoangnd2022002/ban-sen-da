package com.example.duantn.repository;

import com.example.duantn.entity.LightAspect;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LightAspectRepository extends JpaRepository<LightAspect, Integer> {

    @Query(value = "SELECT p from LightAspect p  where p.status = 1 ORDER BY p.idLightAspect desc ")
    List<LightAspect> getLightAspectStatus1();

    @Query(value = "SELECT p from LightAspect p  where p.status = 2")
    List<LightAspect> getLightAspectStatus2();

    @Query(value = "SELECT p.lightAspectName from LightAspect p where p.lightAspectName = :name and p.status = 1" )
    String findNameLightAspectByName(String name);

    @Query(value = "SELECT p from Product p where p.type.idType = :id and p.status = 1" )
    List<Product> getProductByLightAspect(Pageable page, int id);

    @Query(value = "SELECT u FROM LightAspect u where u.lightAspectName LIKE %:keyword% and u.status=1 ")
    List<LightAspect> searchLightAspect(String keyword);

    @Query(value = "select s.lightAspectName from LightAspect s where s.lightAspectName=:type and s.status=1")
    String checkTrung(String type);

}