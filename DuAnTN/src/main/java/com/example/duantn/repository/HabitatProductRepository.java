package com.example.duantn.repository;

import com.example.duantn.entity.HabitatsProduct;
import com.example.duantn.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;


public interface HabitatProductRepository extends JpaRepository<HabitatsProduct, Integer> {
    @Transactional
    @Modifying
    @Query(value = "update  HabitatsProduct hp set hp.status=2 where hp.habitats.idHabitats=:id")
    void updateStatus2(int id);
    @Transactional
    @Modifying
    @Query(value = "delete from  HabitatsProduct hp where hp.product.idProduct=:idProduct")
    void deleteByIDProduct(int idProduct);

    @Query(value = "select hp from Product p join HabitatsProduct hp on p.idProduct=hp.product.idProduct where hp.habitats.idHabitats=:idHabitat")
    List<Product> getProductByHabitat(int idHabitat);

    @Query(value = "select hp from HabitatsProduct hp where hp.habitats.idHabitats=:idHabitat")
    List<HabitatsProduct> getProductByHabitatTEST(int idHabitat);

    @Query(value = "select hp from HabitatsProduct hp where hp.status=1 and hp.product.idProduct=:idProduct")
    List<HabitatsProduct> getProductByH(int idProduct);
}
