package com.example.duantn.repository;

import com.example.duantn.entity.Orders;
import com.example.duantn.entity.OrderDetail;
import com.example.duantn.entity.ProductSize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
    OrderDetail getOrderDetailByProductSizeAndOrder(ProductSize product, Orders order);
    List<OrderDetail> getOrderDetailByOrder(Orders order);

    @Query("select od from OrderDetail od where od.order.idOrder=:id")
    List<OrderDetail> listOdByO(int id);

    @Query("select od.productSize.price from OrderDetail od where od.idOrderDetail=:id")
    Float getPriceVer2(int id);

    @Query("select od.price from OrderDetail od where od.order.idOrder=:id")
    Float getPrice(int id);

    @Query("select od.productSize.color.colorName from OrderDetail od where od.order.idOrder=:id")
    String getColorName(int id);

    @Query("select od.productSize.product.productName from OrderDetail od where od.order.idOrder=:id")
    String getProName(int id);

    @Query("select od.productSize.size.sizeName from OrderDetail od where od.order.idOrder=:id")
    String getSizeName(int id);

    @Modifying
    @Transactional
    @Query("update OrderDetail o set o.order.idOrder =:idOrder where o.idOrderDetail = :idOrderDetail")
    void updateGioHang(int idOrder,int idOrderDetail);

    @Modifying
    @Transactional
    @Query("update OrderDetail o set o.quantity =o.quantity + :soLuong where o.idOrderDetail = :idOrderDetail")
    void updateSanPham(int soLuong,int idOrderDetail);

    @Modifying
    @Query("update OrderDetail odt set odt.quantity =:quantity where odt.idOrderDetail =:idOrderDetail")
    OrderDetail updateQuantity(int quantity,int idOrderDetail);
//    void deleteOrderDetail(OrderDetail orderDetail);

    @Query(value = "select  p.productSize.idProductSize from OrderDetail p where p.idOrderDetail=:id")
    Integer getidProductSizeByByIdOrderDetail(int id);
    @Query(value = "select  p.productSize from OrderDetail p where p.idOrderDetail=:id")
    ProductSize getProductSizeByByIdOrderDetail(int id);
    @Modifying
    @Transactional
    @Query(value = "update OrderDetail od set od.price=:price,od.productName=:productName,od.sizeName=:sizeName,od.colorName=:colorName where od.idOrderDetail=:idOrderDetail")
    void update(Float price,String productName,String sizeName,String colorName,int idOrderDetail);

    @Query(value = "select  p.quantity from OrderDetail p where p.idOrderDetail=:id")
    Integer getsoLuong(int id);

    @Query(value = "select  count (p.idOrderDetail)from OrderDetail p where p.order.status='GIỎ HÀNG' and p.order.user.idUser=:idUser")
    Integer getsoLuongTheoUser(int idUser);
}