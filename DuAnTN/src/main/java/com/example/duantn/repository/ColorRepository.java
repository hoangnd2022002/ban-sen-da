package com.example.duantn.repository;

import com.example.duantn.entity.Color;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface ColorRepository extends JpaRepository<Color,Integer> {


    @Query(value = "select s.colorName from Color s where s.colorName=:color and s.status=1")
    String checkTrung(String color);

//    List<Color> getAllByCate(int id);

    @Query(value = "select c from Color c where c.status = 1")
    List<Color> getAllColorStatus1();

//    @Query(value = "SELECT * FROM datn_test.product_color where id_product=:product and id_color=:color",nativeQuery = true)
//    Optional<ProductColor> checkTonTaiTrongProColor(int product, int color);
//
//    @Query(value = "SELECT * FROM datn_test.product_color where id_product=:product and status=1",nativeQuery = true)
//    List<ProductColor> searchProductAndColorStatus1(int product);
//    @Query(value = "SELECT * FROM datn_test.product_color where id_product=:product and status=2",nativeQuery = true)
//    List<ProductColor> searchProductAndColorStatus2(int product);
//    @Query(value = "SELECT * FROM datn_test.product_color where id_product=:product ",nativeQuery = true)
//    List<ProductColor> searchProductAndColorStatus(int product);
//
//
//
//    @Modifying
//    @Transactional
//    @Query(value = "UPDATE ProductColor p set p.status=2 where p.product.idProduct=:product ")
//    void changeStatusByProduct(int product);



}
