package com.example.duantn.security.dto;

import lombok.*;

import java.util.Collection;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    private Long userId;
    private String userName;
    private String fullName;
    private String email;
    private Collection<?> role;
    private Collection<?> urlEndpoints;
    private Boolean firstLogin;
    private Boolean isChangePwd;
    private Boolean isNormalTwoFactorAuthUsed;
}
