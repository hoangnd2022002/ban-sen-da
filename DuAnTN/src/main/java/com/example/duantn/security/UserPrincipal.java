package com.example.duantn.security;


import com.example.duantn.entity.Role;
import com.example.duantn.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class UserPrincipal implements UserDetails {
    private Integer id;
    @JsonIgnore
    private String email;
    private String fullName;

    @JsonIgnore
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    public static final String ROLE_PREFIX = "ROLE";
    public static final String ROLE_PERMISSION_CONNECTOR = "_";

    public static UserPrincipal create(User user) {
        Role rol = user.getRole();
        List<Role> roles = new ArrayList<>();
        roles.add(rol);
        List<String> rights = getPermission(roles);
        List<GrantedAuthority> authorities = rights.stream().map(r -> new SimpleGrantedAuthority(r)).collect(Collectors.toList());
        return new UserPrincipal(user.getIdUser(), user.getEmail(), user.getUsername(), user.getPassword(), authorities);
    }

    private static List<String> getPermission(Collection<Role> roles) {
        List<String> rights = new ArrayList<>();
        for (Role role : roles) {
            rights.add(String.valueOf(role.getIdRole()));
        }
        return rights;
    }

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
