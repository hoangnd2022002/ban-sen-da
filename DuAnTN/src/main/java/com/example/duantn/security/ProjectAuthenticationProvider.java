package com.example.duantn.security;


import com.example.duantn.entity.User;
import com.example.duantn.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component()
@RequiredArgsConstructor
@Slf4j
public class ProjectAuthenticationProvider implements AuthenticationProvider {
    private final UserRepository userRepository;
    @Override
    @Transactional
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        AuthenticationToken authenticationToken = (AuthenticationToken) authentication;
        String username = authenticationToken.getPrincipal().toString();
        User authenticatedUser = userRepository.searchUserByUsername(username);
        if (authenticatedUser.getUsername().equals(username)){
            if (authenticatedUser!=null){
//        String pwd = Base64Common.decodeBaseToString(authenticationToken.getPassword());

                if (BCrypt.checkpw(authenticationToken.getPassword(), authenticatedUser.getPassword())) {
//            if (authenticationToken.getPassword().equals(authenticatedUser.get().getPassword().concat(authenticatedUser.get().getEmail()))) {
                    UserPrincipal userDetails = UserPrincipal.create(authenticatedUser);
                    return new UsernamePasswordAuthenticationToken(authenticatedUser, authentication.getCredentials(), userDetails.getAuthorities());
                } else {
                    throw new BadCredentialsException("Mật khẩu không đúng");
//            throw new RestException(ApiResponseCode.EMAIL_OR_PASSWORD_INVALID);
                }
            }
        }
        throw new BadCredentialsException("Tài khoản hoặc mật khẩu không đúng");
    }
    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
