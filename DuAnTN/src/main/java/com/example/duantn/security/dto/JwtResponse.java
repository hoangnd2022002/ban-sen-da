package com.example.duantn.security.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {
    private String accessToken;
    private String role;
    private String refreshToken;
    private UserResponse user;

    public JwtResponse(String accessToken,String role) {
        this.accessToken = accessToken;
        this.role = role;
    }
}
