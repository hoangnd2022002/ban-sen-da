package com.example.duantn.conf;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.dto.TransactionStatusDTO;
import com.example.duantn.entity.Orders;
import com.example.duantn.entity.TransactionHistory;
import com.example.duantn.entity.User;
import com.example.duantn.repository.OrderRepository;
import com.example.duantn.repository.TransactionHistoryRepository;
import com.example.duantn.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@CrossOrigin("*")
@RequiredArgsConstructor
public class DataPaymentController {

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final TransactionHistoryRepository transactionHistoryRepository;

    @GetMapping("/thong-tin-thanh-toan")
    public RedirectView transactionHandle(
            @RequestParam(value = "vnp_Amount",required = false)String        amount,
            @RequestParam(value = "vnp_BankCode",required = false)String      bankCode,
            @RequestParam(value = "vnp_BankTranNo",required = false)String    bankTranNo,
            @RequestParam(value = "vnp_CodeType",required = false)String      cardType,
            @RequestParam(value = "vnp_OrderInfo",required = false)String     orderInfo,
            @RequestParam(value = "vnp_PayDate",required = false)String       payDate,
            @RequestParam(value = "vnp_ResponseCode",required = false)String  responseCode,
            @RequestParam(value = "vnp_TmnCode",required = false)String       tmnCode,
            @RequestParam(value = "vnp_TransactionNo",required = false)String transactionNo,
            @RequestParam(value = "vnp_TxnRef",required = false)String        txnRef,
            @RequestParam(value = "vnp_SecureHash",required = false)String    secureHash
    ){
        try {
            User u = orderRepository.getUserFromOrder(Integer.parseInt(txnRef.substring(6)));
            Integer idUser = u.getIdUser();
    //        int idUser=1;
            TransactionStatusDTO result = new TransactionStatusDTO();
            if (responseCode.equalsIgnoreCase("02")){
                result.setStatus("02");
                result.setMessage("failed");
                result.setData(null);
                return new RedirectView(StatusOrder.HOST +"/vnpay/error");
            }
            User user= userRepository.findById(idUser).orElse(null);
            if (user==null) {
                result.setStatus("NOT FOUND USER");
                result.setMessage("failed");
                result.setData(null);
                return new RedirectView(StatusOrder.HOST +"/vnpay/error");

            }
            if (orderRepository.getOrderGioHangFromUser(Integer.parseInt(txnRef.substring(6)), user.getIdUser()) == null) {
                result.setStatus("11");
                result.setStatus("Order không tồn tại");
                result.setData(null);
            }
            Orders orders = orderRepository.getOrderGioHangFromUser(Integer.parseInt(txnRef.substring(6)), user.getIdUser());
            if (orders.getStatus().equalsIgnoreCase("TRẢ TRƯỚC")){
                result.setData(null);
                result.setMessage("Order đã tồn tại");
                result.setStatus("12");
                return new RedirectView(StatusOrder.HOST +"/vnpay/error");

            }
            if (String.valueOf(orders.getIdOrder()).equalsIgnoreCase(txnRef.substring(6))){
                TransactionHistory transactionHistory = new TransactionHistory();
                transactionHistory.setAmount( amount);
                transactionHistory.setBankCode( bankCode);
                transactionHistory.setBankTranNo( bankTranNo);
                transactionHistory.setCardType( cardType);
                transactionHistory.setOrderInfo( orderInfo);
                transactionHistory.setPayDate( payDate);
                transactionHistory.setResponseCode( responseCode);
                transactionHistory.setTmnCode( tmnCode);
                transactionHistory.setTransactionNo( transactionNo);
                transactionHistory.setTxnRef( txnRef);
                transactionHistory.setSecureHash( secureHash);
                transactionHistoryRepository.save(transactionHistory);
                if (responseCode.equalsIgnoreCase("00")){
                    return new RedirectView(StatusOrder.HOST +"/vnpay/success");
                }else {
                    return new RedirectView(StatusOrder.HOST +"/cart");
                }
            }
            else {
                result.setData(null);
                result.setMessage("Lỗi k vào lỗi nào");
                result.setStatus("999");
                return new RedirectView(StatusOrder.HOST +"/vnpay/error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new RedirectView(StatusOrder.HOST +"/vnpay/error");
        }


    }

}
