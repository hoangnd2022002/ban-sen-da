package com.example.duantn.conf;

import com.example.duantn.security.JwtAuthenticationEntryPoint;
import com.example.duantn.security.jwt.JWTConfigurer;
import com.example.duantn.security.jwt.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@EnableWebSecurity
@CrossOrigin("*")
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//    @Autowired
//    CustomUserDetailService customUserDetailService;


//    private  final  BCryptPasswordEncoder pe;

    private final TokenProvider tokenProvider;
    private final JwtAuthenticationEntryPoint entryPoint;

//    public WebSecurityConfig(
//            TokenProvider tokenProvider,
//            JwtAuthenticationEntryPoint entryPoint
//    ) {
//        this.tokenProvider = tokenProvider;
//        this.entryPoint = entryPoint;
//    }

    //Mã hóa mật khẩu
//    @Bean
//    public PasswordEncoder getPassword() {
//        return new BCryptPasswordEncoder();
//    }

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable();
//        http.authorizeRequests()
////                .antMatchers("/api/**").permitAll()
//                .antMatchers("/api/**").authenticated()
//                .anyRequest().permitAll()
//        .and()
//        .apply(securityConfigurerAdapter());
//    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(entryPoint)
                .accessDeniedHandler(entryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/api/product/free/**").permitAll()
                .antMatchers("/image/**").permitAll()
                .antMatchers("/api-email/**").permitAll()
                .antMatchers("/api/category-dad/free/**").permitAll()
                .antMatchers("/api/user/free/**").permitAll()
                .antMatchers("/api/type/free/**").permitAll()
                .antMatchers("/api/size/free/**").permitAll()
                .antMatchers("/api/color/free/**").permitAll()
                .antMatchers("/api/habitat/free/**").permitAll()
                .antMatchers("/api/light-aspect/free/**").permitAll()
                .antMatchers("/api/**")
                .authenticated()
                .and()
                .addFilterBefore(new FilterCheckJWT(), BasicAuthenticationFilter.class)
                .apply(securityConfigurerAdapter());

    }


//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(customUserDetailService).passwordEncoder(getPassword());
//    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
