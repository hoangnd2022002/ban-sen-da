/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.duantn.conf;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.Config;
import com.example.duantn.dto.PaymentDTO;
import com.example.duantn.dto.PaymentResDTO;
import com.example.duantn.dto.TransactionStatusDTO;
import com.example.duantn.entity.*;
import com.example.duantn.repository.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author CTT VNPAY
 */
@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
@RequestMapping("/api")
public class ajaxServlet extends HttpServlet {

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final TransactionHistoryRepository transactionHistoryRepository;
    private final ProductSizeRepository productSizeRepository;

    @PostMapping("/vnpay")
    public ResponseEntity<PaymentResDTO> doPost(@RequestBody PaymentDTO req) throws ServletException, IOException {
        PaymentResDTO result = new PaymentResDTO();
        List<Integer> integerList = new ArrayList<>();
        Orders orders = orderRepository.findById(req.getIdOrder()).orElse(null);

        for (OrderDetail orderDetail : req.getOrderDetailsList()) {
            ProductSize o = new ProductSize();
            o = productSizeRepository.findById(orderDetail.getProductSize().getIdProductSize()).orElse(null);
            integerList.add(o.getStatus());
        }
        boolean coChuaStatus2 = integerList.contains(2);
        if (!coChuaStatus2) {
                String vnp_Version = "2.1.0";
                String vnp_Command = "pay";
                String orderType = "other";
                long amount = Integer.parseInt(req.getAmount()) * 100;
                String bankCode = "NCB";


                String vnp_TxnRef = Config.getRandomNumber(6) + String.valueOf(req.getIdOrder());
                String vnp_IpAddr = "0.0.0.0.0.0.0.1";
                String vnp_TmnCode = Config.vnp_TmnCode;

                Map<String, String> vnp_Params = new HashMap<>();
                vnp_Params.put("vnp_Version", vnp_Version);
                vnp_Params.put("vnp_Command", vnp_Command);
                vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
                vnp_Params.put("vnp_Amount", String.valueOf(amount));
                vnp_Params.put("vnp_CurrCode", "VND");

                if (bankCode != null && !bankCode.isEmpty()) {
                    vnp_Params.put("vnp_BankCode", bankCode);
                }
                vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
                vnp_Params.put("vnp_OrderInfo", "Thanh toan don hang:" + vnp_TxnRef);
                vnp_Params.put("vnp_OrderType", orderType);

                String locate = "vn";
                if (locate != null && !locate.isEmpty()) {
                    vnp_Params.put("vnp_Locale", locate);
                } else {
                    vnp_Params.put("vnp_Locale", "vn");
                }
                vnp_Params.put("vnp_ReturnUrl", Config.vnp_Returnurl);
                vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

                Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                String vnp_CreateDate = formatter.format(cld.getTime());
                vnp_Params.put("vnp_CreateDate", vnp_CreateDate);

                cld.add(Calendar.MINUTE, 15);
                String vnp_ExpireDate = formatter.format(cld.getTime());
                vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);

                List fieldNames = new ArrayList(vnp_Params.keySet());
                Collections.sort(fieldNames);
                StringBuilder hashData = new StringBuilder();
                StringBuilder query = new StringBuilder();
                Iterator itr = fieldNames.iterator();
                while (itr.hasNext()) {
                    String fieldName = (String) itr.next();
                    String fieldValue = (String) vnp_Params.get(fieldName);
                    if ((fieldValue != null) && (fieldValue.length() > 0)) {
                        //Build hash data
                        hashData.append(fieldName);
                        hashData.append('=');
                        hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                        //Build query
                        query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                        query.append('=');
                        query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                        if (itr.hasNext()) {
                            query.append('&');
                            hashData.append('&');
                        }
                    }
                }
                String queryUrl = query.toString();
                String vnp_SecureHash = Config.hmacSHA512(Config.vnp_HashSecret, hashData.toString());
                queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
                String paymentUrl = Config.vnp_PayUrl + "?" + queryUrl;
                com.google.gson.JsonObject job = new JsonObject();
                job.addProperty("code", "00");
                job.addProperty("message", "success");
                job.addProperty("data", paymentUrl);

                result.setUrl(paymentUrl);
                result.setMessage("success");
                result.setStatus("00");

            } else {
                result.setUrl(StatusOrder.HOST +"/home");
            }
        return ResponseEntity.status(HttpStatus.OK).body(result);
        }
    }

