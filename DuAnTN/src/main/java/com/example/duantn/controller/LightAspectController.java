package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.entity.LightAspect;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.service.LightAspectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/light-aspect")
@RequiredArgsConstructor
public class LightAspectController {

    private final LightAspectService lightAspectService;

    @PostMapping("/add-light-aspect")
    public ResponseEntity<ResponseDTO<LightAspect>> addLightAspect(@RequestBody LightAspect lightAspect){
        return ResponseEntity.ok().body(lightAspectService.addLightAspect(lightAspect));
    }
    @PostMapping("/edit-light-aspect")
    public ResponseEntity<ResponseDTO<LightAspect>> editLightAspect(@RequestBody LightAspect lightAspect){
        return ResponseEntity.ok().body(lightAspectService.editLightAspect(lightAspect));
    }
    @PostMapping("/delete-light-aspect")
    public ResponseEntity<ResponseDTO<LightAspect>> deleteLightAspect(@RequestBody LightAspect lightAspect){
        return ResponseEntity.ok().body(lightAspectService.deleteLightAspect(lightAspect));
    }
    @GetMapping("/free/get-all-light-aspect1")
    public ResponseEntity<ResponseDTO<List<LightAspect>>> getAllLightAspect1(){
        return ResponseEntity.ok().body(lightAspectService.getAll1());
    }
    @GetMapping("/free/get-all-light-aspect2")
    public ResponseEntity<ResponseDTO<List<LightAspect>>> getAllLightAspect2(){
        return ResponseEntity.ok().body(lightAspectService.getAll2());
    }
    @PostMapping("/free/get-detail")
    public ResponseEntity<ResponseDTO<LightAspect>> getLightAspectByID(@RequestBody SearchReq searchReq){
        return ResponseEntity.ok().body(lightAspectService.getLightAspectByIDLightAspect(Integer.parseInt(searchReq.getKeyword())));
    }
    @PostMapping("/free/get-by-name")
    public ResponseEntity<ResponseDTO<List<LightAspect>>> getLightAspectByName(@RequestBody SearchReq searchReq){
        return ResponseEntity.ok().body(lightAspectService.getLightAspectByName(searchReq.getKeyword()));
    }
    @PostMapping("/free/get-product-detail-by-light-aspect")
    public ResponseEntity<ResponseDTO<List<ProductSize>>> getListProductSizeByLightAspect(@RequestParam(defaultValue = "10") int pageSize,
                                                                                          @RequestParam(defaultValue = "0") int page,@RequestBody SearchReq searchReq){
        if (page <0){
            return null;
        }else {
            return ResponseEntity.ok().body(lightAspectService.getProductSizeByLightAspect(page,pageSize,Integer.parseInt(searchReq.getKeyword())));

        }
    }
    @PostMapping("/free/get-product-by-light-aspect")
    public ResponseEntity<ResponseDTO<List<Product>>> getProductByLightAspect(@RequestParam(defaultValue = "10") int pageSize,
                                                                              @RequestParam(defaultValue = "0") int page,@RequestBody SearchReq searchReq){
        if (page <0){
            return null;
        }else {
            return ResponseEntity.ok().body(lightAspectService.getProductByLightAspect(page,pageSize,Integer.parseInt(searchReq.getKeyword())));
        }

    }
}
