package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.Color;
import com.example.duantn.service.ColorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/color")
@RequiredArgsConstructor
public class ColorController {
    private final ColorService colorService;
    @PostMapping("/add-color")
    public ResponseEntity<ResponseDTO<Color>> addColor(@RequestBody Color color){
        return ResponseEntity.ok().body(colorService.addColor(color));
    }
    @PostMapping("/edit-color")
    public ResponseEntity<ResponseDTO<Color>> editColor(@RequestBody Color color){
        return ResponseEntity.ok().body(colorService.editColor(color));
    }

    @PostMapping("/delete-color")
    public ResponseEntity<ResponseDTO<Color>> deleteColor(@RequestBody Color color){
        return ResponseEntity.ok().body(colorService.deleteColor(color));
    }
    @GetMapping("/free/get-all-color")
    public ResponseEntity<ResponseDTO<List<Color>>> getAllColor(){
        return ResponseEntity.ok().body(colorService.getAll());
    }
//    @GetMapping("/get-all-color-by-cate-dad/{id}")
//    public ResponseEntity<ResponseDTO<List<Color>>> getAllColorByCateDad(@PathVariable int id){
//        return ResponseEntity.ok().body(colorService.getByCateDad(id));
//    }
}
