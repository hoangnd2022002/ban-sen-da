package com.example.duantn.controller;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.ProductDTOReq;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.repository.ProductSizeRepository;
import com.example.duantn.service.impl.ProductServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductServiceImpl productService;


    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/get-all-product")
    public ResponseEntity<Page<ProductDTORes>> getAll(@RequestParam(defaultValue = "0") int currentPage){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getAllProducts();
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
//
    @GetMapping("/free/get-all-product1")
    public ResponseEntity<Page<ProductDTORes>> getAllStatus1(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
//        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getAllProductsStatus1();
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }

//    @GetMapping("/image/{filename}")
//    public ResponseEntity<byte[]> getImage(@PathVariable String filename) throws IOException {
//        byte[] data = Files.readAllBytes(Path.of(StatusOrder.PATH_IMAGE + filename));
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.IMAGE_JPEG);
//        return new ResponseEntity<>(data, headers, HttpStatus.OK);
//    }
    @GetMapping("/free/get-all-product-het-so-luong")
    public ResponseEntity<Page<ProductDTORes>> getAllStatusSoLuong(
            @RequestParam(defaultValue = "") String name,
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize

    ){
//        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getAllProductHetSoLuong(name);
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }

    @PostMapping("/free/get-all-product-size1")
    public ResponseEntity<List<ProductSize>> getAllPSStatus1(
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "1") int category,
            @RequestBody SearchReq searchReq
    ){
        if (page <0){
            return null;
        }
        List<ProductSize> productDTORes = productService.getAllProductsSizeStatus1(page, pageSize,category,searchReq.getKeyword());
        return ResponseEntity.ok().body(productDTORes);
    }


    @GetMapping("/get-all-product2")
    public ResponseEntity<Page<ProductDTORes>> getAllStatus2(@RequestParam(defaultValue = "0") int currentPage){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getAllProductsStatus2();
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }

    @PostMapping("/free/get-detail")
    public ResponseEntity<ResponseDTO<ProductDTORes>> getByID(
            @RequestBody ProductDTOReq productDTOReq,
            @RequestParam(defaultValue = "1") int status
            ){
        productDTOReq.setStatus(status);
        ResponseDTO<ProductDTORes> p =productService.getDetailProduct(productDTOReq);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/save-product")
    public ResponseEntity<ResponseDTO<ProductDTORes>> saveProduct(@RequestBody ProductDTOReq productDTOReq){
        ResponseDTO<ProductDTORes> p =productService.addProduct(productDTOReq);
        return ResponseEntity.ok().body(p);
    }

    @PostMapping("/get-detail-by-qr-code")
    public ResponseEntity<ResponseDTO<ProductSize>> getDetailByQRCode(@RequestBody ProductSize productSize){
        ResponseDTO<ProductSize> p =productService.getDetailByQrCode(productSize.getQrCode());
        return ResponseEntity.ok().body(p);
    }

    @PostMapping("/delete-product")
    public void deleteProduct(@RequestBody ProductDTOReq productDTOReq){
         productService.deleteProduct(productDTOReq);
    }

    @PostMapping("/edit-product")
    public ResponseEntity<ResponseDTO<ProductDTORes>> editProduct(@RequestBody ProductDTOReq productDTOReq ) throws IOException {

        ResponseDTO<ProductDTORes> p =productService.editProduct(productDTOReq);
        return ResponseEntity.ok().body(p);
    }

    @PostMapping("/ban-lai-product")
    public ResponseEntity<ResponseDTO<ProductDTORes>> banLaiProduct(@RequestBody ProductDTOReq productDTOReq ) throws IOException {

        ResponseDTO<ProductDTORes> p =productService.banLaiProduct(productDTOReq);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/free/find-product-by-idcatedad/{id}")
    public ResponseEntity<Page<ProductDTORes>> findByIDCateDad(
            @PathVariable int id
            ,@RequestParam(defaultValue = "0") int currentPage
            ,@RequestParam(defaultValue = "10") int pageSize
    ){
        int startItem = currentPage * pageSize;
        List<ProductDTORes> productDTORes = productService.getAllProductsByType(id);
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }

    @PostMapping("/free/find-product-by-idcatedad-no-page/{id}")
    public ResponseEntity<List<Product>> findByIDCateDadAndNameNoPage(
            @PathVariable int id,@RequestBody SearchReq searchReq
    ){
        List<Product> productSizeList = productService.getProductByNameAndType(searchReq,id);
        return ResponseEntity.ok().body(productSizeList);
    }


    @PostMapping("/free/get-all-product-by-keyword-and-cate")
    public ResponseEntity<Page<ProductDTORes>> getPByKeywordAndCate(@RequestParam(defaultValue = "0") int currentPage , @RequestBody SearchReq searchReq){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getProductByName(searchReq);
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @PostMapping("/free/get-all-product-ngung-ban-by-keyword-and-cate")
    public ResponseEntity<Page<ProductDTORes>> getPNgungBan(@RequestParam(defaultValue = "0") int currentPage , @RequestBody SearchReq searchReq){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getProductByNameNgungBan(searchReq);
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @PostMapping("/free/get-all-product-by-keyword")
    public ResponseEntity<Page<ProductDTORes>> getPByKeyword(@RequestParam(defaultValue = "0") int currentPage , @RequestBody SearchReq searchReq){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getProductByNamePro(searchReq);
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @PostMapping("/free/get-all-product-by-price")
    public ResponseEntity<Page<ProductDTORes>> getPByPrice(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "12")int pageSize
            , @RequestBody SearchReq searchReq){
//        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getProductByPrice(searchReq);
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @GetMapping("/free/get-all-product-by-priceASC")
    public ResponseEntity<Page<ProductDTORes>> getPByPriceTangDan(@RequestParam(defaultValue = "0") int currentPage ){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getProductPriceTangDan();
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }@GetMapping("/free/get-all-product-by-priceDESC")
    public ResponseEntity<Page<ProductDTORes>> getPByPriceGiamDan(@RequestParam(defaultValue = "0") int currentPage){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        if (currentPage<0){
            return null;
        }
        List<ProductDTORes> productDTORes = productService.getProductPriceGiamDan();
        List<ProductDTORes> productList = productDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<ProductDTORes> pages = new PageImpl<>(productList, PageRequest.of(currentPage, pageSize), productDTORes.size());
        return ResponseEntity.ok().body(pages);
    }


    @Value("${jwt.secret}")
    private String secretKey;
    private final ProductSizeRepository productSizeRepository;

    @GetMapping("/free/get-expiration-token")
   public boolean getJwtExpiration(@RequestHeader(HttpHeaders.AUTHORIZATION) String Authorization ) {
       try {
           Jws<Claims> jws = Jwts.parser().setSigningKey(secretKey)
                   .parseClaimsJws(Authorization.substring(6));
           Claims claims = jws.getBody();
           return claims.getExpiration().after(Date.from(Instant.now()));
       }catch (Exception e){
           return false;
       }
   }

}
