package com.example.duantn.controller;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.OrderDTO;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.entity.*;
import com.example.duantn.repository.OrderDetailRepository;
import com.example.duantn.repository.OrderRepository;
import com.example.duantn.repository.ProductRepository;
import com.example.duantn.repository.ProductSizeRepository;
import com.example.duantn.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.io.IOException;
import java.security.Principal;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/order-xac-nhan")
@RequiredArgsConstructor
public class OrderControllerHyu {

    private final OrderService orderService;

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;
    private final ProductSizeRepository productSizeRepository;
    private final OrderDetailRepository orderDetailRepository;


    @PostMapping("/them-vao-gio")
    public ResponseEntity themVaoGio(
            @RequestBody ProductSize productSize,
            Principal principal
            ){
        String userString = principal.getName();
        Integer idUser = Integer.valueOf(userString.substring(userString.indexOf("=")+1,userString.indexOf(","))) ;

        return orderService.themVaoGio(productSize,User.builder().idUser(idUser).build());
    }

    @PostMapping("/ban-tai-quay")
    public ResponseEntity<ResponseDTO<OrderDTO>> banTaiQuay(@RequestBody OrderDTO orderDTO,Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
        orderDTO.setIdUser(idUser);
        ResponseDTO<OrderDTO> p =orderService.xacNhanTaiQuayDonHang(orderDTO);
        return ResponseEntity.ok().body(p);
    }

    @GetMapping("/get-cart")
    public ResponseEntity getCart(
            Principal principal
    ){
        String userString = principal.getName();
        Integer idUser = Integer.valueOf(userString.substring(userString.indexOf("=")+1,userString.indexOf(","))) ;
        return ResponseEntity.ok(orderService.getOrderDetail(User.builder().idUser(idUser).build()));
    }
    @PostMapping("/get-orderDetail")
    public ResponseEntity getOrderDetail(
            @RequestBody() Orders orders
    ){
        return ResponseEntity.ok(orderService.getOrderDetail(orders));
    }
    @GetMapping("/get-order-by-id/{id}")
    public ResponseEntity  getOrder(
            @PathVariable("id") Integer id
    ){
        return ResponseEntity.ok(orderService.getOrder(id));
    }

    @PostMapping("/get-order-online")
    public ResponseEntity getOrderOnline(@RequestBody SearchReq searchReq){
        searchReq.setNgayBatDau(Timestamp.valueOf(searchReq.getNgayBatDau().toLocalDateTime().with(LocalTime.MIN)));
        searchReq.setNgayKetThuc(Timestamp.valueOf(searchReq.getNgayKetThuc().toLocalDateTime().with(LocalTime.MAX)));
        return ResponseEntity.ok(orderRepository.getOrderOnline(searchReq.getNgayBatDau(),searchReq.getNgayKetThuc()));
    }

    @PostMapping("/free/get-order-all")
    public void getOrderOnlineAll(){
        List<Orders> orders=orderRepository.findAll();
        for ( Orders orders1 : orders ) {
            List<OrderDetail> orderDetailList = orderDetailRepository.getOrderDetailByOrder(orders1);
            for ( OrderDetail orderDetail : orderDetailList ) {
                ProductSize productSize = productSizeRepository.checkProductSize(orderDetail.getProductSize().getIdProductSize()).orElse(null);
                if (productSize!=null){
                    orderDetailRepository.update(productSize.getPrice(),productSize.getProduct().getProductName(),productSize.getSize().getSizeName(),productSize.getColor().getColorName(),orderDetail.getIdOrderDetail());
                }
            }
        }
    }
    @PostMapping("/get-order-xan-nhan-huy")
    public ResponseEntity getOrderXacNhanHuy(@RequestBody SearchReq searchReq){
        searchReq.setNgayBatDau(Timestamp.valueOf(searchReq.getNgayBatDau().toLocalDateTime().with(LocalTime.MIN)));
        searchReq.setNgayKetThuc(Timestamp.valueOf(searchReq.getNgayKetThuc().toLocalDateTime().with(LocalTime.MAX)));
        return ResponseEntity.ok(orderRepository.getOrderXacNhanHuy(searchReq.getNgayBatDau(),searchReq.getNgayKetThuc()));
    }
    @PostMapping("/get-order-by-day")
    public ResponseEntity getOrderByDay(@RequestBody SearchReq searchReq
            ,@RequestParam(defaultValue = "10") int pageSize
            ,@RequestParam(defaultValue = "0") int pageNumber){
        if (pageNumber<0){
            return null;
        }else {
        Pageable page =PageRequest.of(pageNumber, pageSize, Sort.by("idOrder").ascending());
        searchReq.setNgayBatDau(Timestamp.valueOf(searchReq.getNgayBatDau().toLocalDateTime().with(LocalTime.MIN)));
        searchReq.setNgayKetThuc(Timestamp.valueOf(searchReq.getNgayKetThuc().toLocalDateTime().with(LocalTime.MAX)));
        return ResponseEntity.ok(orderRepository.getOrderByDay(page,searchReq.getKeyword(),searchReq.getNgayBatDau(),searchReq.getNgayKetThuc()));
        }
    }
    @PostMapping("/get-order-offline")
    public ResponseEntity getOrderOffline(@RequestBody SearchReq searchReq){

        searchReq.setNgayBatDau(Timestamp.valueOf(searchReq.getNgayBatDau().toLocalDateTime().with(LocalTime.MIN)));
        searchReq.setNgayKetThuc(Timestamp.valueOf(searchReq.getNgayKetThuc().toLocalDateTime().with(LocalTime.MAX)));
        return ResponseEntity.ok(orderRepository.getOrderOffline(searchReq.getNgayBatDau(),searchReq.getNgayKetThuc()));
    }

    @GetMapping("/get-order-detail-by-id/{id}")
    public ResponseEntity getOrderDetailById(
            @PathVariable("id") Integer id
    ){
        return ResponseEntity.ok(orderService.getOrderDetailById(id));
    }

    @PutMapping("/update-quantity")
    public ResponseEntity updateQuantity(
            @RequestBody() OrderDetail orderDetail
    ){
        orderService.updateQuanTity(orderDetail);
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/delete-product-in-cart/{idOrderDetail}")
    public void deleteProductInCart(
            Principal principal,
            @PathVariable("idOrderDetail")Integer idOrderDetail
    ){
        System.out.println(principal.getName());
        orderService.deleteOrderDetail(OrderDetail.builder().idOrderDetail(idOrderDetail).build());
    }

    @PostMapping("/huy-khi-dang-giao")
    public ResponseEntity<ResponseDTO<OrderDTO>> huyKhiDangGiao(@RequestBody OrderDTO orderDTO){
        ResponseDTO<OrderDTO> p =orderService.huyKhiDangGiao(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/cho-xac-nhan-huy-tai-quay")
    public ResponseEntity<ResponseDTO<Orders>> choXacNhanHuyTaiQuay(@RequestBody Orders orders){
        ResponseDTO<Orders> p =orderService.choXacNhanHuyTaiQuay(orders);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/xac-nhan-huy-tai-quay")
    public ResponseEntity<ResponseDTO<Orders>> xacNhanHuyTaiQuay(@RequestBody Orders orders,
                                                                 Principal principal){

        String userString = principal.getName();
        Integer idUser = Integer.valueOf(userString.substring(userString.indexOf("=")+1,userString.indexOf(","))) ;
        ResponseDTO<Orders> p =orderService.xacNhanHuyTaiQuay(orders,idUser);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/huy-bo-huy-tai-quay")
    public ResponseEntity<ResponseDTO<Orders>> huyBoHuyTaiQuay(@RequestBody Orders orders){
        ResponseDTO<Orders> p =orderService.huyBoHuyTaiQuay(orders);
        return ResponseEntity.ok().body(p);
    }

    @PostMapping("/cho-xac-nhan")
    public ResponseEntity<ResponseDTO<OrderDTO>> choXacNhan(
            @RequestBody OrderDTO orderDTO,
            Principal principal
    ){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
        orderDTO.setIdUser(idUser);
        ResponseDTO<OrderDTO> p =orderService.choXacNhan(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/cho-xac-nhan/vn-pay")
    public ResponseEntity<ResponseDTO<OrderDTO>> choXacNhanVNPay(
            @RequestBody OrderDTO orderDTO,
            Principal principal
    ) throws ServletException, IOException {
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
        orderDTO.setIdUser(idUser);
        ResponseDTO<OrderDTO> p =orderService.choXacNhanVNPay(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/dang-cbi-hang")
    public ResponseEntity<ResponseDTO<OrderDTO>> chuanBiDonHang(@RequestBody OrderDTO orderDTO){
        ResponseDTO<OrderDTO> p =orderService.xacNhanDangChuanBiDonHang(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("dang-giao")
    public ResponseEntity<ResponseDTO<OrderDTO>> dangGiao(@RequestBody OrderDTO orderDTO){
        ResponseDTO<OrderDTO> p =orderService.xacNhanDangGiaoDonHang(orderDTO);
        return ResponseEntity.ok().body(p);
    }@PostMapping("/da-giao")
    public ResponseEntity<ResponseDTO<OrderDTO>> daGiao(@RequestBody OrderDTO orderDTO){
        ResponseDTO<OrderDTO> p =orderService.xacNhanDaGiaoDonHang(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/da-nhan")
    public ResponseEntity<ResponseDTO<OrderDTO>> daNhan(@RequestBody OrderDTO orderDTO){
        ResponseDTO<OrderDTO> p =orderService.xacNhanDaNhanDonHang(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("/da-huy")
    public ResponseEntity<ResponseDTO<OrderDTO>> daHuy(@RequestBody OrderDTO orderDTO){
        ResponseDTO<OrderDTO> p =orderService.xacNhanHuyDonHang(orderDTO);
        return ResponseEntity.ok().body(p);
    }
    @PostMapping("so-luong-dang-giao")
    public int soLuongDangGiao(){
        String trangThai=StatusOrder.DANG_GIAO;
        return orderService.soLuongTheoTrangThai(trangThai);
    }@PostMapping("/so-luong-da-giao")
    public int soLuongDaGiao(){
        String trangThai=StatusOrder.DA_GIAO;
        return orderService.soLuongTheoTrangThai(trangThai);
    }
    @PostMapping("/so-luong-da-nhan")
    public int soLuongDaNhan(){
        String trangThai=StatusOrder.DA_NHAN;
        return orderService.soLuongTheoTrangThai(trangThai);
    }
    @PostMapping("/so-luong-da-huy")
    public int soLuongDaHuy(){
        String trangThai=StatusOrder.DA_HUY;
        return orderService.soLuongTheoTrangThai(trangThai);
    }
    @GetMapping("/so-luong-da-het-hang")
    public int soLuongDaHetHang(){
        return productRepository.listProduct();
    }
    @PostMapping("/so-luong-da-xac-nhan")
    public int soLuongDaXacNhan(){
        String trangThai=StatusOrder.DA_XAC_NHAN;
        return orderService.soLuongTheoTrangThai(trangThai);
    }
    @GetMapping("/so-luong-chua-xac-nhan")
    public int soLuongDaChuaXacNhan(){
        String trangThai=StatusOrder.CHO_XAC_NHAN;
        return orderService.soLuongTheoTrangThai(trangThai);
    }
    @GetMapping("/so-luong-gio-hang-theo-user")
    public int soLuongDaChuaXacNhan(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
        return orderService.soLuongTheoGioHangUser(idUser);
    }
    @GetMapping("/lay-order-chua-xac-nhan-user")
    public ResponseEntity<List<Orders>> chuaXacNhanUser(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
//        String status = "CHƯA XÁC NHẬN";
        List<Orders> p =orderService.getOrderByUser(idUser,StatusOrder.CHO_XAC_NHAN);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/lay-order-da-xac-nhan-user")
    public ResponseEntity<List<Orders>> daXacNhanUser(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
//        String status = "ĐÃ XÁC NHẬN";
        List<Orders> p =orderService.getOrderByUser(idUser,StatusOrder.DA_XAC_NHAN);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/lay-order-dang-giao-user")
    public ResponseEntity<List<Orders>> dangGiaoUser(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
//        String status = "ĐANG GIAO";
        List<Orders> p =orderService.getOrderByUser(idUser,StatusOrder.DANG_GIAO);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/lay-order-da-giao-user")
    public ResponseEntity<List<Orders>> daGiaoUser(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
//        String status = "ĐÃ GIAO";
        List<Orders> p =orderService.getOrderByUser(idUser,StatusOrder.DA_GIAO);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/lay-order-da-nhan-user")
    public ResponseEntity<List<Orders>> daNhanUser(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
//        String status = "ĐÃ NHẬN";
        List<Orders> p =orderService.getOrderByUser(idUser,StatusOrder.DA_NHAN);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/lay-order-da-huy-user")
    public ResponseEntity<List<Orders>> daHuyUser(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
//        String status = "ĐÃ HỦY";
        List<Orders> p =orderService.getOrderByUser(idUser,StatusOrder.DA_HUY);
        return ResponseEntity.ok().body(p);
    }

    @GetMapping("/cac-don-da-giao")
    public ResponseEntity<ResponseDTO<Page<Orders>>> caDonDaGiao(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        ResponseDTO<Page<Orders>> p =orderService.getOrderByStatus(StatusOrder.DA_GIAO,currentPage,pageSize);
        return ResponseEntity.ok().body(p);
    }

    @GetMapping("/cac-don-dang-giao")
    public ResponseEntity<ResponseDTO<Page<Orders>>> cacDonDangGiao(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        ResponseDTO<Page<Orders>> p =orderService.getOrderByStatus(StatusOrder.DANG_GIAO,currentPage,pageSize);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/cac-don-da-huy")
    public ResponseEntity<ResponseDTO<Page<Orders>>> cacDonDaHuy(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        ResponseDTO<Page<Orders>> p =orderService.getOrderByStatus(StatusOrder.DA_HUY,currentPage,pageSize);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/cac-don-da-nhan")
    public ResponseEntity<ResponseDTO<Page<Orders>>> cacDonDaNhan(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        ResponseDTO<Page<Orders>> p =orderService.getOrderByStatus(StatusOrder.DA_NHAN,currentPage,pageSize);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/cac-don-dang-chuan-bi")
    public ResponseEntity<ResponseDTO<Page<Orders>>> cacDonDaDangChuanBi(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        ResponseDTO<Page<Orders>> p =orderService.getOrderByStatus(StatusOrder.DA_XAC_NHAN,currentPage,pageSize);
        return ResponseEntity.ok().body(p);
    }

    @GetMapping("/cac-don-cho-xac-nhan")
    public ResponseEntity<ResponseDTO<Page<Orders>>> cacDonChoXacNhan(
            @RequestParam(defaultValue = "0") int currentPage,
            @RequestParam(defaultValue = "10") int pageSize
    ){
        ResponseDTO<Page<Orders>> p =orderService.getOrderByStatus(StatusOrder.CHO_XAC_NHAN,currentPage,pageSize);
        return ResponseEntity.ok().body(p);
    }
}
