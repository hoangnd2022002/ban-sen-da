package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.RoleDTOReq;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.dto.request.UserDTOReq;
import com.example.duantn.dto.response.UserDTORes;
import com.example.duantn.entity.Role;
import com.example.duantn.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    public final UserService userService;
    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/get-all-user")
    public ResponseEntity<Page<UserDTORes>> getAllUser(@RequestParam(defaultValue = "0") int currentPage ){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        List<UserDTORes> userDTORes = userService.getAllUser();
        List<UserDTORes> userList = userDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<UserDTORes> pages = new PageImpl<>(userList, PageRequest.of(currentPage, pageSize), userDTORes.size());
        return ResponseEntity.ok().body(pages);
    }

    @GetMapping("/get-all-user1")
    public ResponseEntity<Page<UserDTORes>> getAllUserStatus1(@RequestParam(defaultValue = "0") int currentPage ){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        List<UserDTORes> userDTORes = userService.getAllUserStatus1();
        List<UserDTORes> userList = userDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<UserDTORes> pages = new PageImpl<>(userList, PageRequest.of(currentPage, pageSize), userDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @GetMapping("/get-all-user2")
    public ResponseEntity<Page<UserDTORes>> getAllUserStatus2(@RequestParam(defaultValue = "0") int currentPage ){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        List<UserDTORes> userDTORes = userService.getAllUserStatus2();
        List<UserDTORes> userList = userDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<UserDTORes> pages = new PageImpl<>(userList, PageRequest.of(currentPage, pageSize), userDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @GetMapping("/get-user")
    public ResponseEntity<ResponseDTO<UserDTORes>> getUserByID(Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
        return ResponseEntity.ok().body(userService.getDetailUser(idUser));
    }
    @PostMapping("/save-user")
    public ResponseEntity<ResponseDTO<UserDTORes>> saveUser(@RequestBody UserDTOReq userDTOReq)  {
        return ResponseEntity.ok(userService.addUser(userDTOReq));
    }
    @PostMapping("/delete-user")
    public void deleteUser(@RequestBody UserDTOReq userDTOReq){
        userService.deleteUser(userDTOReq);
    }

    @PostMapping("/edit-user")
    public ResponseEntity<ResponseDTO<UserDTORes>> editProduct(@RequestBody UserDTOReq userDTOReq){
        return ResponseEntity.ok(userService.editUser(userDTOReq));
    }
    @PostMapping("/get-user-by-role")
    public ResponseEntity<Page<UserDTORes>> getAllUserByIDRole(@RequestParam(defaultValue = "0") int currentPage ,@RequestBody RoleDTOReq roleDTOReq){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        List<UserDTORes> userDTOReslst  = userService.getAllUserByRole(roleDTOReq);
        List<UserDTORes> userList = userDTOReslst.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<UserDTORes> pages = new PageImpl<>(userList, PageRequest.of(currentPage, pageSize), userDTOReslst.size());
        return ResponseEntity.ok().body(pages);
    }

    @GetMapping("/get-all-role")
    public ResponseEntity<List<Role>> getAllUserByIDRole(){
        return ResponseEntity.ok().body(userService.getAllRole());
    }
    @PostMapping("/get-all-user-by-keyword")
    public ResponseEntity<Page<UserDTORes>> getAllUserByKeyword(@RequestParam(defaultValue = "0") int currentPage , @RequestBody SearchReq searchReq){
        int pageSize = 10;
        int startItem = currentPage * pageSize;
        List<UserDTORes> userDTORes = userService.getUserByName(searchReq);
        List<UserDTORes> userList = userDTORes.stream()
                .skip(startItem)
                .limit(pageSize).collect(Collectors.toList());
        Page<UserDTORes> pages = new PageImpl<>(userList, PageRequest.of(currentPage, pageSize), userDTORes.size());
        return ResponseEntity.ok().body(pages);
    }
    @PostMapping("/free/register-user")
    public ResponseEntity<ResponseDTO<UserDTORes>> register(@RequestBody UserDTOReq userDTOReq)  {
        return ResponseEntity.ok(userService.addUser(userDTOReq));
    }
    @PostMapping("/free/change-pass")
    public ResponseEntity<ResponseDTO<UserDTORes>> changePass(@RequestBody UserDTOReq userDTOReq, Principal principal){
        String userString = principal.getName();
        int idUser = Integer.parseInt(userString.substring(userString.indexOf("=") + 1, userString.indexOf(",")));
        userDTOReq.setIdUser(idUser);
        return ResponseEntity.ok(userService.changePass(userDTOReq));
    }

}
