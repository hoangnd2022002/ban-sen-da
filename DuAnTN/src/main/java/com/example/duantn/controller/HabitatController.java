package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.entity.Habitats;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.service.HabitatService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/habitat")
@RequiredArgsConstructor
public class HabitatController {

    private final HabitatService habitatService;

    @PostMapping("/add-habitat")
    public ResponseEntity<ResponseDTO<Habitats>> addHabitats(@RequestBody Habitats habitat){
        return ResponseEntity.ok().body(habitatService.addHabitats(habitat));
    }
    @PostMapping("/edit-habitat")
    public ResponseEntity<ResponseDTO<Habitats>> editHabitats(@RequestBody Habitats habitat){
        return ResponseEntity.ok().body(habitatService.editHabitats(habitat));
    }
    @PostMapping("/delete-habitat")
    public ResponseEntity<ResponseDTO<Habitats>> deleteHabitats(@RequestBody Habitats habitat){
        return ResponseEntity.ok().body(habitatService.deleteHabitats(habitat));
    }
    @GetMapping("/free/get-all-habitat1")
    public ResponseEntity<ResponseDTO<List<Habitats>>> getAllHabitats1(){
        return ResponseEntity.ok().body(habitatService.getAll1());
    }
    @GetMapping("/free/get-all-habitat2")
    public ResponseEntity<ResponseDTO<List<Habitats>>> getAllHabitats2(){
        return ResponseEntity.ok().body(habitatService.getAll2());
    }
    @PostMapping("/free/get-detail")
    public ResponseEntity<ResponseDTO<Habitats>> getHabitatsByID(@RequestBody SearchReq searchReq){
        return ResponseEntity.ok().body(habitatService.getHabitatsByIDHabitats(Integer.parseInt(searchReq.getKeyword())));
    }
    @PostMapping("/free/get-by-name")
    public ResponseEntity<ResponseDTO<Habitats>> getHabitatsByName(@RequestBody SearchReq searchReq){
        return ResponseEntity.ok().body(habitatService.getHabitatsByName(searchReq.getKeyword()));
    }
    @PostMapping("/free/get-product-detail-by-habitat")
    public ResponseEntity<ResponseDTO<List<ProductSize>>> getListProductSizeByHabitat(@RequestParam(defaultValue = "10") int pageSize,
                                                                                      @RequestParam(defaultValue = "0") int page,
                                                                                      @RequestBody SearchReq searchReq){
        if (page <0){
            return null;
        }else {
            return ResponseEntity.ok().body(habitatService.getProductSizeByHabitats(page,pageSize,Integer.parseInt(searchReq.getKeyword())));
        }
    }
    @PostMapping("/free/get-product-by-habitat")
    public ResponseEntity<ResponseDTO<List<Product>>> getProductByHabitat(@RequestParam(defaultValue = "10") int pageSize,
                                                                          @RequestParam(defaultValue = "0") int page,
                                                                          @RequestBody SearchReq searchReq){
        if (page <0){
            return null;
        }else {
            return ResponseEntity.ok().body(habitatService.getProductByHabitats(page,pageSize,Integer.parseInt(searchReq.getKeyword())));
        }
    }
}
