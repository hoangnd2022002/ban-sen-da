package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.entity.*;
import com.example.duantn.service.TypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/type")
@RequiredArgsConstructor
public class TypeController {
    private final TypeService typeService;
    @PostMapping("/add-type")
    public ResponseEntity<ResponseDTO<Type>> addType(@RequestBody Type type){
        return ResponseEntity.ok().body(typeService.addType(type));
    }
    @PostMapping("/edit-type")
    public ResponseEntity<ResponseDTO<Type>> editType(@RequestBody Type type){
        return ResponseEntity.ok().body(typeService.editType(type));
    }
    @PostMapping("/delete-type")
    public ResponseEntity<ResponseDTO<Type>> deleteType(@RequestBody Type type){
        return ResponseEntity.ok().body(typeService.deleteType(type));
    }
    @GetMapping("/free/get-all-type1")
    public ResponseEntity<ResponseDTO<List<Type>>> getAllType1(){

            return ResponseEntity.ok().body(typeService.getAll1());

    }
    @GetMapping("/free/get-all-type2")
    public ResponseEntity<ResponseDTO<List<Type>>> getAllType2(){
            return ResponseEntity.ok().body(typeService.getAll2());
    }
    @PostMapping("/free/get-detail")
    public ResponseEntity<ResponseDTO<Type>> getTypeByID(@RequestBody SearchReq searchReq){
            return ResponseEntity.ok().body(typeService.getTypeByIDType(Integer.parseInt(searchReq.getKeyword())));
    }
    @PostMapping("/free/get-by-name")
    public ResponseEntity<ResponseDTO<List<Type>>> getTypeByName( @RequestBody SearchReq searchReq){
            return ResponseEntity.ok().body(typeService.getTypeByName(searchReq.getKeyword()));

    }
    @PostMapping("/free/get-product-detail-by-type")
    public ResponseEntity<ResponseDTO<List<ProductSize>>> getListProductSizeByType(@RequestParam(defaultValue = "10") int pageSize,
                                                                                   @RequestParam(defaultValue = "0") int page,
                                                                                   @RequestBody SearchReq searchReq){
        if (page <0){
            return null;
        }else {
            return ResponseEntity.ok().body(typeService.getProductSizeByType(page, pageSize,searchReq));
        }
    }
    @PostMapping("/free/get-product-by-type")
    public ResponseEntity<ResponseDTO<List<Product>>> getProductByType(@RequestParam(defaultValue = "10") int pageSize,
                                                                       @RequestParam(defaultValue = "0") int page,
                                                                       @RequestBody SearchReq searchReq){
        if (page <0){
            return null;
        }else {
            return ResponseEntity.ok().body(typeService.getProductByType(page, pageSize,Integer.parseInt(searchReq.getKeyword())));
        }
    }

}
