package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.ThongKeDTO;
import com.example.duantn.dto.ThongKeTheoNam;
import com.example.duantn.service.ThongKeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/thong-ke")
@RequiredArgsConstructor
public class ThongKeController {

    private final ThongKeService thongKeService;

    @PostMapping("/search-theo-ngay")
    public ResponseEntity<ResponseDTO<ThongKeDTO>> ThongKeByDate(@RequestBody ThongKeDTO thongKeDTO){
        ResponseDTO<ThongKeDTO> p =thongKeService.thongKeByDate(thongKeDTO);
        return ResponseEntity.ok().body(p);
    }
    @GetMapping("/search-theo-nam")
    public ResponseEntity<ResponseDTO<List<ThongKeTheoNam>>> ThongKeTheoNam(@RequestParam(defaultValue = "2023") String year){
        ResponseDTO<List<ThongKeTheoNam>> p =thongKeService.ThongKeTheoNamService(year);
        return ResponseEntity.ok().body(p);
    }
}
