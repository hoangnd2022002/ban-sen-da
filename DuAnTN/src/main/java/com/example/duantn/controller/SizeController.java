package com.example.duantn.controller;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.Size;
import com.example.duantn.service.SizeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/size")
@RequiredArgsConstructor
public class SizeController {
    private final SizeService sizeService;
    @PostMapping("/add-size")
    public ResponseEntity<ResponseDTO<Size>> addSize(@RequestBody Size size){
        return ResponseEntity.ok().body(sizeService.addSize(size));
    }
    @PostMapping("/edit-size")
    public ResponseEntity<ResponseDTO<Size>> editSize(@RequestBody Size size){
        return ResponseEntity.ok().body(sizeService.editSize(size));
    }
    @PostMapping("/delete-size")
    public ResponseEntity<ResponseDTO<Size>> deleteSize(@RequestBody Size size){
        return ResponseEntity.ok().body(sizeService.deleteSize(size));
    }
    @GetMapping("/free/get-all-size")
    public ResponseEntity<ResponseDTO<List<Size>>> getAllSize(){
        return ResponseEntity.ok().body(sizeService.getAll());
    }
//    @GetMapping("/get-all-size-by-cate-dad/{id}")
//    public ResponseEntity<ResponseDTO<List<Size>>> getAllSizeByCateDad(@PathVariable int id){
//        return ResponseEntity.ok().body(sizeService.getByCateDad(id));
//    }
}
