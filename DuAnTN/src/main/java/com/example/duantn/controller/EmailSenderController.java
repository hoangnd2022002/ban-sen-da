package com.example.duantn.controller;

import com.example.duantn.base.MailModule;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.User;
import com.example.duantn.service.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api-email")
public class EmailSenderController {

    @Autowired
    EmailSenderService senderService;

    @PostMapping("/send-mail")
    public void sendMail(@RequestBody MailModule mailModule){
        senderService.setMail(mailModule);
        System.out.println("send success");
    }

    @PostMapping("/pass-change")
    public ResponseDTO<User> changePass(@RequestBody MailModule mailModule){
        senderService.changeMail(mailModule);
        System.out.println("send success");
        return senderService.changeMail(mailModule);
    }
}
