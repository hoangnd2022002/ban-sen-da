package com.example.duantn.controller;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.Contact;
import com.example.duantn.repository.UserRepository;
import com.example.duantn.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;


@CrossOrigin("*")
@RestController
@RequestMapping("/api/contact")
@RequiredArgsConstructor
public class ContactController {

    private final UserRepository userRepository;
    private final ContactService contactService;

    @PostMapping("/add-contact")
    public ResponseEntity<ResponseDTO<Contact>> addContact(@RequestBody Contact contact, Principal principal){
        String userString = principal.getName();
        Integer idUser = Integer.valueOf(userString.substring(userString.indexOf("=")+1,userString.indexOf(","))) ;
        contact.setUser(userRepository.findById(idUser).orElse(null));
        ResponseDTO<Contact> responseDTO=contactService.addContact(contact);
        return ResponseEntity.ok().body(responseDTO);
    }
    @PostMapping("/phan-hoi")
    public ResponseEntity<ResponseDTO<Contact>> phanHoi(@RequestBody Contact contact){
        ResponseDTO<Contact> responseDTO=contactService.phanHoi(contact);
        return ResponseEntity.ok().body(responseDTO);
    }
    @PostMapping("/tu-choi-phan-hoi")
    public ResponseEntity<ResponseDTO<Contact>> tuChoiPhanHoi(@RequestBody Contact contact){
        ResponseDTO<Contact> responseDTO=contactService.tuChoiPhanHoi(contact);
        return ResponseEntity.ok().body(responseDTO);
    }
    @GetMapping("/contact-tu-choi")
    public ResponseEntity<List<Contact>> ContactTuChoiPhanHoi(
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam(defaultValue = "0") int page){
        if (page <0){
            return null;
        }else {
            String status ="TỪ CHỐI PHẢN HỒI";
            List<Contact> responseDTO = contactService.ListPhanHoiBangStatus(page, pageSize,status);
            return ResponseEntity.ok().body(responseDTO);
        }
    }

    @GetMapping("/contact-da-phan-hoi")
    public ResponseEntity<List<Contact>> ContactDaPhanHoi(@RequestParam(defaultValue = "10") int pageSize,
                                                       @RequestParam(defaultValue = "0") int page){
        if (page <0){
            return null;
        }else {
            String status ="ĐÃ PHẢN HỒI";
            List<Contact> responseDTO = contactService.ListPhanHoiBangStatus(page, pageSize,status);
            return ResponseEntity.ok().body(responseDTO);
    }
    }

    @GetMapping("/contact-chua-phan-hoi")
    public ResponseEntity<List<Contact>> ContactChuaPhanHoi(@RequestParam(defaultValue = "10") int pageSize,
                                                       @RequestParam(defaultValue = "0") int page){
            if (page <0){
                return null;
            }else {
                String status ="CHƯA PHẢN HỒI";
                List<Contact> responseDTO = contactService.ListPhanHoiBangStatus(page, pageSize,status);
                return ResponseEntity.ok().body(responseDTO);
            }
    }
    @GetMapping("/so-luong-chua-phan-hoi")
    public ResponseEntity<?> soLuongChuaPhanHoi(){

            String status ="CHƯA PHẢN HỒI";
            int responseDTO = contactService.soLuongPhanHoi(status);
            return ResponseEntity.ok().body(responseDTO);

    }
    @GetMapping("/so-luong-da-phan-hoi")
    public ResponseEntity<?> soLuongDaPhanHoi(){

        String status ="ĐÃ PHẢN HỒI";
        int responseDTO = contactService.soLuongPhanHoi(status);
        return ResponseEntity.ok().body(responseDTO);

    }
    @GetMapping("/so-luong-tu-choi-phan-hoi")
    public ResponseEntity<?> soLuongTuChoiPhanHoi(){

        String status ="TỪ CHỐI PHẢN HỒI";
        int responseDTO = contactService.soLuongPhanHoi(status);
        return ResponseEntity.ok().body(responseDTO);

    }

}
