package com.example.duantn.controller;

import com.example.duantn.security.AuthenticationToken;
import com.example.duantn.security.dto.JwtResponse;
import com.example.duantn.security.dto.LoginRequest;
import com.example.duantn.security.jwt.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@CrossOrigin("*")
@RestController
@RequestMapping("/auth")
public class SecurityController {

    Logger log = LoggerFactory.getLogger(SecurityController.class);
    private final AuthenticationProvider authenticationProvider;
    private final TokenProvider tokenProvider;

    private final PasswordEncoder passwordEncoder;

    @PostMapping("/sign-in")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
//        AuthenticationProvider authenticationProvider = null;
        Authentication authentication = authenticationProvider.authenticate(new AuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        String accessToken = tokenProvider.createToken(authentication, false);
        tokenProvider.getAuthentication(accessToken);
//        String refreshToken = tokenProvider.createToken(authentication, true);

        return ResponseEntity.ok().body(new JwtResponse(accessToken,authentication.getAuthorities().toString()));

    }
}
