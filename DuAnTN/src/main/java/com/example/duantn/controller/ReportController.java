package com.example.duantn.controller;

import com.example.duantn.dto.HoaDonDTO;
import com.example.duantn.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Base64;

@RestController
@CrossOrigin("*")
@RequestMapping("api/report")
public class ReportController {

    @Autowired
    ReportService reportService;
    @PostMapping("/export-hoa-don")
    public ResponseEntity exportPDF(@RequestBody HoaDonDTO hoaDonDTO, Principal principal) throws Exception {
        String userString = principal.getName();
        Integer idUser = Integer.valueOf(userString.substring(userString.indexOf("=")+1,userString.indexOf(","))) ;
        hoaDonDTO.setNguoiBanHang(String.valueOf(idUser));

        byte[] byteArr = reportService.exportPDF(hoaDonDTO);
        HttpHeaders headers = new HttpHeaders();
        ContentDisposition contentDisposition = ContentDisposition.builder("attachment").filename("HoaDon").build();
        headers.setContentDisposition(contentDisposition);
        MediaType mediaType = MediaType.parseMediaType("application/pdf;charset=utf-8");
        headers.setContentType(mediaType);
        return ResponseEntity.ok().headers(headers).body(byteArr);

    }
    @PostMapping("/export-hoa-don-online")
    public ResponseEntity exportPDFOnline(@RequestBody HoaDonDTO hoaDonDTO) throws Exception {
        byte[] byteArr = reportService.exportPDFOnline(hoaDonDTO);
        HttpHeaders headers = new HttpHeaders();
        ContentDisposition contentDisposition = ContentDisposition.builder("attachment").filename("HoaDonOnline").build();
        headers.setContentDisposition(contentDisposition);
        MediaType mediaType = MediaType.parseMediaType("application/pdf;charset=utf-8");
        headers.setContentType(mediaType);
        return ResponseEntity.ok().headers(headers).body(byteArr);
    }
}
