package com.example.duantn.base;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class MailModule {
    private String email;
    private String verification;
    private String passNew;
}
