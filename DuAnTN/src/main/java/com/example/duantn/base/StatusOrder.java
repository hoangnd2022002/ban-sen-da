package com.example.duantn.base;

public class StatusOrder {
    public static final String DA_XAC_NHAN = "ĐÃ XÁC NHẬN";
    public static final String DANG_GIAO = "ĐANG GIAO";
    static public final String DA_NHAN = "ĐÃ NHẬN";
    static public final String DA_GIAO = "ĐÃ GIAO";
    static public final String DA_HUY = "ĐÃ HỦY";
    static public final String BAN_TAI_QUAY = "BÁN TẠI QUẦY";
    public static final String CHO_XAC_NHAN = "CHỜ XÁC NHẬN";
    static public final String GIO_HANG = "GIỎ HÀNG";
    static public final String HUY_TAI_QUAY = "HỦY TẠI QUẦY";
    static public final String CHO_HUY_TAI_QUAY = "CHỜ HỦY TẠI QUẦY";


    static public final String TRA_TRUOC = "TRẢ TRƯỚC";
    static public final String TRA_SAU = "TRẢ SAU";
    static public final String HOAN_TIEN = "HOÀN TIỀN";





//    static public final String HOST = "https://duantn-git-main-underman2112.vercel.app";
    static public final String HOST = "http://localhost:4200";
    static public final String HOST_SERVER = "http://localhost:8080";
//    static public final String HOST_SERVER = "https://a24d-1-52-219-44.ngrok-free.app";
    static public final String PATH_IMAGE = "src/main/resources/static/images/";

}
