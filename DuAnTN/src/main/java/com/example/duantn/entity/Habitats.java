package com.example.duantn.entity;

import lombok.*;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "habitats")
public class Habitats {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_habitats")
    private Integer idHabitats;

    @Column(name = "habitats_name")
    private String habitatsName;

    @Column(name = "status")
    private Integer status;



}
