package com.example.duantn.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Table(name = "transaction_history")
public class TransactionHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_transaction_history")
    private Integer idTransacitonHistory;

    @Column(name = "amount")
    private String amount;

    @Column(name = "bank_code")
    private String bankCode;

    @Column(name = "bank_tran_no")
    private String bankTranNo;

    @Column(name = "card_type")
    private String cardType;

    @Column(name = "orderInfo")
    private String orderInfo;

    @Column(name = "pay_date")
    private String payDate;

    @Column(name = "response_code")
    private String responseCode;

    @Column(name = "tmn_code")
    private String tmnCode;

    @Column(name = "transaction_no")
    private String transactionNo;

    @Column(name = "txn_ref")
    private String txnRef;

    @Column(name = "secure_hash")
    private String secureHash;

}
