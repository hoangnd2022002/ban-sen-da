package com.example.duantn.entity;

import lombok.*;

import javax.persistence.*;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "order_detail")
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order_detail")
    private Integer idOrderDetail;

    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_order")
    private Orders order;

    @Column(name = "price")
    private Float price;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "size_name")
    private String sizeName;

    @Column(name = "color_name")
    private String colorName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_product_size")
    private ProductSize productSize;

    @Column(name = "status")
    private String status;
}
