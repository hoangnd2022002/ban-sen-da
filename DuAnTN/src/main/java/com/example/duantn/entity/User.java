package com.example.duantn.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Integer idUser;

    @Column(name = "username")
    private String username;
    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "sex")
    private Byte sex;

    @Column(name = "phone")
    private String phone;

    @Column(name = "createdate")
    private java.sql.Timestamp createdate;

    @Column(name = "name")
    private String name;

    @Column(name = "status")
    private int status;

    @Column(name = "expiration_date")
    private java.sql.Timestamp expirationDate;

    @Column(name = "uuid")
    private String UUID;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_role")
    private Role role;


}
