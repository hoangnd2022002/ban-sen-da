package com.example.duantn.entity;

import lombok.*;

import javax.persistence.*;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_comment")
    private Integer idComment;

    @Column(name = "content")
    private String content;

    @Column(name = "createdate")
    private java.sql.Timestamp createdate;

    @Column(name = "id_user")
    private Integer idUser;

    @Column(name = "id_product")
    private Integer idProduct;


}
