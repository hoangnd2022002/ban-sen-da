package com.example.duantn.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_contact")
    private Integer idContact;

    @Column(name = "message_user")
    private String messageUser;
    @Column(name = "message_shop")
    private String messageShop;

    @Column(name = "createdate")
    private java.sql.Timestamp createdate;

    @Column(name = "responsedate")
    private java.sql.Timestamp responsedate;

    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private User user;


}
