package com.example.duantn.entity;

import lombok.*;

import javax.persistence.*;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Integer idOrder;

    @Column(name = "create_date")
    private java.sql.Timestamp createDate;

    @Column(name = "delivery_date")
    private java.sql.Timestamp deliveryDate;

    @Column(name = "total_money")
    private Float totalMoney;

    @Column(name = "status")
    private String status;

    @Column(name = "status_money")
    private String statusMoney;

    @Column(name = "description")
    private String moTa;

    @Column(name = "address")
    private String diaChi;

    @Column(name = "conversion_date")
    private java.sql.Timestamp conversionDate;

    @Column(name = "update_date")
    private java.sql.Timestamp updateDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private User user;
}
