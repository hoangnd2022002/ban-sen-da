package com.example.duantn.entity;

import lombok.*;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "light_aspect")
public class LightAspect {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_light_aspect")
    private Integer idLightAspect;

    @Column(name = "status")
    private Integer status;

    @Column(name = "light_aspect_name")
    private String lightAspectName;


}
