package com.example.duantn.service;

import com.example.duantn.conf.NumToVietnameseWordUtils;
import com.example.duantn.dto.HoaDonDTO;
import com.example.duantn.dto.OrderDetailReport;
import com.example.duantn.dto.SanPhamDTO;
import com.example.duantn.entity.OrderDetail;
import com.example.duantn.entity.Orders;
import com.example.duantn.entity.User;
import com.example.duantn.repository.OrderDetailRepository;
import com.example.duantn.repository.OrderRepository;
import com.example.duantn.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ReportService {
    private final OrderDetailRepository orderDetailRepository;
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    public byte[] exportPDF (HoaDonDTO hoaDonDTO) throws JRException {
        try {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ClassPathResource("templates/hoa_don.jrxml").getInputStream());
            Map<String, Object> parameters = new HashMap<>();
            var listItems = hoaDonDTO.getTable1();
            int i = 1;
            int tongTienThanhToan = 0;
            if (listItems != null && listItems.size() > 0) {
                for (SanPhamDTO data : listItems) {
                    data.setStt(String.valueOf(i));
                    int thanhTien =(Integer.parseInt(data.getSoLuong())*Integer.parseInt(data.getDonGia()));
                    data.setDonGia(NumToVietnameseWordUtils.convertToCommas(data.getDonGia()));
                    data.setThanhTien(NumToVietnameseWordUtils.convertToCommas(String.valueOf(thanhTien)));
                    tongTienThanhToan+=thanhTien;
                    i++;
                }
                // khởi tạo data source
                JRBeanCollectionDataSource listFeeDatasource = new JRBeanCollectionDataSource(listItems);
                parameters.put("table1", listFeeDatasource);

            }

            LocalTime currentTime = LocalTime.now();
            DateTimeFormatter thoiGian = DateTimeFormatter.ofPattern("HH:mm:ss");
            String formattedThoiGian = currentTime.format(thoiGian);
            User user = userRepository.findById(Integer.parseInt(hoaDonDTO.getNguoiBanHang())).orElse(null);
            if (!ObjectUtils.isEmpty(user)){
                parameters.put("logo", new ClassPathResource("static/images/logo.png").getInputStream());
                parameters.put("nguoiBan",user.getName()+" -"+hoaDonDTO.getNguoiBanHang());
                parameters.put("tongTienThanhToan",NumToVietnameseWordUtils.convertToCommas(String.valueOf(tongTienThanhToan)));
                parameters.put("tienKhachTra",NumToVietnameseWordUtils.convertToCommas(hoaDonDTO.getTienKhachTra()));
                parameters.put("tienTraLai",NumToVietnameseWordUtils.convertToCommas(String.valueOf(Integer.parseInt(hoaDonDTO.getTienKhachTra())-tongTienThanhToan)));
                parameters.put("thoiGian", formattedThoiGian);
                parameters.put("ngay", String.valueOf(LocalDate.now().getDayOfMonth()));
                parameters.put("thang",String.valueOf(LocalDate.now().getMonthValue()));
                parameters.put("nam",String.valueOf(LocalDate.now().getYear()));
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
                return JasperExportManager.exportReportToPdf(jasperPrint);
            }
            return null;
        }catch (JRException | IOException e) {
            throw new JRException(e.getMessage());
        }
    }
    public byte[] exportPDFOnline (HoaDonDTO hoaDonDTO) throws JRException {
        try {
            JasperReport jasperReport = JasperCompileManager.compileReport(new ClassPathResource("templates/hoa_don_online.jrxml").getInputStream());
            Map<String, Object> parameters = new HashMap<>();
            Orders orders = orderRepository.findById(Integer.parseInt(hoaDonDTO.getMaHoaDon())).orElse(null);

               List<OrderDetail> orderDetailList = orderDetailRepository.getOrderDetailByOrder(orders);
               hoaDonDTO.setTable2(OrderDetailReport.convertDTO(orderDetailList));
               var listItems = hoaDonDTO.getTable2();
               int i = 1;
               float tongTienThanhToan =orders.getTotalMoney();
               if (listItems != null && listItems.size() > 0) {
                   for (OrderDetailReport data : listItems) {
                       data.setStt(String.valueOf(i));
                       i++;
                   }
                   // khởi tạo data source
                   JRBeanCollectionDataSource listFeeDatasource = new JRBeanCollectionDataSource(listItems);
                   parameters.put("table2", listFeeDatasource);
               }
               parameters.put("logo", new ClassPathResource("static/images/logo.png").getInputStream());
               parameters.put("maHoaDon",hoaDonDTO.getMaHoaDon());
               parameters.put("hoTen",orders.getUser().getName());
               parameters.put("SDT",orders.getUser().getPhone());
               parameters.put("diaChi",orders.getDiaChi());
               parameters.put("tongTienThanhToan",NumToVietnameseWordUtils.convertToCommas(String.valueOf(tongTienThanhToan)));
               parameters.put("bangChu",NumToVietnameseWordUtils.num2String( (long) tongTienThanhToan));
               parameters.put("ngay",String.valueOf( orders.getCreateDate().toLocalDateTime().getDayOfMonth()));
               parameters.put("thang",String.valueOf( orders.getCreateDate().toLocalDateTime().getMonthValue()));
               parameters.put("nam",String.valueOf( orders.getCreateDate().toLocalDateTime().getYear()));
               JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
               return JasperExportManager.exportReportToPdf(jasperPrint);

        }catch (JRException | IOException e) {
            throw new JRException(e.getMessage());
        }

    }

}
