package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.Contact;
import com.example.duantn.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;
    private final EmailSenderService emailSenderService;

    public ResponseDTO<Contact> addContact(Contact contact) {
        ResponseDTO<Contact> responseDTO = new ResponseDTO<>();
        if (StringUtils.hasText(contact.getMessageUser())) {
            contact.setStatus("CHƯA PHẢN HỒI");
            contact.setCreatedate(Timestamp.valueOf(LocalDateTime.now()));
            contactRepository.save(contact);
            responseDTO.setData(contact);
            responseDTO.setMessage("SUCCESS CONTACT");
        } else {
            responseDTO.setMessage("FAILE CONTACT");
        }
        return responseDTO;
    }

    public ResponseDTO<Contact> phanHoi(Contact contact) {
        ResponseDTO<Contact> responseDTO = new ResponseDTO<>();
        Contact contactRepo = contactRepository.findById(contact.getIdContact()).orElse(null);
        if (contactRepo != null) {
            if (StringUtils.hasText(contact.getMessageShop())) {
                contactRepo.setResponsedate(Timestamp.valueOf(LocalDateTime.now()));
                contactRepo.setStatus("ĐÃ PHẢN HỒI");
                contactRepo.setMessageShop(contact.getMessageShop());
                contactRepository.save(contactRepo);
                emailSenderService.sendMailDaPhanHoi(contactRepo);
                responseDTO.setData(contactRepo);
                responseDTO.setMessage("SUCCESS CONTACT");
            } else {
                responseDTO.setMessage("FAILE CONTACT");
            }
        } else {
            responseDTO.setMessage("FAILE CONTACT");
        }
        return responseDTO;
    }

    public ResponseDTO<Contact> tuChoiPhanHoi(Contact contact) {
        ResponseDTO<Contact> responseDTO = new ResponseDTO<>();
        Contact contact1 = contactRepository.findById(contact.getIdContact()).orElse(null);
        if (contact1 != null) {
            contact1.setResponsedate(Timestamp.valueOf(LocalDateTime.now()));
            contact1.setStatus("TỪ CHỐI PHẢN HỒI");
            contactRepository.save(contact1);
            emailSenderService.sendMailTuChoiPhanHoi(contact1);
            responseDTO.setData(contact1);
            responseDTO.setMessage("SUCCESS CONTACT");
        }
        else {
        responseDTO.setMessage("FAILE CONTACT");
        }
        return responseDTO;
    }

    public List<Contact> ListPhanHoiBangStatus(int pageNumber, int pageSize, String status){
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        return contactRepository.getContactTheoStatus(page,status);
    }
    public Integer soLuongPhanHoi( String status){
        return contactRepository.soLuongContactTheoStatus(status);
    }

}
