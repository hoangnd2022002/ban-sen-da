package com.example.duantn.service;

import com.example.duantn.base.MailModule;
import com.example.duantn.conf.NumToVietnameseWordUtils;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.OrderDTO;
import com.example.duantn.entity.Contact;
import com.example.duantn.entity.Orders;
import com.example.duantn.entity.User;
import com.example.duantn.repository.ContactRepository;
import com.example.duantn.repository.OrderRepository;
import com.example.duantn.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmailSenderService {
    @Autowired
    private JavaMailSender mail;

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final ContactRepository contactRepository;

    public void setMail(MailModule mailModule) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        User u = userRepository.findUserByEmail(mailModule.getEmail());
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Forgot password");
        UUID uuid = UUID.randomUUID();
        String uuidAsString = uuid.toString();
        LocalDateTime date = LocalDateTime.now();
        Timestamp now = Timestamp.valueOf(date.plusMinutes(3));
        userRepository.setUUID(mailModule.getEmail(),now,uuidAsString);
        mailMessage.setText("Dear " +u.getName()+"\n"+
                "Chúng tôi nhận thấy rằng bạn đang muốn thay đổi mật khẩu, mã này có hiệu lực trong 3 phút. \n"+"Vui lòng nhập đoạn mã này vào ô xác nhận:"+uuidAsString);
        mailMessage.setTo(mailModule.getEmail());
        mail.send(mailMessage);
    }

    public void sendMailDaGiao(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Vui lòng xác nhận đơn hàng "+orders.getIdOrder()+" là đã nhận hàng");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Đơn hàng của bạn đã được giao, xin vui lòng ấn nút ĐÃ NHẬN trong phần theo dõi đơn hàng của bạn. \n"+"Cảm ơn bạn đã tin tưởng và mua hàng tại sen đá Liên Hoa của chúng tôi. \n" +"Nếu bạn có thắc mắc xin vui lòng liên hệ lại với chúng tôi");
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }
    public void sendMailDangGiao(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Đơn hàng "+orders.getIdOrder()+" đang trong quá trình vận chuyển");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Đơn hàng của bạn đã đang trong quá trình vận chuyển. \n"+"Vui lòng chuẩn bị "+
                        NumToVietnameseWordUtils.convertToCommas(String.valueOf(orders.getTotalMoney())) +
                        " VND "+"để thanh toán và nhận được những sản phẩm chất lượng từ chúng tôi." );
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }
    public void sendMailChoXacNhan(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Đơn hàng "+orders.getUser().getName()+" đang chờ xác nhận");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Chúng tôi đã nhận được yêu cầu đặt hàng của bạn và đang xử lý nhé. Bạn sẽ nhận được thông báo tiếp theo khi đơn hàng đã được xác nhận. ");
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }
    public void sendMailDaXacNhan(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Đơn hàng "+orders.getUser().getName()+" đã được xác nhận");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Chúng tôi đã xác nhận đơn hàng của bạn và chuẩn bị bàn giao cho bên vận chuyển. \n"+"Chỉ cần đợi chúng tôi một ít thời gian thôi để nhận được sản phẩm chất lượng tới từ chúng tôi");
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }
    public void huyKhiDangGiao(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Đơn hàng "+orders.getUser().getName()+" đã giao không thành công");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Chúng tôi đã rất tiếc khi thầy đơn hàng của bạn đã giao không thành công. \n"+"Bạn có thể đặt lại hàng trên website của chúng tôi");
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }

    public void huyKhiTraTruoc(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Đơn hàng "+orders.getUser().getName()+" đã giao không thành công");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Đơn hàng của bạn đã bị hủy. \n"+"Xin vui lòng liên hệ lại với cửa hàng để lấy lại tiền ");
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }

    public void huyTaiQuay(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        for ( User user : userRepository.getUserByIDRole(1) ) {
            orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
            mailMessage.setFrom("websendalienhoa@gmail.com");
            mailMessage.setSubject("Đơn hàng do "+orders.getUser().getName()+"-"+orders.getUser().getIdUser()+"tạo vừa bị hủy tại quầy");
            mailMessage.setText("Dear " +user.getName()+"\n"+
                    "Có 1 đơn hàng do "+orders.getUser().getName()+"-"+orders.getUser().getIdUser()+" đã bị hủy tại quầy . ");
            mailMessage.setTo(user.getEmail());
            mail.send(mailMessage);
        }

    }

    public void thongBaoDaHuyDonHang(Orders orders) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        orders.setUser(orderRepository.getUserByOrder(orders.getIdOrder()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Đơn hàng "+orders.getUser().getName()+" đã giao không thành công");
        mailMessage.setText("Dear " +orders.getUser().getName()+"\n"+
                "Chúng tôi đã rất tiếc khi hủy đơn hàng của bạn. \n"+"Bạn có thể đặt lại hàng trên website của chúng tôi");
        mailMessage.setTo(orders.getUser().getEmail());
        mail.send(mailMessage);
    }

    public void sendMailDaPhanHoi(Contact contact) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Thông báo của cửa hàng sen đá Liên Hoa");
        mailMessage.setText("Dear " +contact.getUser().getName()+",\n"+
                contact.getMessageShop());
        mailMessage.setTo(contact.getUser().getEmail());
        mail.send(mailMessage);
    }
    public void sendMailTuChoiPhanHoi(Contact contact) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        contact.setUser(contactRepository.getUserByContact(contact.getIdContact()));
        mailMessage.setFrom("websendalienhoa@gmail.com");
        mailMessage.setSubject("Thông báo của cửa hàng sen đá Liên Hoa");
        mailMessage.setText("Dear " +contact.getUser().getName()+",\n"+
                "Chúng tôi đã nhận được tin nhắn của bạn và xin từ chối phản hồi");
        mailMessage.setTo(contact.getUser().getEmail());
        mail.send(mailMessage);
    }

    public ResponseDTO<User> changeMail(MailModule mailModule) {
        ResponseDTO<User> responseDTO = new ResponseDTO<>();
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        User u = userRepository.findUserByEmail(mailModule.getEmail());
        if (!ObjectUtils.isEmpty(u)){
            if (u.getExpirationDate().before(now)){
                responseDTO.setMessage("Mã này đã bị vô hiệu");
            }else {
                if (u.getUUID().equals(mailModule.getVerification())){
                    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
                    u.setPassword(bCryptPasswordEncoder.encode(mailModule.getPassNew()));
                    userRepository.save(u);
                    responseDTO.setMessage("Đổi thành công");
                    responseDTO.setData(u);
                }else {
                    responseDTO.setMessage("Mã không hợp lệ");
                }
            }
        }else {
            responseDTO.setMessage("Không tìm thấy email");

        }
        return responseDTO;
    }
}
