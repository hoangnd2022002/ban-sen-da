package com.example.duantn.service;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.HabitatsProduct;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.entity.Habitats;
import com.example.duantn.repository.HabitatProductRepository;
import com.example.duantn.repository.HabitatRepository;
import com.example.duantn.repository.ProductRepository;
import com.example.duantn.repository.ProductSizeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HabitatService {

    private final HabitatRepository habitatRepository;
    private final HabitatProductRepository habitatProductRepository;
    private final ProductSizeRepository productSizeRepository;
    private final ProductRepository productRepository;

    public ResponseDTO<List<Habitats>> getAll(){
        ResponseDTO<List<Habitats>> result = new ResponseDTO<>();
        result.setData(habitatRepository.findAll());
        result.setMessage("GET_HABITATS_SUCCESS");
        return result;
    }
    public ResponseDTO<List<Habitats>> getAll1(){
        ResponseDTO<List<Habitats>> result = new ResponseDTO<>();
        result.setData(habitatRepository.getHabitatsStatus1());
        result.setMessage("GET_HABITATS_SUCCESS");
        return result;
    }
    public ResponseDTO<List<Habitats>> getAll2(){
        ResponseDTO<List<Habitats>> result = new ResponseDTO<>();
        result.setData(habitatRepository.getHabitatsStatus2());
        result.setMessage("GET_HABITATS_SUCCESS");
        return result;
    }

    public ResponseDTO<Habitats> addHabitats(Habitats habitat){
        ResponseDTO<Habitats> result = new ResponseDTO<>();
        String s= habitatRepository.checkTrung(habitat.getHabitatsName());
        if (!StringUtils.hasText(s)){
            habitat.setStatus(1);
            habitatRepository.save(habitat);
            result.setData(habitat);
            result.setMessage("ADD_HABITATS_SUCCESS");
        }else {
            result.setData(habitat);
            result.setMessage("FAIL_NAME_HABITATS");
        }
        return result;
    }
    public ResponseDTO<Habitats> editHabitats(Habitats habitat){
        ResponseDTO<Habitats> result = new ResponseDTO<>();
        String s= habitatRepository.checkTrung(habitat.getHabitatsName());
        if (!StringUtils.hasText(s)){
            habitat.setStatus(1);
            habitatRepository.save(habitat);
            result.setData(habitat);
            result.setMessage("EDIT_HABITATS_SUCCESS");
        }else {
            result.setData(habitat);
            result.setMessage("FAIL_HABITATS_COLOR");
        }
        return result;
    }
    public ResponseDTO<Habitats> deleteHabitats(Habitats habitat){
        ResponseDTO<Habitats> result = new ResponseDTO<>();
        if (habitatRepository.findById(habitat.getIdHabitats()).isPresent()){
            habitat.setStatus(2);
            habitatRepository.save(habitat);
            habitatProductRepository.updateStatus2(habitat.getIdHabitats());
            List<HabitatsProduct> habitatsProducts= habitatProductRepository.getProductByHabitatTEST(habitat.getIdHabitats());
            for ( HabitatsProduct habitatsProduct : habitatsProducts ) {
              List<HabitatsProduct> list = habitatProductRepository.getProductByH(habitatsProduct.getProduct().getIdProduct());
              if (list.size() < 1){
                  Product product = productRepository.findById(habitatsProduct.getProduct().getIdProduct()).orElse(null);
                  product.setStatus(2);
                  productSizeRepository.changeStatusByProduct(habitatsProduct.getProduct().getIdProduct());
                  productRepository.save(product);
              }
            }

//            List<ProductSize> productSizeList = productSizeRepository.findProductSizesByHabitat(habitat.getIdHabitats());
//            if (productSizeList.size()>0){
//                for ( ProductSize productSize : productSizeList ) {
//                    productSize.setStatus(2);
//                    productSizeRepository.save(productSize);
//                    if (productSizeRepository.demSoLuongProductSize(productSize.getProduct().getIdProduct()) == 0){
//                        productRepository.updateStatus(productSize.getProduct().getIdProduct());
//                    }
//                }
//                result.setData(habitat);
//                result.setMessage("DELETE_HABITATS_SUCCESS");
//            }
            result.setData(habitat);
            result.setMessage("DELETE_HABITATS_SUCCESS");
        }
        return result;
    }

    public ResponseDTO<List<Product>> getProductByHabitats(int pageNumber, int pageSize,int id){
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        ResponseDTO<List<Product>> result = new ResponseDTO<>();
        result.setData(habitatRepository.getProductByHabitats(page,id));
        result.setMessage("GET_PRODUCT_SUCCESS");
        return result;
    }
    public ResponseDTO<List<ProductSize>> getProductSizeByHabitats(int pageNumber, int pageSize,int id){
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        ResponseDTO<List<ProductSize>> result = new ResponseDTO<>();
        List<ProductSize> productSizeList = new ArrayList<>();
        for ( ProductSize productSize : productSizeRepository.findProductSizesByHabitatPage(page,id)) {
            productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
            productSizeList.add(productSize);
        }
        result.setData(productSizeList);
        result.setMessage("GET_PRODUCT_SIZE_SUCCESS");
        return result;
    }
    public ResponseDTO<Habitats> getHabitatsByIDHabitats(int id){
        ResponseDTO<Habitats> result = new ResponseDTO<>();
        Habitats habitat = habitatRepository.findById(id).orElse(null);
        result.setData(habitat);
        result.setMessage("GET_HABITATS_SUCCESS");
        return result;
    }
    public ResponseDTO<Habitats> getHabitatsByName(String name){
        ResponseDTO<Habitats> result = new ResponseDTO<>();
        Habitats habitat = habitatRepository.searchHabitats(name);
        result.setData(habitat);
        result.setMessage("GET_HABITATS_SUCCESS");
        return result;
    }
}
