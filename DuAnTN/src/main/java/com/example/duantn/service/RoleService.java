package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.RoleDTOReq;
import com.example.duantn.dto.request.UserDTOReq;
import com.example.duantn.dto.response.UserDTORes;
import com.example.duantn.entity.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRole();

}
