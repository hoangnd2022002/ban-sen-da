package com.example.duantn.service;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.LightAspect;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.entity.Type;
import com.example.duantn.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LightAspectService {
    private final ColorRepository colorRepository; 
    private final ProductSizeRepository productSizeRepository;
    private final ProductRepository productRepository;
    private final TypeRepository typeRepository;
    private final LightAspectRepository lightAspectRepository;

    public ResponseDTO<List<LightAspect>> getAll(){
        ResponseDTO<List<LightAspect>> result = new ResponseDTO<>();
        result.setData(lightAspectRepository.findAll());
        result.setMessage("GET_LIGHT_ASPECT_SUCCESS");
        return result;
    }
    public ResponseDTO<List<LightAspect>> getAll1(){
        ResponseDTO<List<LightAspect>> result = new ResponseDTO<>();
        result.setData(lightAspectRepository.getLightAspectStatus1());
        result.setMessage("GET_LIGHT_ASPECT_SUCCESS");
        return result;
    }
    public ResponseDTO<List<LightAspect>> getAll2(){
        ResponseDTO<List<LightAspect>> result = new ResponseDTO<>();
        result.setData(lightAspectRepository.getLightAspectStatus2());
        result.setMessage("GET_LIGHT_ASPECT_SUCCESS");
        return result;
    }

    public ResponseDTO<LightAspect> addLightAspect(LightAspect lightAspect){
        ResponseDTO<LightAspect> result = new ResponseDTO<>();
        String s= lightAspectRepository.checkTrung(lightAspect.getLightAspectName());
        if (!StringUtils.hasText(s)){
            lightAspect.setStatus(1);
            lightAspectRepository.save(lightAspect);
            result.setData(lightAspect);
            result.setMessage("ADD_TYPE_SUCCESS");
        }else {
            result.setData(lightAspect);
            result.setMessage("FAIL_NAME_TYPE");
        }
        return result;
    }
    public ResponseDTO<LightAspect> editLightAspect(LightAspect lightAspect){
        ResponseDTO<LightAspect> result = new ResponseDTO<>();
        String s= lightAspectRepository.checkTrung(lightAspect.getLightAspectName());
        if (!StringUtils.hasText(s)){
            lightAspect.setStatus(1);
            lightAspectRepository.save(lightAspect);
            result.setData(lightAspect);
            result.setMessage("EDIT_LIGHT_ASPECT_SUCCESS");
        }else {
            result.setData(lightAspect);
            result.setMessage("FAIL_LIGHT_ASPECT_NAME");
        }
        return result;
    }
    public ResponseDTO<LightAspect> deleteLightAspect(LightAspect lightAspect){
        ResponseDTO<LightAspect> result = new ResponseDTO<>();
        if (lightAspectRepository.findById(lightAspect.getIdLightAspect()).isPresent()){
            lightAspect.setStatus(2);
            lightAspectRepository.save(lightAspect);
            List<ProductSize> productSizeList = productSizeRepository.findProductSizesByLightAspect(lightAspect.getIdLightAspect());
            if (productSizeList.size()>0){
                for ( ProductSize productSize : productSizeList ) {
                    productSize.setStatus(2);
                    productSizeRepository.save(productSize);
                    if (productSizeRepository.demSoLuongProductSize(productSize.getProduct().getIdProduct()) == 0){
                        productRepository.updateStatus(productSize.getProduct().getIdProduct());
                    }
                }
            result.setData(lightAspect);
            result.setMessage("DELETE_LIGHT_ASPECT_SUCCESS");
            }
            result.setData(lightAspect);
            result.setMessage("DELETE_LIGHT_ASPECT_SUCCESS");
        }
        return result;
    }

    public ResponseDTO<List<Product>> getProductByLightAspect(int pageNumber, int pageSize,int id){
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        ResponseDTO<List<Product>> result = new ResponseDTO<>();
        result.setData(lightAspectRepository.getProductByLightAspect(page,id));
        result.setMessage("GET_PRODUCT_SUCCESS");
        return result;
    }
    public ResponseDTO<List<ProductSize>> getProductSizeByLightAspect(int pageNumber, int pageSize,int id){
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        ResponseDTO<List<ProductSize>> result = new ResponseDTO<>();
        List<ProductSize> productSizeList = new ArrayList<>();
        for ( ProductSize productSize : productSizeRepository.findProductSizesByLightAspectPage(page,id)) {
            productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
            productSizeList.add(productSize);
        }
        result.setData(productSizeList);
        result.setMessage("GET_PRODUCT_SIZE_SUCCESS");
        return result;
    }
    public ResponseDTO<LightAspect> getLightAspectByIDLightAspect(int id){
        ResponseDTO<LightAspect> result = new ResponseDTO<>();
        LightAspect lightAspect = lightAspectRepository.findById(id).orElse(null);
        result.setData(lightAspect);
        result.setMessage("GET_LIGHT_ASPECT_SUCCESS");
        return result;
    }
    public ResponseDTO<List<LightAspect>> getLightAspectByName(String name){
        ResponseDTO<List<LightAspect>> result = new ResponseDTO<>();
        List<LightAspect> lightAspect = lightAspectRepository.searchLightAspect(name);
        result.setData(lightAspect);
        result.setMessage("GET_LIGHT_ASPECT_SUCCESS");
        return result;
    }
}
