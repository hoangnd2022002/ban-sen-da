package com.example.duantn.service.impl;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.OrderDTO;
import com.example.duantn.entity.*;
import com.example.duantn.repository.*;
import com.example.duantn.service.EmailSenderService;
import com.example.duantn.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    private JavaMailSender mail;
    @Override
    public List<OrderDetail> getOrderDetail(Orders orders) {
        return orderDetailRepository.getOrderDetailByOrder(orders);
    }

    @Override
    public List<Orders> getOrderByUser(int id,String status) {
        List<Orders> lstOrder = new ArrayList<>();
        lstOrder = orderRepository.getOrdersByUser(id,status);
//        if (status.equals(StatusOrder.CHO_XAC_NHAN)){
//             lstOrder = orderRepository.getOrdersByUser(id,StatusOrder.CHO_XAC_NHAN);
//        }else if(status.equals(StatusOrder.DA_XAC_NHAN)){
//             lstOrder = orderRepository.getOrdersByUser(id,StatusOrder.DA_XAC_NHAN);
//        }else if(status.equals("ĐÃ GIAO")){
//             lstOrder = orderRepository.getOrdersByUser(id,StatusOrder.DA_GIAO);
//        }else if(status.equals("ĐÃ HỦY")){
//             lstOrder = orderRepository.getOrdersByUser(id,StatusOrder.DA_HUY);
//        }else if(status.equals("ĐÃ NHẬN")){
//             lstOrder = orderRepository.getOrdersByUser(id,StatusOrder.DA_NHAN);
//        }else if(status.equals("ĐANG GIAO")){
//             lstOrder = orderRepository.getOrdersByUser(id,StatusOrder.DANG_GIAO);
//        }
        return lstOrder;
    }



    @Override
    public Orders getOrder(Integer idOrder) {
        return orderRepository.getOrdersByIdOrder(idOrder);
    }

    private final OrderDetailRepository orderDetailRepository;
    private final ProductSizeRepository productSizeRepository;
    private final UserRepository userRepository;
    private final EmailSenderService emailSenderService;
    private final TransactionHistoryRepository transactionHistoryRepository;

    @Override
    public ResponseDTO<OrderDTO> xacNhanTaiQuayDonHang(OrderDTO orderDTO) {
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();

        if (!ObjectUtils.isEmpty(orderDTO)||!ObjectUtils.isEmpty(orderDTO.getOrderDetailsList())){
            Orders orders = new Orders();               //Tạo giỏ hàng mới
            orders.setStatus(StatusOrder.BAN_TAI_QUAY);
            orders.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            orders.setCreateDate(Timestamp.valueOf(LocalDateTime.now()));
            orders.setUser(userRepository.findById(orderDTO.getIdUser()).orElse(null));
            float tongTien = 0;
            for ( OrderDetail orderDetail :orderDTO.getOrderDetailsList()) {
                float gia = 0;
                int soLuong = 0;
                gia = productSizeRepository.getPriceByID(orderDetail.getProductSize().getIdProductSize());
                soLuong = orderDetail.getQuantity();
                tongTien += (soLuong*gia);
            }
            orders.setTotalMoney(tongTien);
            Orders orderHasSave= orderRepository.save(orders);
            List<OrderDetail> orderDetails = orderDTO.getOrderDetailsList();
            List<OrderDetail> listIntDB = new ArrayList<>();
            int quaSoLuong=0;
            for ( OrderDetail orderDetail : orderDetails ) {
                listIntDB.add(orderDetail);
                int soLuong = productSizeRepository.getQuantityByID(orderDetail.getProductSize().getIdProductSize());
                if (soLuong<orderDetail.getQuantity()){
                    quaSoLuong++;
                }
            }
            if (quaSoLuong == 0){
                for ( OrderDetail orderDetail : orderDetails ) {
                    listIntDB.add(orderDetail);
                    int soLuong = productSizeRepository.getQuantityByID(orderDetail.getProductSize().getIdProductSize());
                    productSizeRepository.updateQuantity((soLuong-orderDetail.getQuantity()),orderDetail.getProductSize().getIdProductSize());
                    orderDetail.setOrder(orderHasSave);
                    orderDetail.setSizeName(orderDetail.getProductSize().getSize().getSizeName());
                    orderDetail.setColorName(orderDetail.getProductSize().getColor().getColorName());
                    orderDetail.setProductName(orderDetail.getProductSize().getProduct().getProductName());
                    orderDetail.setPrice(orderDetail.getProductSize().getPrice());
                }
                orderDetailRepository.saveAll(listIntDB);
                orderDTO.setTotalMoney(tongTien);
                orderDTOResponseDTO.setMessage("Bán tại quầy thành công");
                orderDTO.setTotalMoney(tongTien);
                orderDTOResponseDTO.setData(orderDTO);
            }else {
                orderDTOResponseDTO.setMessage("Bán tại quầy thất bại");
            }
        }else {
            orderDTOResponseDTO.setMessage("Bán tại quầy thất bại");
        }
        return orderDTOResponseDTO;
    }

    @Override
    public ResponseDTO<OrderDTO> xacNhanDangChuanBiDonHang(OrderDTO orderDTO) {
        Orders orders = orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        if (orders.getStatus().equals(StatusOrder.CHO_XAC_NHAN)){
            List<OrderDetail> orderDetailList = orderDetailRepository.listOdByO(orderDTO.getIdOrder());     //số lượng ở req
            boolean soLuongBoolean = true;
            for ( OrderDetail orderDetail : orderDetailList ) {
                int soLuong = productSizeRepository.getQuantityByID(orderDetail.getProductSize().getIdProductSize());
                ProductSize productSize = productSizeRepository.findById(orderDetail.getProductSize().getIdProductSize()).orElse(null);
                if (productSize.getQuantity()<orderDetail.getQuantity()){
                    soLuongBoolean=false;
                }
            }
            if (soLuongBoolean == true){
            for ( OrderDetail orderDetail : orderDetailList ) {
                int soLuong = productSizeRepository.getQuantityByID(orderDetail.getProductSize().getIdProductSize());
                productSizeRepository.updateQuantity((soLuong-orderDetail.getQuantity()),orderDetail.getProductSize().getIdProductSize());
            }
            emailSenderService.sendMailDaXacNhan(orders);
            orders.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            orders.setStatus(StatusOrder.DA_XAC_NHAN);
            orderRepository.save(orders);
            orderDTOResponseDTO.setMessage("Đổi thành công");
            }else {
                orderDTOResponseDTO.setMessage("Hết hàng");
            }
        }else {
            orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
        }
        return orderDTOResponseDTO;
    }

    @Override
    public ResponseDTO<OrderDTO> choXacNhan(OrderDTO orderDTO)  {
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        List<Integer> integerList = new ArrayList<>();
        List<Integer> soLuongSanPham = new ArrayList<>();
        for ( OrderDetail orderDetail : orderDTO.getOrderDetailsList() ) {
            ProductSize o = new ProductSize();
            o = productSizeRepository.findById(orderDetail.getProductSize().getIdProductSize()).orElse(null);
            integerList.add(o.getStatus());
        }
        boolean coChuaStatus2 = integerList.contains(2);
        if (!coChuaStatus2){
            if (!ObjectUtils.isEmpty(orderDTO)||!ObjectUtils.isEmpty(orderDTO.getOrderDetailsList())){
                List<OrderDetail> od_db = orderDetailRepository.listOdByO(orderDTO.getIdOrder());
                List<OrderDetail> od_req = orderDTO.getOrderDetailsList();
                float tongTien = 0;
                for ( OrderDetail orderDetail :od_req) {
                    float gia = 0;
                    int soLuong = 0;
                    gia = orderDetail.getProductSize().getPrice();
                    soLuong = orderDetailRepository.getsoLuong(orderDetail.getIdOrderDetail());
                    tongTien += (soLuong*gia);
                }
                orderDTO.setTotalMoney(tongTien);

                List<Integer> listIntDB = new ArrayList<>();
                List<Integer> listIntReq = new ArrayList<>();
                for ( OrderDetail orderDetail : od_db ) {
                    listIntDB.add(orderDetail.getIdOrderDetail());          //get ID DB
                }
                for ( OrderDetail orderDetail2 : od_req ) {
                    listIntReq.add(orderDetail2.getIdOrderDetail());        //get ID req
                }

                listIntDB.removeAll(listIntReq);                 // lấy ra id bản ghi OD k có trong req
                if (listIntDB.size()>0){
                    Orders orders = new Orders();               //Tạo giỏ hàng mới
                    orders.setUser(orderRepository.getUserFromOrder(orderDTO.getIdOrder()));
                    orders.setStatus(StatusOrder.GIO_HANG);
                    orders.setCreateDate(Timestamp.valueOf(LocalDateTime.now()));
                    Orders orderHasSave= orderRepository.save(orders);
                    for (Integer integer : listIntDB) {
                        OrderDetail o = orderDetailRepository.findById(integer).orElse(null);
                        o.setOrder(orderHasSave);
                        o.setPrice(o.getProductSize().getPrice());
                        orderDetailRepository.save(o);
                    }
                }
                for ( OrderDetail orderDetail : od_req ) {
                    orderDetail.setStatus("1");
                    orderDetail.setPrice(orderDetailRepository.getPriceVer2(orderDetail.getIdOrderDetail()));
                    orderDetail.setColorName(orderDetail.getProductSize().getColor().getColorName());
                    orderDetail.setProductName(orderDetail.getProductSize().getProduct().getProductName());
                    orderDetail.setSizeName(orderDetail.getProductSize().getSize().getSizeName());
                    orderDetail.setProductSize(orderDetailRepository.getProductSizeByByIdOrderDetail(orderDetail.getIdOrderDetail()));
                    orderDetail.setOrder(orderRepository.findById(orderDTO.getIdOrder()).orElse(null));
                    orderDetailRepository.save(orderDetail);
                }
                orderDTO.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
                orderDTO.setCreateDate(Timestamp.valueOf(LocalDateTime.now()));
                orderDTO.setStatusMoney(StatusOrder.TRA_SAU);
                orderDTO.setIdUser(orderRepository.getUserFromOrder(orderDTO.getIdOrder()).getIdUser());
                orderDTOResponseDTO.setMessage("Đổi thành công");
                orderDTOResponseDTO.setData(orderDTO);
                orderDTO.setStatus(StatusOrder.CHO_XAC_NHAN);
                Orders orders1= new Orders();
                orders1.setIdOrder(orderDTO.getIdOrder());
                orderRepository.save(orderDTO.convertToEntity(orderDTO));
                emailSenderService.sendMailChoXacNhan(orderDTO.convertToEntity(orderDTO));
            }else {
                orderDTOResponseDTO.setMessage("Đổi trạng thái đơn hàng thất bại");
                orderDTOResponseDTO.setData(orderDTO);
            }
        }else {
            orderDTOResponseDTO.setMessage("Đổi trạng thái đơn hàng thất bại");
        }
        return orderDTOResponseDTO;
    }
    @Override
    public ResponseDTO<OrderDTO> choXacNhanVNPay(OrderDTO orderDTO) {
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
            List<TransactionHistory> transactionHistoryList = transactionHistoryRepository.findAllStatus00();
            int orderIDStringCount = String.valueOf(orderDTO.getIdOrder()).length();
            int i =0;
        for ( TransactionHistory transactionHistory : transactionHistoryList ) {
            int lastTwoDigits = Integer.parseInt(transactionHistory.getTxnRef().substring(transactionHistory.getTxnRef().length() - orderIDStringCount));  //lấy các số cuối
           if (lastTwoDigits==orderDTO.getIdOrder()){
               i++;
           }
        }
        if (i>0){
            if (!ObjectUtils.isEmpty(orderDTO)||!ObjectUtils.isEmpty(orderDTO.getOrderDetailsList())){
                List<OrderDetail> od_db = orderDetailRepository.listOdByO(orderDTO.getIdOrder());
                List<OrderDetail> od_req = orderDTO.getOrderDetailsList();
                float tongTien = 0;
                for ( OrderDetail orderDetail :od_req) {
                    int idProductSize = orderDetailRepository.getidProductSizeByByIdOrderDetail(orderDetail.getIdOrderDetail());
                    float gia = 0;
                    int soLuong = 0;
                    gia = productSizeRepository.getPriceByID(idProductSize);
                    soLuong = orderDetailRepository.getsoLuong(orderDetail.getIdOrderDetail());
                    tongTien += (soLuong*gia);
                }
                if (tongTien>=10000){
                    orderDTO.setTotalMoney(tongTien);



                    List<Integer> listIntDB = new ArrayList<>();
                    List<Integer> listIntReq = new ArrayList<>();
                    for ( OrderDetail orderDetail : od_db ) {
                        listIntDB.add(orderDetail.getIdOrderDetail());          //get ID DB
                    }
                    for ( OrderDetail orderDetail2 : od_req ) {
                        listIntReq.add(orderDetail2.getIdOrderDetail());        //get ID req
                    }

                    listIntDB.removeAll(listIntReq);                 // lấy ra id bản ghi OD k có trong req
                    if (listIntDB.size()>0){
                        Orders orders = new Orders();               //Tạo giỏ hàng mới
                        orders.setUser(orderRepository.getUserFromOrder(orderDTO.getIdOrder()));
                        orders.setStatus(StatusOrder.GIO_HANG);
                        orders.setCreateDate(Timestamp.valueOf(LocalDateTime.now()));
                        Orders orderHasSave= orderRepository.save(orders);
                        for (Integer integer : listIntDB) {
                            OrderDetail o = orderDetailRepository.findById(integer).orElse(null);
                            o.setOrder(orderHasSave);
                            o.setPrice(o.getProductSize().getPrice());
                            orderDetailRepository.save(o);
                        }
                    }
                    for ( OrderDetail orderDetail : od_req ) {
                        orderDetail.setStatus("1");
                        orderDetail.setProductSize(orderDetailRepository.getProductSizeByByIdOrderDetail(orderDetail.getIdOrderDetail()));
                        orderDetail.setPrice(orderDetail.getProductSize().getPrice());
                        orderDetail.setProductName(orderDetail.getProductSize().getProduct().getProductName());
                        orderDetail.setColorName(orderDetail.getProductSize().getColor().getColorName());
                        orderDetail.setSizeName(orderDetail.getProductSize().getSize().getSizeName());
                        orderDetail.setOrder(orderRepository.findById(orderDTO.getIdOrder()).orElse(null));
                        orderDetailRepository.save(orderDetail);
                    }
                    orderDTO.setCreateDate(Timestamp.valueOf(LocalDateTime.now()));
                    orderDTO.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
                    orderDTO.setStatusMoney(StatusOrder.TRA_TRUOC);
                    orderDTO.setIdUser(orderRepository.getUserFromOrder(orderDTO.getIdOrder()).getIdUser());
                    orderDTOResponseDTO.setMessage("Đổi thành công");
                    orderDTOResponseDTO.setData(orderDTO);
                    orderDTO.setStatus(StatusOrder.CHO_XAC_NHAN);
                    Orders orders1= new Orders();
                    orders1.setIdOrder(orderDTO.getIdOrder());
                    orderRepository.save(orderDTO.convertToEntity(orderDTO));
                    emailSenderService.sendMailChoXacNhan(orderDTO.convertToEntity(orderDTO));
                }
                else {
                    orderDTOResponseDTO.setMessage("MONEY_MUST_NOT_BE_LESS_THAN_10000");
                    orderDTOResponseDTO.setData(orderDTO);
                }
            }
            else {
                orderDTOResponseDTO.setMessage("ORDER_IS_NULL");
                orderDTOResponseDTO.setData(orderDTO);
            }
        }
        else {
            orderDTOResponseDTO.setMessage("PAYMENT FAILED");
            orderDTOResponseDTO.setData(orderDTO);
        }
        return orderDTOResponseDTO;
    }

    @Override
    public ResponseDTO<OrderDTO> xacNhanDangGiaoDonHang(OrderDTO orderDTO) {
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
            Orders order=orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        if (order!=null){
        if (order.getStatus().equals(StatusOrder.DA_XAC_NHAN)){
            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            order.setStatus(StatusOrder.DANG_GIAO);
            orderRepository.save(order);
                emailSenderService.sendMailDangGiao(order);
                orderDTOResponseDTO.setMessage("Đổi thành công");
                orderDTOResponseDTO.setData(orderDTO);
                return orderDTOResponseDTO;
            }else {
                orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
                orderDTOResponseDTO.setData(orderDTO);
            }
        }
        orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
        orderDTOResponseDTO.setData(orderDTO);

        return orderDTOResponseDTO;
    }

    @Override
    public ResponseDTO<OrderDTO> xacNhanDaGiaoDonHang(OrderDTO orderDTO) {
        Orders order = orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        LocalDateTime now = LocalDateTime.now(); // Lấy thời gian hiện tại
        LocalDateTime later = now.plusDays(3); // Cộng thêm 3 ngày
        if (order!=null){
            order.setDeliveryDate(Timestamp.valueOf(LocalDateTime.now()));
            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            order.setConversionDate(Timestamp.valueOf(later));
            order.setStatus(StatusOrder.DA_GIAO);
            orderRepository.save(order);
            emailSenderService.sendMailDaGiao(order);
            orderDTOResponseDTO.setMessage("Đổi thành công");
            orderDTOResponseDTO.setData(orderDTO);
        }else {
            orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
            orderDTOResponseDTO.setData(orderDTO);
        }
        return orderDTOResponseDTO;
    }

    @Override
    public ResponseDTO<OrderDTO> xacNhanDaNhanDonHang(OrderDTO orderDTO) {
        Orders order = orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        if (order!=null){
            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            order.setStatus(StatusOrder.DA_NHAN);
            orderRepository.save(order);
            orderDTOResponseDTO.setMessage("Đổi thành công");
            orderDTOResponseDTO.setData(orderDTO);
            return orderDTOResponseDTO;
        }else {
            orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
            orderDTOResponseDTO.setData(orderDTO);
        }
        return orderDTOResponseDTO;

    }

    @Override
    public ResponseDTO<OrderDTO> xacNhanHuyDonHang(OrderDTO orderDTO) {
        Orders order = orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        if (order!=null&&order.getStatus().equals(StatusOrder.CHO_XAC_NHAN)&&order.getStatusMoney().equals(StatusOrder.TRA_TRUOC)){
            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            order.setStatus(StatusOrder.DA_HUY);
            order.setStatusMoney(StatusOrder.HOAN_TIEN);
            orderRepository.save(order);
            emailSenderService.huyKhiTraTruoc(order);
            orderDTOResponseDTO.setMessage("Đổi thành công");
            orderDTOResponseDTO.setData(orderDTO);
        } else if (order!=null&&order.getStatus().equals(StatusOrder.CHO_XAC_NHAN)&&order.getStatusMoney().equals(StatusOrder.TRA_SAU)) {
            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            order.setStatus(StatusOrder.DA_HUY);
            order.setStatusMoney(StatusOrder.TRA_SAU);
            orderRepository.save(order);
            emailSenderService.thongBaoDaHuyDonHang(order);
            orderDTOResponseDTO.setMessage("Đổi thành công");
            orderDTOResponseDTO.setData(orderDTO);
        }else {
            orderDTOResponseDTO.setMessage("Lỗi không xác định");
        }
//        if (order!=null&&order.getStatus().equals("ĐANG GIAO")){
//            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
//            order.setStatus(StatusOrder.DA_HUY);
//            orderRepository.save(order);
//            emailSenderService.huyKhiDangGiao(order);
//            orderDTOResponseDTO.setMessage("Đổi thành công");
//            orderDTOResponseDTO.setData(orderDTO);
//            return orderDTOResponseDTO;
//        }else {
//            orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
//            orderDTOResponseDTO.setData(orderDTO);
//        }
        return orderDTOResponseDTO;

    }
    @Override
    public ResponseDTO<OrderDTO> huyKhiDangGiao(OrderDTO orderDTO) {

        Orders order = orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        if (order!=null){
            List<OrderDetail> orderDetailList = orderDetailRepository.listOdByO(order.getIdOrder());
            for ( OrderDetail orderDetail : orderDetailList) {
                productSizeRepository.updateQuantityTraHang(orderDetail.getQuantity(),orderDetail.getProductSize().getIdProductSize());
            }
            order.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            if (order.getStatusMoney().equals("TRẢ TRƯỚC")){
                order.setStatusMoney("HOÀN TIỀN");
            }
            order.setStatus(StatusOrder.DA_HUY);
            orderRepository.save(order);
            orderDTOResponseDTO.setMessage("Đổi thành công");
            orderDTOResponseDTO.setData(orderDTO);
            emailSenderService.huyKhiDangGiao(order);
            return orderDTOResponseDTO;
        }
        else {
            orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
            orderDTOResponseDTO.setData(orderDTO);
        }
        return orderDTOResponseDTO;

    }




    @Override
    public ResponseDTO<OrderDTO> capNhatSanPham(OrderDTO orderDTO) {
        orderRepository.updateOrderStatus(StatusOrder.BAN_TAI_QUAY,orderDTO.getIdOrder());
        Orders order = orderRepository.findById(orderDTO.getIdOrder()).orElse(null);
        if (order!=null){
        }
        ResponseDTO<OrderDTO> orderDTOResponseDTO = new ResponseDTO<>();
        orderDTOResponseDTO.setMessage("Đổi thành công");
        orderDTOResponseDTO.setData(orderDTO);
        return orderDTOResponseDTO;
    }

    @Override
    public ResponseEntity themVaoGio(ProductSize productSize, User user) {
        Orders order = this.orderRepository.getByStatusAndUser(StatusOrder.GIO_HANG,user);
        if(order == null ){
            order = orderRepository.save(Orders.builder().user(user).status(StatusOrder.GIO_HANG).build());
        }
        OrderDetail exists =
                orderDetailRepository.getOrderDetailByProductSizeAndOrder(productSize,order);
        if(!(exists == null)) {
            boolean b = (exists.getQuantity() + productSize.getQuantity()) > exists.getProductSize().getQuantity();
            if((exists.getQuantity()+productSize.getQuantity()) > exists.getProductSize().getQuantity()) return ResponseEntity.badRequest().build();
            exists.setQuantity(exists.getQuantity()+productSize.getQuantity());
            orderDetailRepository.save(exists);
            return ResponseEntity.ok().build();
        }
        orderDetailRepository
                .save(OrderDetail.builder()
                                .order(order)
                                .productSize(productSize)
                                .quantity(productSize.getQuantity())
                                .status("1")
                                .build());
        return ResponseEntity.ok().build();
    }

    @Override
    public List<OrderDetail> getOrderDetail(User user) {
        Orders order = orderRepository.getByStatusAndUser(StatusOrder.GIO_HANG,user);
        List<OrderDetail> orderDetails = orderDetailRepository.getOrderDetailByOrder(order);

        return orderDetails;
    }



    @Override
    public OrderDetail getOrderDetailById(Integer id) {
        return  orderDetailRepository.findById(id).get();
    }

    @Override
    public void deleteOrderDetail( OrderDetail orderDetail) {
        orderDetailRepository.delete(orderDetail);
    }

    @Override
    public void updateQuanTity(OrderDetail orderDetail) {
//        orderDetailRepository.updateQuantity(orderDetail.getQuantity(),orderDetail.getIdOrderDetail());
        orderDetailRepository.save(orderDetail);
    }

    @Override
    public Integer soLuongTheoTrangThai(String trangThai) {
        return orderRepository.soLuongTheoTrangThai(trangThai);
    }

    @Override
    public Integer soLuongTheoGioHangUser(int id) {
        return orderDetailRepository.getsoLuongTheoUser(id);
    }


    @Override
    public ResponseDTO<Page<Orders>> getOrderByStatus(String status, int pageNumber , int pageSize) {
        Pageable page =
                PageRequest.of(pageNumber, pageSize, Sort.by("idOrder").ascending());
        Page<Orders> orders = orderRepository.getOrderByStatus(status,page);
//        if (orders!=null){
//            OrderDTO o = OrderDTO.fromDTO(order);
//        }
        ResponseDTO<Page<Orders>> orderDTOResponseDTO = new ResponseDTO<>();
        orderDTOResponseDTO.setMessage("Thành công");
        orderDTOResponseDTO.setData(orders);
        return orderDTOResponseDTO;
    }
    @Override
    public ResponseDTO<Orders> xacNhanHuyTaiQuay(Orders order, int idUser) {
        ResponseDTO<Orders> orderDTOResponseDTO = new ResponseDTO<>();

        Orders ordersInDB = orderRepository.findById(order.getIdOrder()).orElse(null);

        if (!ObjectUtils.isEmpty(ordersInDB)) {
            if (ordersInDB.getUser().getIdUser() != idUser) {
                orderDTOResponseDTO.setMessage("ERROR-USER");
                orderDTOResponseDTO.setData(ordersInDB);
            } else {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(ordersInDB.getUpdateDate().getTime());
                cal.add(Calendar.MINUTE, 30);
                Timestamp newTimestamp = new Timestamp(cal.getTimeInMillis());
                if (Timestamp.valueOf(LocalDateTime.now()).before(newTimestamp)) {
                    List<OrderDetail> orderDetailList = orderDetailRepository.listOdByO(order.getIdOrder());
                    for (OrderDetail orderDetail : orderDetailList) {
                        productSizeRepository.updateQuantityTraHang(orderDetail.getQuantity(), orderDetail.getProductSize().getIdProductSize());
                    }
                    ordersInDB.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
                    ordersInDB.setStatus(StatusOrder.HUY_TAI_QUAY);
                    ordersInDB.setMoTa(order.getMoTa());
                    orderRepository.save(ordersInDB);
                    emailSenderService.huyTaiQuay(ordersInDB);
                    orderDTOResponseDTO.setMessage("Đổi thành công");
                    orderDTOResponseDTO.setData(ordersInDB);
                    return orderDTOResponseDTO;
                } else {
                    orderDTOResponseDTO.setMessage("Hết thời gian để hủy đơn hàng");
                }
            }
        } else {
            orderDTOResponseDTO.setMessage("Đổi trạng thái thất bại");
            orderDTOResponseDTO.setData(ordersInDB);
        }

        return orderDTOResponseDTO;
    }
    @Override
    public ResponseDTO<Orders> huyBoHuyTaiQuay(Orders order) {
        Orders orders= orderRepository.findById(order.getIdOrder()).orElse(null);
        if (!ObjectUtils.isEmpty(orders)){
            orders.setUpdateDate(Timestamp.valueOf(LocalDateTime.now()));
            orders.setStatus(StatusOrder.BAN_TAI_QUAY);
            orderRepository.save(orders);
        }
        return null;
    }
    @Override
    public ResponseDTO<Orders> choXacNhanHuyTaiQuay(Orders order) {
        ResponseDTO<Orders> result = new ResponseDTO<>();
        Orders orders= orderRepository.findById(order.getIdOrder()).orElse(null);
        if (!ObjectUtils.isEmpty(orders)){
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(orders.getUpdateDate().getTime());
            cal.add(Calendar.MINUTE, 30);
            Timestamp newTimestamp = new Timestamp(cal.getTimeInMillis());
            if (Timestamp.valueOf(LocalDateTime.now()).before(newTimestamp)){
                orders.setStatus(StatusOrder.CHO_HUY_TAI_QUAY);
                orders.setMoTa(order.getMoTa());
                orderRepository.save(orders);
                result.setMessage("SUCCESS");
                result.setData(orders);
            }
            else {
                result.setMessage("Hết thời gian để hủy đơn hàng");
            }
        }
        else {
            result.setMessage("Đơn hàng không tồn tại");
        }
        return result;
    }
}


