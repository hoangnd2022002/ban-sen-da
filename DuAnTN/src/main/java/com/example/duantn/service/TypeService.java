package com.example.duantn.service;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.entity.Color;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.entity.Type;
import com.example.duantn.repository.ColorRepository;
import com.example.duantn.repository.ProductRepository;
import com.example.duantn.repository.ProductSizeRepository;
import com.example.duantn.repository.TypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TypeService {
    private final ColorRepository colorRepository; 
    private final ProductSizeRepository productSizeRepository;
    private final ProductRepository productRepository;
    private final TypeRepository typeRepository;

    public ResponseDTO<List<Type>> getAll(){
        ResponseDTO<List<Type>> result = new ResponseDTO<>();
        result.setData(typeRepository.findAll());
        result.setMessage("GET_TYPE_SUCCESS");
        return result;
    }
    public ResponseDTO<List<Type>> getAll1(){
        ResponseDTO<List<Type>> result = new ResponseDTO<>();
        result.setData(typeRepository.getTypeStatus1());
        result.setMessage("GET_TYPE_SUCCESS");
        return result;
    }
    public ResponseDTO<List<Type>> getAll2(){
        ResponseDTO<List<Type>> result = new ResponseDTO<>();
        result.setData(typeRepository.getTypeStatus2());
        result.setMessage("GET_TYPE_SUCCESS");
        return result;
    }

    public ResponseDTO<Type> addType(Type type){
        ResponseDTO<Type> result = new ResponseDTO<>();
        String s= typeRepository.checkTrung(type.getTypeName());
        if (!StringUtils.hasText(s)){
            type.setStatus(1);
            typeRepository.save(type);
            result.setData(type);
            result.setMessage("ADD_TYPE_SUCCESS");
        }else {
            result.setData(type);
            result.setMessage("FAIL_NAME_TYPE");
        }
        return result;
    }
    public ResponseDTO<Type> editType(Type type){
        ResponseDTO<Type> result = new ResponseDTO<>();
        String s= colorRepository.checkTrung(type.getTypeName());
        if (!StringUtils.hasText(s)){
            type.setStatus(1);
            typeRepository.save(type);
            result.setData(type);
            result.setMessage("EDIT_TYPE_SUCCESS");
        }else {
            result.setData(type);
            result.setMessage("FAIL_TYPE_COLOR");
        }
        return result;
    }
    public ResponseDTO<Type> deleteType(Type type){
        ResponseDTO<Type> result = new ResponseDTO<>();
        if (typeRepository.findById(type.getIdType()).isPresent()){
            type.setStatus(2);
            typeRepository.save(type);
            List<ProductSize> productSizeList = productSizeRepository.findProductSizesByType(type.getIdType());
            if (productSizeList.size()>0){
                for ( ProductSize productSize : productSizeList ) {
                    productSize.setStatus(2);
                    productSizeRepository.save(productSize);
                    if (productSizeRepository.demSoLuongProductSize(productSize.getProduct().getIdProduct()) == 0){
                        productRepository.updateStatus(productSize.getProduct().getIdProduct());
                    }
                }
            result.setData(type);
            result.setMessage("DELETE_TYPE_SUCCESS");
            }
            result.setData(type);
            result.setMessage("DELETE_TYPE_SUCCESS");
        }
        return result;
    }

    public ResponseDTO<List<Product>> getProductByType(int pageNumber,int pageSize,int id){
        ResponseDTO<List<Product>> result = new ResponseDTO<>();
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        result.setData(typeRepository.getProductByType(page,id));
        result.setMessage("GET_PRODUCT_SUCCESS");
        return result;
    }
    public ResponseDTO<List<ProductSize>> getProductSizeByType(int pageNumber, int pageSize, SearchReq searchReq){
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        ResponseDTO<List<ProductSize>> result = new ResponseDTO<>();
        List<ProductSize> productSizeList = new ArrayList<>();
        for ( ProductSize productSize : productSizeRepository.findProductSizesByTypePage(page, searchReq.getIdType(), searchReq.getKeyword()) ) {
            productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
            productSizeList.add(productSize);
        }
        result.setData(productSizeList);
        result.setMessage("GET_PRODUCT_SIZE_SUCCESS");
        return result;
    }
    public ResponseDTO<Type> getTypeByIDType(int id){
        ResponseDTO<Type> result = new ResponseDTO<>();
        Type type = typeRepository.findById(id).orElse(null);
        result.setData(type);
        result.setMessage("GET_TYPE_SUCCESS");
        return result;
    }
    public ResponseDTO<List<Type>> getTypeByName(String name){

        ResponseDTO<List<Type>> result = new ResponseDTO<>();
        List<Type> type = typeRepository.searchType(name);
        result.setData(type);
        result.setMessage("GET_TYPE_SUCCESS");
        return result;
    }
}
