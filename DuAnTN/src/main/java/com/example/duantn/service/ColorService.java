package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.Color;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.repository.ColorRepository;
import com.example.duantn.repository.ProductRepository;
import com.example.duantn.repository.ProductSizeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ColorService {
    private final ColorRepository colorRepository; 
    private final ProductSizeRepository productSizeRepository;
    private final ProductRepository productRepository;
    public ResponseDTO<Color> addColor(Color color){
        ResponseDTO<Color> result = new ResponseDTO<>();
        String s= colorRepository.checkTrung(color.getColorName());
        if (!StringUtils.hasText(s)){
            color.setStatus(1);
            colorRepository.save(color);
            result.setData(color);
            result.setMessage("ADD_COLOR_SUCCESS");
        }else {
            result.setData(color);
            result.setMessage("FAIL_NAME_COLOR");
        }
        return result;
    }
    public ResponseDTO<Color> editColor(Color color){
        ResponseDTO<Color> result = new ResponseDTO<>();
        String s= colorRepository.checkTrung(color.getColorName());
        if (!StringUtils.hasText(s)){
            color.setStatus(1);
            colorRepository.save(color);
            result.setData(color);
            result.setMessage("EDIT_COLOR_SUCCESS");
        }else {
            result.setData(color);
            result.setMessage("FAIL_NAME_COLOR");
        }
        return result;
    }
    public ResponseDTO<Color> deleteColor(Color color){
        ResponseDTO<Color> result = new ResponseDTO<>();
        if (colorRepository.findById(color.getId()).isPresent()){
            color.setStatus(2);
            colorRepository.save(color);
            List<ProductSize> productSizeList = productSizeRepository.searchProductSizeTheoColor(color.getId());
            if (productSizeList.size()>0){
                for ( ProductSize productSize : productSizeList ) {
                    productSize.setStatus(2);
                    productSizeRepository.save(productSize);
                    if (productSizeRepository.demSoLuongProductSize(productSize.getProduct().getIdProduct()) == 0){
                        productRepository.updateStatus(productSize.getProduct().getIdProduct());
                    }
                }
            result.setData(color);
            result.setMessage("DELETE_COLOR_SUCCESS");
            }
            result.setData(color);
            result.setMessage("FAIL_DELETE_COLOR");
        }
        return result;
    }
    public ResponseDTO<List<Color>> getAll(){
        ResponseDTO<List<Color>> result = new ResponseDTO<>();
        result.setData(colorRepository.getAllColorStatus1());
        result.setMessage("GET_COLOR_SUCCESS");
        return result;
    }
//    public ResponseDTO<List<Color>> getByCateDad(int id){
//        ResponseDTO<List<Color>> result = new ResponseDTO<>();
//        result.setData(colorRepository.getAllByCate(id));
//        result.setMessage("GET_COLOR_SUCCESS");
//        return result;
//    }
}
