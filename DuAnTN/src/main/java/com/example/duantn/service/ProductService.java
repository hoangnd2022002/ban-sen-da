package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.ProductDTOReq;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.entity.ProductSize;

import java.util.List;

public interface ProductService {
    List<ProductDTORes> getAllProducts();
    List<ProductDTORes> getAllProductsStatus1();
    List<ProductDTORes> getAllProductHetSoLuong(String name);
    List<ProductDTORes> getAllProductsStatus2();
    ResponseDTO<ProductDTORes> getDetailProduct(ProductDTOReq productDTOReq);
    ProductDTORes getDetailProductByID(int id);
    ResponseDTO<ProductDTORes> addProduct(ProductDTOReq productDTOReq);
    ResponseDTO<ProductDTORes> editProduct(ProductDTOReq productDTOReq);
    ResponseDTO<ProductDTORes> banLaiProduct(ProductDTOReq productDTOReq);
    void deleteProduct(ProductDTOReq productDTOReq);
    List<ProductDTORes> getAllProductsByType(int id);
    List<ProductDTORes> getProductByPrice(SearchReq searchReq);
    List<ProductDTORes> getProductPriceGiamDan();
    List<ProductDTORes> getProductPriceTangDan();
    List<ProductSize> getAllProductsSizeStatus1(int pageNumber, int pageSize,int category,String keyword);
    ResponseDTO<ProductSize> getDetailByQrCode(String QrCode);

}
