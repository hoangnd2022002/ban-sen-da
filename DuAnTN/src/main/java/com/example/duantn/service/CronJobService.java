package com.example.duantn.service;

import com.example.duantn.entity.Orders;
import com.example.duantn.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CronJobService {

    private final OrderRepository orderRepository;


    @SneakyThrows
    @Scheduled(initialDelay = 10000,fixedDelay = 30000)
    public void startBatch(){
        List<Orders> list = orderRepository.getOrderDAGIAO();
        Timestamp ngayHienTai = Timestamp.valueOf(LocalDateTime.now());
        for ( Orders orders : list ) {
            if (!ObjectUtils.isEmpty(orders.getConversionDate())){
                if (ngayHienTai.after(orders.getConversionDate())){
                    orderRepository.updateOrderStatus("ĐÃ NHẬN",orders.getIdOrder());
                }
            }
        }
        System.out.println("reload"+ngayHienTai);
    }
}
