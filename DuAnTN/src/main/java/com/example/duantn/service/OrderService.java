package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.OrderDTO;
import com.example.duantn.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

public interface OrderService {
    ResponseDTO<OrderDTO> xacNhanDangChuanBiDonHang(OrderDTO orderDTO);
    ResponseDTO<OrderDTO> choXacNhan(OrderDTO orderDTO);
    ResponseDTO<OrderDTO> choXacNhanVNPay(OrderDTO orderDTO) throws ServletException, IOException;
    ResponseDTO<OrderDTO> xacNhanDangGiaoDonHang(OrderDTO orderDTO);
    ResponseDTO<OrderDTO> xacNhanDaGiaoDonHang(OrderDTO orderDTO);
    ResponseDTO<OrderDTO> huyKhiDangGiao(OrderDTO orderDTO);
    ResponseDTO<Orders> xacNhanHuyTaiQuay(Orders orders, int idUser);
    ResponseDTO<Orders> choXacNhanHuyTaiQuay(Orders orders);
    ResponseDTO<Orders> huyBoHuyTaiQuay(Orders orders);
    ResponseDTO<OrderDTO> xacNhanDaNhanDonHang(OrderDTO orderDTO);
    ResponseDTO<OrderDTO> xacNhanHuyDonHang(OrderDTO orderDTO);
    ResponseDTO<OrderDTO> xacNhanTaiQuayDonHang(OrderDTO orderDTO);
    ResponseDTO<Page<Orders>> getOrderByStatus(String status, int pageNumber, int pageSize);
    ResponseDTO<OrderDTO> capNhatSanPham(OrderDTO orderDTO);
    ResponseEntity themVaoGio(ProductSize productSize, User user);
    List<OrderDetail> getOrderDetail(User user);
    List<OrderDetail> getOrderDetail(Orders orders);
    List<Orders> getOrderByUser(int id,String status);
    Orders getOrder(Integer idOrder);
    OrderDetail getOrderDetailById(Integer id);
    void deleteOrderDetail(OrderDetail orderDetail);
    void updateQuanTity(OrderDetail orderDetail);
    Integer soLuongTheoTrangThai(String trangThai);
    Integer soLuongTheoGioHangUser(int id);
}
