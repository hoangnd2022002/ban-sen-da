package com.example.duantn.service.impl;

import com.example.duantn.base.StatusOrder;
import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.ProductDTOReq;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.entity.Habitats;
import com.example.duantn.entity.HabitatsProduct;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.repository.*;
import com.example.duantn.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {


    private final ProductRepository productRepository;
    private final ProductSizeRepository productSizeRepository;
    private final HabitatRepository habitatRepository;
    private final HabitatProductRepository habitatProductRepository;
    private final ProductDTORes productDTORes;
    private final TypeRepository typeRepository;
    private final LightAspectRepository lightAspectRepository;

    @Override
    public List<ProductDTORes> getAllProducts() {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst = productRepository.findAll();
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
            productDTORes1.setStt(String.valueOf(stt));
            stt++;
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTOResList.add(productDTORes1);
        }
        return productDTOResList;
    }

    @Override
    public List<ProductDTORes> getAllProductsStatus1() {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst = productRepository.getProductsStatus1();

        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
            productDTORes1.setStt(String.valueOf(stt));
            stt++;
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTORes1.setType(product.getType());
            productDTORes1.setLightAspect(product.getLightAspect());
            productDTOResList.add(productDTORes1);
        }
        return productDTOResList;
    }

    @Override
    public List<ProductDTORes> getAllProductHetSoLuong(String name) {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst = productRepository.getProductHetSoLuongVer2(name);

        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
            productDTORes1.setStt(String.valueOf(stt));
            stt++;
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTORes1.setType(product.getType());
            productDTORes1.setLightAspect(product.getLightAspect());
            productDTOResList.add(productDTORes1);
        }

        return productDTOResList;
    }

    @Override
    public List<ProductSize> getAllProductsSizeStatus1(int pageNumber,int pageSize, int type, String keyword) {
        Pageable page =
                PageRequest.of(pageNumber, pageSize);
        List<ProductSize> productSizeList = new ArrayList<>();

        for ( ProductSize productSize : productSizeRepository.getAllPSStatus1(page,keyword)) {
            productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
            productSizeList.add(productSize);
        }

        return productSizeList;
    }

    @Override
    public ResponseDTO<ProductSize> getDetailByQrCode(String QrCode) {
        ResponseDTO<ProductSize> result = new ResponseDTO<>();
        ProductSize productSize= productSizeRepository.getProductSizeByQrCode(QrCode);
        if (productSize != null){
            if (productSize.getQuantity()<=0){
                result.setMessage("PRODUCT_DETAIL_IS_OUT_OF_STOCK");
            }else {
                result.setData(productSize);
                result.setMessage("SUCCESS");
            }
        }else {
            result.setMessage("PRODUCT_DETAIL_HAS_BEEN_DELETE");
        }
        return result;
    }

    public List<ProductDTORes> getAllProductsByType(int id) {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst = productRepository.getProductsByIDTypeS1(id);
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
            productDTORes1.setStt(String.valueOf(stt));
            stt++;
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTORes1.setType(product.getType());
            productDTORes1.setLightAspect(product.getLightAspect());
            productDTOResList.add(productDTORes1);
        }
        return productDTOResList;
    }

    @Override
    public List<ProductDTORes> getAllProductsStatus2() {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst = productRepository.getProductsStatus2();
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
            productDTORes1.setStt(String.valueOf(stt));
            stt++;
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus2(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTORes1.setType(product.getType());
            productDTORes1.setLightAspect(product.getLightAspect());
            productDTOResList.add(productDTORes1);
        }
        return productDTOResList;
    }





    @Override
    public ResponseDTO<ProductDTORes> getDetailProduct(ProductDTOReq productDTOReq) {
        ResponseDTO<ProductDTORes> result = new ResponseDTO<>();
        try {
            Product p = productRepository.findById(productDTOReq.getIdProduct()).orElse(null);
            if (p != null && p.getStatus() == 1) {
                ProductDTORes productDTORes1 = productDTORes.fromDTOAddPro(p);
                List<ProductSize> productSizeList = new ArrayList<>();
                for (ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct())) {
                    productSize.setImagePath(StatusOrder.HOST_SERVER + "/image/" + productSize.getImagePath());
                    productSizeList.add(productSize);
                }
                List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
                productDTORes1.setHabitatsList(habitatsList);
                productDTORes1.setProductSizeList(productSizeList);
                productDTORes1.setType(p.getType());
                productDTORes1.setLightAspect(p.getLightAspect());
                result.setData(productDTORes1);
                result.setMessage("FIND_SUCCESS");
            } else if (p != null && p.getStatus() == 2) {
                ProductDTORes productDTORes1 = productDTORes.fromDTOAddPro(p);
                List<ProductSize> productSizeList = new ArrayList<>();
                for (ProductSize productSize : productSizeRepository.searchProductAndSizeStatus(productDTORes1.getIdProduct())) {
                    productSize.setImagePath(StatusOrder.HOST_SERVER + "/image/" + productSize.getImagePath());
                    productSizeList.add(productSize);
                }
                List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
                productDTORes1.setHabitatsList(habitatsList);
                productDTORes1.setProductSizeList(productSizeList);
                productDTORes1.setType(p.getType());
                productDTORes1.setLightAspect(p.getLightAspect());
                productDTORes1.setProductName(p.getProductName());
                result.setData(productDTORes1);
                result.setMessage("FIND_SUCCESS");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage("FAIL_PRODUCT_NAME");
        }
        return result;
    }

    @Override
    public ProductDTORes getDetailProductByID(int id) {
        Product p = productRepository.findById(id).orElse(null);
        ProductDTORes productDTORes1 = new ProductDTORes();
        if (p != null) {
            productDTORes1 = ProductDTOReq.fromDTO(p);
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTORes1.setType(p.getType());
            productDTORes1.setLightAspect(p.getLightAspect());
        }
        return productDTORes1;
    }

    @Override
    public ResponseDTO<ProductDTORes> addProduct(ProductDTOReq productDTOReq) {
        ResponseDTO<ProductDTORes> result = new ResponseDTO<>();
        try {
            Product p = productDTOReq.convertToEntityProduct(productDTOReq);
//            ProductSize ps = productDTOReq.convertToEntityProductSize(productDTOReq);
            List<ProductSize> productSizeList =productDTOReq.getProductSizeList();
            List<Habitats> habitatsList =productDTOReq.getHabitatsList();
            if (productRepository.findNameProductByName(productDTOReq.getProductName()) != null) {
                result.setMessage("FAIL_PRODUCT_NAME");
            } else if (productRepository.findNameProductByName(productDTOReq.getProductName()) == null) {
                p.setStatus(1);
                p.setType(typeRepository.findById(productDTOReq.getType()).orElse(null));
                p.setLightAspect(lightAspectRepository.findById(productDTOReq.getLightAspect()).orElse(null));
                List<ProductSize> productSizeList1 = new ArrayList<>();
                List<Integer> listColor = new ArrayList<>();
                List<Integer> listSize = new ArrayList<>();

                for ( ProductSize productSize : productSizeList) {
                    productSize.setStatus(1);
                    productSize.setProduct(p);
                    byte[] decodedBytes = Base64.getDecoder().decode(productSize.getImagePath());   // Tạo một đối tượng BufferedImage từ base64 string
                    BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes));                     // Tạo một đường dẫn đến thư mục resource
                    String newNameImage= String.valueOf(UUID.randomUUID());
                    String resourceFolder = "src/main/resources/static/images/";                                                  // Lưu file vào thư mục resource
                    File output = new File(resourceFolder + newNameImage+".jpg");
                    ImageIO.write(image, "jpg", output);
                    productSize.setImagePath(newNameImage+".jpg");
                    productSize.setQrCode(String.valueOf(UUID.randomUUID()));
                    listSize.add(productSize.getSize().getIdSize());
                    listColor.add(productSize.getColor().getId());
                    productSizeList1.add(productSize);
                }
                List<String> stringList = new ArrayList<>();
                for (int i = 0; i < listColor.size(); i++) {
                    String checkSo= String.valueOf(listColor.get(i)) +String.valueOf(listSize.get(i));
                   stringList.add(checkSo);
                }
                Map<String, Integer> countMap = new HashMap<>();
                for (String item : stringList) {
                    countMap.put(item, countMap.getOrDefault(item, 0) + 1);
                }
                List<String> duplicatedItems = countMap.entrySet().stream()
                        .filter(entry -> entry.getValue() > 1)
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList());
                if (duplicatedItems.size()>0){
                    result.setMessage("Trùng Size hoặc Màu");
                }else {
                    List<ProductSize> productSizeList2 = new ArrayList<>();
                    Product get = productRepository.save(p);
                    for ( ProductSize productSize : productSizeList1 ) {
                        productSize.setProduct(get);
                        productSizeList2.add(productSize);
                    }
                    productSizeRepository.saveAll(productSizeList2);
                    List<HabitatsProduct> habitatsProductList = new ArrayList<>();
                    for (Habitats habitats:habitatsList) {
                        HabitatsProduct habitatsProduct = new HabitatsProduct();
                        habitatsProduct.setHabitats(habitats);
                        habitatsProduct.setProduct(get);
                        habitatsProduct.setStatus(1);
                        habitatsProductList.add(habitatsProduct);
                    }
                    habitatProductRepository.saveAll(habitatsProductList);
                    result.setMessage("SUCCESS_PRODUCT");
                    result.setData(productDTORes.fromDTOAddPro(get));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage("FAIL_PRODUCT");
        }
        return result;
    }

    @Override
    public ResponseDTO<ProductDTORes> editProduct(ProductDTOReq productDTOReq) {
        ResponseDTO<ProductDTORes> result = new ResponseDTO<>();
        try {
            Product p = productDTOReq.convertToEntityProduct(productDTOReq);
//            ProductSize ps = productDTOReq.convertToEntityProductSize(productDTOReq);
            Product pName = productRepository.findById(productDTOReq.getIdProduct()).orElse(null);//tên theo id = tên theo req thì sửa ?
            List<ProductSize> productSizeList = productDTOReq.getProductSizeList();
            List<Habitats> habitatsList = productDTOReq.getHabitatsList();
            if (!ObjectUtils.isEmpty(pName)) {
                if (!pName.getProductName().equals(productDTOReq.getProductName()) && productRepository.findNameProductByName(productDTOReq.getProductName()) != null) {
                    result.setMessage("FAIL_PRODUCT_NAME");
                    result.setData(productDTORes.fromDTOAddPro(p));
                } else {
                    p.setStatus(1);
                    p.setMoTa(productDTOReq.getMoTa());
                    List<Integer> listColor = new ArrayList<>();
                    List<Integer> listSize = new ArrayList<>();
                    List<ProductSize> productSizeList1 = new ArrayList<>();
                    List<ProductSize> productSizeList2 = new ArrayList<>();
                    List<HabitatsProduct> habitatsProductList = new ArrayList<>();
                    habitatProductRepository.deleteByIDProduct(productDTOReq.getIdProduct());
                    for (Habitats habitats : habitatsList) {
                        HabitatsProduct habitatsProduct = new HabitatsProduct();
                        habitatsProduct.setProduct(pName);
                        habitatsProduct.setHabitats(habitats);
                        habitatsProduct.setStatus(1);
                        habitatsProductList.add(habitatsProduct);
                    }
                    habitatProductRepository.saveAll(habitatsProductList);
                    for (ProductSize productSize : productSizeList) {
                        if (productSize.getIdProductSize() != null) {
                            ProductSize productSize1 = productSizeRepository.checkProductSize(productSize.getIdProductSize()).orElse(null);
                            if (!ObjectUtils.isEmpty(productSize1)) {
                                productSize1.setQuantity(productSize.getQuantity());            //size
                                if (productSize.getImagePath().length() > 200) {
                                    byte[] decodedBytes = Base64.getDecoder().decode(productSize.getImagePath());   // Tạo một đối tượng BufferedImage từ base64 string
                                    BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes));                     // Tạo một đường dẫn đến thư mục resource
                                    String newNameImage = String.valueOf(UUID.randomUUID());
                                    String resourceFolder = "src/main/resources/static/images/";                                                  // Lưu file vào thư mục resource
                                    File output = new File(resourceFolder + newNameImage + ".jpg");
                                    ImageIO.write(image, "jpg", output);
                                    productSize.setImagePath(newNameImage + ".jpg");
                                    productSize1.setImagePath(newNameImage + ".jpg");
                                } else {
                                    productSize1.setImagePath(productSize1.getImagePath());
                                }
                                productSize1.setPrice(productSize.getPrice());            //image
                                productSize1.setSize(productSize.getSize());            //image
                                productSize1.setColor(productSize.getColor());            //image
                                productSize1.setStatus(productSize.getStatus());            //status
                                productSizeList1.add(productSize1);
                            }
                        }
                        if (productSize.getIdProductSize() == null) {
                            productSize.setProduct(pName);
                            productSize.setPrice(productSize.getPrice());
                            productSize.setQrCode(String.valueOf(UUID.randomUUID()));
                            productSize.setStatus(1);
                            byte[] decodedBytes = Base64.getDecoder().decode(productSize.getImagePath());   // Tạo một đối tượng BufferedImage từ base64 string
                            BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes));                     // Tạo một đường dẫn đến thư mục resource
                            String newNameImage = String.valueOf(UUID.randomUUID());
                            String resourceFolder = "src/main/resources/static/images/";                                                  // Lưu file vào thư mục resource
                            File output = new File(resourceFolder + newNameImage + ".jpg");
                            ImageIO.write(image, "jpg", output);
                            productSize.setImagePath(newNameImage + ".jpg");
                            productSize.setStatus(1);
                            productSizeList2.add(productSize);
                        }
                        listSize.add(productSize.getSize().getIdSize());
                        listColor.add(productSize.getColor().getId());
                    }
                    productSizeRepository.saveAll(productSizeList1);
                    productSizeRepository.saveAll(productSizeList2);
                    productRepository.updateMotaTenAnhSangType(productDTOReq.getMoTa(), productDTOReq.getProductName(), productDTOReq.getType(), productDTOReq.getLightAspect(), productDTOReq.getIdProduct());
                    result.setData(productDTORes.fromDTOAddPro(p));
                    result.setMessage("SUCCESS-EDIT-PRODUCT");

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage("FAIL_PRODUCT");
        }

        return result;
    }

    @Override
    public ResponseDTO<ProductDTORes> banLaiProduct(ProductDTOReq productDTOReq) {
        ResponseDTO<ProductDTORes> result = new ResponseDTO<>();
        try {
            Product p = productDTOReq.convertToEntityProduct(productDTOReq);
//            ProductSize ps = productDTOReq.convertToEntityProductSize(productDTOReq);
            Product pName = productRepository.findById(productDTOReq.getIdProduct()).orElse(null);//tên theo id = tên theo req thì sửa ?
            List<ProductSize> productSizeList = productDTOReq.getProductSizeList();
            List<Habitats> habitatsList = productDTOReq.getHabitatsList();
            if (!ObjectUtils.isEmpty(pName)) {
                if (productRepository.findNameProductByName(productDTOReq.getProductName())!=null){
                    result.setMessage("FAIL_PRODUCT_NAME");
                    result.setData(productDTORes.fromDTOAddPro(p));
                }
                else {
                    p.setStatus(1);
                    p.setMoTa(productDTOReq.getMoTa());
                    List<Integer> listColor = new ArrayList<>();
                    List<Integer> listSize = new ArrayList<>();
                    List<ProductSize> productSizeList1 = new ArrayList<>();
                    List<ProductSize> productSizeList2 = new ArrayList<>();
                    List<HabitatsProduct> habitatsProductList = new ArrayList<>();
                    habitatProductRepository.deleteByIDProduct(productDTOReq.getIdProduct());
                    for (Habitats habitats : habitatsList) {
                        HabitatsProduct habitatsProduct = new HabitatsProduct();
                        habitatsProduct.setProduct(pName);
                        habitatsProduct.setHabitats(habitats);
                        habitatsProduct.setStatus(1);
                        habitatsProductList.add(habitatsProduct);
                    }
                    habitatProductRepository.saveAll(habitatsProductList);
                    for (ProductSize productSize : productSizeList) {
                        if (productSize.getIdProductSize() != null) {
                            ProductSize productSize1 = productSizeRepository.checkProductSize(productSize.getIdProductSize()).orElse(null);
                            if (!ObjectUtils.isEmpty(productSize1)) {
                                productSize1.setQuantity(productSize.getQuantity());            //size
                                if (productSize.getImagePath().length() > 200) {
                                    byte[] decodedBytes = Base64.getDecoder().decode(productSize.getImagePath());   // Tạo một đối tượng BufferedImage từ base64 string
                                    BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes));                     // Tạo một đường dẫn đến thư mục resource
                                    String newNameImage = String.valueOf(UUID.randomUUID());
                                    String resourceFolder = "src/main/resources/static/images/";                                                  // Lưu file vào thư mục resource
                                    File output = new File(resourceFolder + newNameImage + ".jpg");
                                    ImageIO.write(image, "jpg", output);
                                    productSize.setImagePath(newNameImage + ".jpg");
                                    productSize1.setImagePath(newNameImage + ".jpg");
                                } else {
                                    productSize1.setImagePath(productSize1.getImagePath());
                                }
                                productSize1.setPrice(productSize.getPrice());            //image
                                productSize1.setStatus(productSize.getStatus());            //status
                                productSizeList1.add(productSize1);
                            }
                        }
                        if (productSize.getIdProductSize() == null) {
                            productSize.setProduct(pName);
                            productSize.setPrice(productSize.getPrice());
                            productSize.setQrCode(String.valueOf(UUID.randomUUID()));
                            productSize.setStatus(1);
                            byte[] decodedBytes = Base64.getDecoder().decode(productSize.getImagePath());   // Tạo một đối tượng BufferedImage từ base64 string
                            BufferedImage image = ImageIO.read(new ByteArrayInputStream(decodedBytes));                     // Tạo một đường dẫn đến thư mục resource
                            String newNameImage = String.valueOf(UUID.randomUUID());
                            String resourceFolder = "src/main/resources/static/images/";                                                  // Lưu file vào thư mục resource
                            File output = new File(resourceFolder + newNameImage + ".jpg");
                            ImageIO.write(image, "jpg", output);
                            productSize.setImagePath(newNameImage + ".jpg");
                            productSize.setStatus(1);
                            productSizeList2.add(productSize);
                        }
                        listSize.add(productSize.getSize().getIdSize());
                        listColor.add(productSize.getColor().getId());
                    }

                        productSizeRepository.saveAll(productSizeList1);
                        productSizeRepository.saveAll(productSizeList2);
                        productRepository.updateMotaTenAnhSangType(productDTOReq.getMoTa(), productDTOReq.getProductName(), productDTOReq.getType(), productDTOReq.getLightAspect(), productDTOReq.getIdProduct());
                        result.setData(productDTORes.fromDTOAddPro(p));
                        result.setMessage("SUCCESS-EDIT-PRODUCT");

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage("FAIL_PRODUCT");
        }

        return result;
    }

    @Override
    public void deleteProduct(ProductDTOReq productDTOReq) {
        ResponseDTO<ProductDTORes> result = new ResponseDTO<ProductDTORes>();
        try {
            Product p = productRepository.findById(productDTOReq.getIdProduct()).orElse(null);
            if (p==null){
                result.setMessage("FAIL_PRODUCT");
            }else {
                p.setStatus(2);
                productRepository.save(p);
                productSizeRepository.changeStatusByProduct(p.getIdProduct());
                result.setData(ProductDTOReq.fromDTO(p));
                result.setMessage("FIND_SUCCESS");
            }

        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage("FAIL_PRODUCT");
        }
    }
    public List<ProductDTORes> getProductByName(SearchReq searchReq) {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst;
        if (ObjectUtils.isEmpty(searchReq.getIdType())){
            lst = productRepository.searchProductNoType(searchReq.getKeyword());
        }
        else {
             lst = productRepository.searchProductAndType(searchReq.getKeyword(), searchReq.getIdType());
        }
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = productDTORes.fromDTOAddProtest(product);
            productDTORes1.setStt(String.valueOf(stt));
            productDTOResList.add(productDTORes1);
            stt++;
        }
        return productDTOResList;
    }
    public List<ProductDTORes> getProductByNameNgungBan(SearchReq searchReq) {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst;
        if (ObjectUtils.isEmpty(searchReq.getIdType())){
            lst = productRepository.searchProductAndTypeNgungBanNoType(searchReq.getKeyword());
        }
        else {
            lst = productRepository.searchProductAndTypeNgungBan(searchReq.getKeyword(), searchReq.getIdType());
        }
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = productDTORes.fromDTOAddProtest2(product);
            productDTORes1.setStt(String.valueOf(stt));
            productDTOResList.add(productDTORes1);
            stt++;
        }
        return productDTOResList;
    }
    public List<ProductDTORes> getProductByNamePro(SearchReq searchReq) {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        List<Product> lst = productRepository.searchProductNotCate(searchReq.getKeyword());
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = productDTORes.fromDTOAddProtest(product);
            productDTORes1.setStt(String.valueOf(stt));
            productDTOResList.add(productDTORes1);
            stt++;
        }
        return productDTOResList;
    }

    public List<Product> getProductByNameAndType(SearchReq searchReq, int id) {
        return productRepository.searchProductInType(searchReq.getKeyword(),id);
    }
    @Override
    public List<ProductDTORes> getProductByPrice(SearchReq searchReq) {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
        String all ="%%";
        List<Product> lst;
        int length = 0;
        if(searchReq.getIdHabitat() == null ) {
            searchReq.setIdHabitat(habitatRepository.getIdHabitats());
        } else length = searchReq.getIdHabitat().size();
        lst = productRepository.search(searchReq.getKeyword1() == null ? 0 : searchReq.getKeyword1()
                , searchReq.getKeyword2() == null ? 100000000 : searchReq.getKeyword2()
                , searchReq.getIdType() == null ? all : searchReq.getIdType().toString()
                , length
                , searchReq.getIdHabitat()
                , searchReq.getIdLightAspect() == null ? all : searchReq.getIdLightAspect().toString()
        );
//        if (searchReq.getIdType()!=null && searchReq.getKeyword1()!=null && searchReq.getKeyword2()!=null){                    //Trường hợp 1
//             lst = productRepository.searchProductByPriceCoType(searchReq.getKeyword1(), searchReq.getKeyword2(),searchReq.getIdType());
//        }else if (searchReq.getIdType()!=null && searchReq.getKeyword1()==null && searchReq.getKeyword2()==null){              //Trường hợp 2
//             lst = productRepository.searchProductByPriceTH2(searchReq.getIdType());
//        }else if (searchReq.getIdType()!=null && searchReq.getKeyword1()!=null && searchReq.getKeyword2()==null){              //Trường hợp 3
//             lst = productRepository.searchProductByPriceTH3(searchReq.getIdType(), searchReq.getKeyword1());
//        }else if (searchReq.getIdType()!=null && searchReq.getKeyword1()==null && searchReq.getKeyword2()!=null){              //Trường hợp 4
//            lst = productRepository.searchProductByPriceTH4(searchReq.getIdType(), searchReq.getKeyword2());
//        }else if (searchReq.getIdType()==null && searchReq.getKeyword1()==null && searchReq.getKeyword2()!=null){              //Trường hợp 5
//             lst = productRepository.searchProductByPriceTH5(searchReq.getKeyword2());
//        }else if (searchReq.getIdType()==null && searchReq.getKeyword1()!=null && searchReq.getKeyword2()==null){              //Trường hợp 6
//             lst = productRepository.searchProductByPriceTH6(searchReq.getKeyword1());
//        }else if (searchReq.getIdType()==null && searchReq.getKeyword1()!=null && searchReq.getKeyword2()!=null){              //Trường hợp 7
//            lst = productRepository.searchProductByPriceTH7(searchReq.getKeyword1(), searchReq.getKeyword2());
//        }
        int stt = 1;
        for (Product product : lst) {
            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
            productDTORes1.setStt(String.valueOf(stt));
            stt++;
            List<ProductSize> productSizeList = new ArrayList<>();
            for ( ProductSize productSize : productSizeRepository.searchProductAndSizeStatus1(productDTORes1.getIdProduct()) ) {
                productSize.setImagePath(StatusOrder.HOST_SERVER+"/image/"+productSize.getImagePath());
                productSizeList.add(productSize);
            }
            List<Habitats> habitatsList = new ArrayList<>(habitatRepository.getHabitatsByProduct(productDTORes1.getIdProduct()));
            productDTORes1.setHabitatsList(habitatsList);
            productDTORes1.setProductSizeList(productSizeList);
            productDTORes1.setType(product.getType());
            productDTORes1.setLightAspect(product.getLightAspect());;
            productDTOResList.add(productDTORes1);
        }
        return productDTOResList;
    }
    @Override
    public List<ProductDTORes> getProductPriceGiamDan() {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
//        List<Product> lst = productRepository.searchProductByPriceGiamDan();
//        int stt = 1;
//        for (Product product : lst) {
//            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
//            productDTORes1.setStt(String.valueOf(stt));
//            productDTOResList.add(productDTORes1);
//            stt++;
//        }
        return productDTOResList;
    }
    @Override
    public List<ProductDTORes> getProductPriceTangDan() {
        List<ProductDTORes> productDTOResList = new ArrayList<>();
//        List<Product> lst = productRepository.searchProductByPriceTangDan();
//        int stt = 1;
//        for (Product product : lst) {
//            ProductDTORes productDTORes1 = ProductDTORes.fromDTO(product);
//            productDTORes1.setStt(String.valueOf(stt));
//            productDTOResList.add(productDTORes1);
//            stt++;
//        }
        return productDTOResList;
    }

}


