package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.entity.Product;
import com.example.duantn.entity.ProductSize;
import com.example.duantn.entity.Size;
import com.example.duantn.repository.ProductRepository;
import com.example.duantn.repository.ProductSizeRepository;
import com.example.duantn.repository.SizeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SizeService {
    private final SizeRepository sizeRepository;
    private final ProductSizeRepository productSizeRepository;
    private final ProductRepository productRepository;

    public ResponseDTO<Size> addSize(Size size){
        ResponseDTO<Size> result = new ResponseDTO<>();
        String s= sizeRepository.checkTrung(size.getSizeName());
        if (!StringUtils.hasText(s)){
            size.setStatus(1);
            sizeRepository.save(size);
            result.setData(size);
            result.setMessage("ADD_SIZE_SUCCESS");
        }else {
            result.setData(size);
            result.setMessage("FAIL_NAME_SIZE");
        }
        return result;
    }
    public ResponseDTO<Size> editSize(Size size){
        ResponseDTO<Size> result = new ResponseDTO<>();
        String s= sizeRepository.checkTrung(size.getSizeName());
        if (!StringUtils.hasText(s)){
            sizeRepository.save(size);
            result.setData(size);
            result.setMessage("EDIT_SIZE_SUCCESS");
        }else {
            result.setData(size);
            result.setMessage("FAIL_NAME_SIZE");
        }
        return result;
    }
    public ResponseDTO<Size> deleteSize(Size size){
        ResponseDTO<Size> result = new ResponseDTO<>();
            if (sizeRepository.findById(size.getIdSize()).isPresent()){
                size.setStatus(2);
                sizeRepository.save(size);
                List<ProductSize> productSizeList = productSizeRepository.searchProductSizeTheoSize(size.getIdSize());
                if (productSizeList.size()>0){
                    for ( ProductSize productSize : productSizeList ) {
                        productSize.setStatus(2);
                        productSizeRepository.save(productSize);
                        if (productSizeRepository.demSoLuongProductSize(productSize.getProduct().getIdProduct()) == 0){
                            productRepository.updateStatus(productSize.getProduct().getIdProduct());
                        }
                    }
                }
                result.setData(size);
                result.setMessage("DELETE_SIZE_SUCCESS");
            }
        result.setMessage("DELETE_SIZE_FAILD");

        return result;
    }
    public ResponseDTO<List<Size>> getAll(){
        ResponseDTO<List<Size>> result = new ResponseDTO<>();
        result.setData(sizeRepository.getAllSizeByStatus1());
        result.setMessage("GET_SIZE_SUCCESS");
        return result;
    }
//    public ResponseDTO<List<Size>> getByCateDad(int id){
//        ResponseDTO<List<Size>> result = new ResponseDTO<>();
//        result.setData(sizeRepository.getAllByCate(id));
//        result.setMessage("GET_SIZE_SUCCESS");
//        return result;
//    }
}
