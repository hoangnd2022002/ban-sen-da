package com.example.duantn.service.impl;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.RoleDTOReq;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.dto.request.UserDTOReq;
import com.example.duantn.dto.response.UserDTORes;
import com.example.duantn.entity.Role;
import com.example.duantn.entity.User;
import com.example.duantn.repository.RoleRepository;
import com.example.duantn.repository.UserRepository;
import com.example.duantn.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.*;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserDTORes userDTORes;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    String themThanhCong="SUCCESS_ADD_USER";
    String suaThanhCong="SUCCESS_EDIT_USER";
    String thatBai="FAIL_USER";
    String trungUsername ="FAIL_USER_NAME";
    String trungEmail="FAIL_USER_EMAIL";
    String trungSDT="FAIL_USER_PHONE";
    String timThanhCong="SUCCESS_FIND_USER";
    @Override
    public List<UserDTORes> getAllUser() {
        List<UserDTORes> userDTOList = new ArrayList<>();
        List<User> lst = userRepository.findAll();
        int stt=1;
        for (User user : lst) {
            UserDTORes userDTO = userDTORes.convertToDTO(user);
            userDTO.setStt(String.valueOf(stt));
            userDTOList.add(userDTO);
            stt++;
        }
        return userDTOList;
    }
    @Override
    public List<UserDTORes> getAllUserStatus1() {
        List<UserDTORes> userDTOList = new ArrayList<>();
        List<User> lst = userRepository.getUsersStatus1();
        int stt=1;
        for (User user : lst) {
            UserDTORes userDTO = userDTORes.convertToDTO(user);
            userDTO.setStt(String.valueOf(stt));
            userDTOList.add(userDTO);
            stt++;
        }
        return userDTOList;
    }
    @Override
    public List<UserDTORes> getAllUserStatus2() {
        List<UserDTORes> userDTOList = new ArrayList<>();
        List<User> lst = userRepository.getUsersStatus2();
        int stt=1;
        for (User user : lst) {
            UserDTORes userDTO = userDTORes.convertToDTO(user);
            userDTO.setStt(String.valueOf(stt));
            userDTOList.add(userDTO);
            stt++;
        }
        return userDTOList;
    }

    @Override
    public ResponseDTO<UserDTORes> getDetailUser(int id) {
        ResponseDTO<UserDTORes> result = new ResponseDTO<UserDTORes>();
        try {
            User u = userRepository.findById(id).orElse(null);
            result.setData(UserDTORes.fromDTO(u));
            result.setMessage(timThanhCong);
        } catch (Exception e) {
            result.setMessage(thatBai);
        }
        return result;
    }

    @Override
    public ResponseDTO<UserDTORes> addUser(UserDTOReq userDTOReq) {
        userDTOReq.setCreatedate(Timestamp.valueOf(LocalDateTime.now()));
        ResponseDTO<UserDTORes> result = new ResponseDTO<UserDTORes>();
        try {
            User u = userDTOReq.convertToEntity(userDTOReq);
            if (userRepository.checkUsername(userDTOReq.getUsername()) != null) {
                result.setMessage(trungUsername);
            } else if (userRepository.checkEmail(userDTOReq.getEmail()) != null) {
                result.setMessage(trungEmail);
            }else if (userRepository.checkPhone(userDTOReq.getPhone()) != null) {
                result.setMessage(trungSDT);
            } else if (userRepository.checkUsername(userDTOReq.getUsername()) ==null && userRepository.checkEmail(userDTOReq.getEmail()) == null){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                u.setPassword(passwordEncoder.encode(userDTOReq.getPassword()));
                u.setStatus(1);
                userRepository.save(u);
                result.setData(userDTOReq.fromDTO(u));
                result.setMessage(themThanhCong);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage(thatBai);
        }
        return result;
    }

    @Override
    public ResponseDTO<UserDTORes> editUser(UserDTOReq userDTOReq) {
        userDTOReq.setCreatedate(Timestamp.valueOf(LocalDateTime.now()));
        ResponseDTO<UserDTORes> result = new ResponseDTO<UserDTORes>();
        try {
            User u = userDTOReq.convertToEntity(userDTOReq);
            User u1 = userRepository.findById(userDTOReq.getIdUser()).orElse(null);
            if (!StringUtils.hasText(userDTOReq.getPassword())){
                if (u1.getUsername().equals(u.getUsername()) || userRepository.checkUsername(userDTOReq.getUsername()) == null){
                    if (u1.getEmail().equals(u.getEmail()) || userRepository.checkEmail(userDTOReq.getEmail()) == null){
                        if (u1.getPhone().equals(u.getPhone()) || userRepository.checkPhone(userDTOReq.getPhone()) == null){

                            u.setStatus(1);
                            userRepository.saveUserWithoutPass(u.getUsername(),u.getEmail(),u.getSex(),u.getPhone(),u.getName(),u.getRole().getIdRole() ,u.getIdUser());
                            result.setData(userDTOReq.fromDTO(u));
                            result.setMessage(suaThanhCong);

                        }else if (userRepository.checkPhone(userDTOReq.getPhone()) != null) {
                            result.setMessage(trungSDT);
                        }
                    }else if (userRepository.checkEmail(userDTOReq.getEmail()) != null) {
                        result.setMessage(trungEmail);
                    }
                }else if (userRepository.checkUsername(userDTOReq.getUsername()) != null) {
                    result.setMessage(trungUsername);
                }
            }else {
                if (u1.getUsername().equals(u.getUsername()) || userRepository.checkUsername(userDTOReq.getUsername()) == null) {
                    if (u1.getEmail().equals(u.getEmail()) || userRepository.checkEmail(userDTOReq.getEmail()) == null) {
                        if (u1.getPhone().equals(u.getPhone()) || userRepository.checkPhone(userDTOReq.getPhone()) == null) {


                            u.setStatus(1);
                            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                            u.setPassword(passwordEncoder.encode(userDTOReq.getPassword()));
                            userRepository.save(u);
                            result.setData(userDTOReq.fromDTO(u));
                            result.setMessage(suaThanhCong);

                        } else if (userRepository.checkPhone(userDTOReq.getPhone()) != null) {
                            result.setMessage(trungSDT);
                        }
                    } else if (userRepository.checkEmail(userDTOReq.getEmail()) != null) {
                        result.setMessage(trungEmail);
                    }
                } else if (userRepository.checkUsername(userDTOReq.getUsername()) != null) {
                    result.setMessage(trungUsername);
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage(thatBai);
        }
        return result;
    }
    @Override
    public void deleteUser(UserDTOReq userDTOReq) {
        User u = userDTOReq.convertToEntity(userDTOReq);
        u.setStatus(2);
        userRepository.save(u);
    }

    @Override
    public List<UserDTORes> getAllUserByRole(RoleDTOReq roleDTOReq) {
        List<UserDTORes> userDTOList = new ArrayList<>();
        List<User> lst = userRepository.getUserByIDRole(roleDTOReq.getIdRole());
        int stt=1;
        for (User user : lst) {
            UserDTORes userDTO = userDTORes.convertToDTO(user);
            userDTO.setStt(String.valueOf(stt));
            userDTOList.add(userDTO);
            stt++;
        }
        return userDTOList;
    }

    @Override
    public List<Role> getAllRole() {
        List<Role> lstRole = roleRepository.findAll();

        return lstRole;
    }
    @Override
    public List<UserDTORes> getUserByName(SearchReq searchReq) {
        List<UserDTORes> userDTOList = new ArrayList<>();
        List<User> lst = userRepository.searchUser(searchReq.getKeyword(),searchReq.getRole());
        int stt = 1;
        for (User user : lst) {
            UserDTORes userDTO = userDTORes.convertToDTO(user);
            userDTO.setStt(String.valueOf(stt));
            userDTOList.add(userDTO);
            stt++;
        }
        return userDTOList;
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.searchUserByUsername(username);
    }

    @Override
    public ResponseDTO<UserDTORes> changePass(UserDTOReq userDTOReq) {

        ResponseDTO<UserDTORes> result = new ResponseDTO<>();
        try {
            User u1 = userRepository.findById(userDTOReq.getIdUser()).orElse(null);
            if (StringUtils.hasText(userDTOReq.getPassword())){
               if (bCryptPasswordEncoder.matches(userDTOReq.getOldPass(),u1.getPassword())){
                   BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                   u1.setPassword(passwordEncoder.encode(userDTOReq.getPassword()));
                   userRepository.save(u1);
                   result.setMessage(suaThanhCong);
               }
               else {
                   result.setMessage(thatBai);
               }
            }else {
                result.setMessage(thatBai);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage(thatBai);
        }
        return result;
    }
}
