package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.ThongKeDTO;
import com.example.duantn.dto.ThongKeTheoNam;
import com.example.duantn.entity.OrderDetail;
import com.example.duantn.entity.Orders;
import com.example.duantn.repository.OrderDetailRepository;
import com.example.duantn.repository.OrderRepository;
import com.example.duantn.repository.ProductSizeRepository;
import com.mchange.lang.FloatUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ThongKeService {

    private final OrderDetailRepository orderDetailRepository;
    private final OrderRepository orderRepository;
    private final ProductSizeRepository productSizeRepository;

    @Autowired
    ThongKeDTO thongKeDTORes;


    public ResponseDTO<ThongKeDTO> thongKeByDate (ThongKeDTO thongKeDTO) {
        ResponseDTO<ThongKeDTO> result = new ResponseDTO<>();
        if (thongKeDTO.getEndDate().after(Timestamp.valueOf(LocalDateTime.now().with(LocalTime.MAX)))) {
            result.setMessage("Ngày kết thúc không được lớn hơn ngày hiện tại");
        } else {
            thongKeDTO.setStartDate(Timestamp.valueOf(thongKeDTO.getStartDate().toLocalDateTime().with(LocalTime.MIN)));
            thongKeDTO.setEndDate(Timestamp.valueOf(thongKeDTO.getEndDate().toLocalDateTime().with(LocalTime.MAX)));
            ThongKeDTO thongKeDTO1 = new ThongKeDTO();
            int soLuongDaNhan = orderRepository.getOrdersByDANHAN(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            int soLuongDangGiao = orderRepository.getOrdersByDANGGIAO(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            int soLuongDaHuy = orderRepository.getOrdersByDAHUY(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            int soLuongDaGiao = orderRepository.getOrdersByDAGIAO(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            int soLuongBanTaiQuay = orderRepository.getOrdersByBANTAIQUAY(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            float doanhThu=0;
            if (orderRepository.doanhThu(thongKeDTO.getStartDate(), thongKeDTO.getEndDate()) == null){
                 doanhThu =0;
            }else {
                 doanhThu = orderRepository.doanhThu(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            }

            List<Orders> orderList = orderRepository.getOrdersDaBan(thongKeDTO.getStartDate(), thongKeDTO.getEndDate());
            int totalQuantity = 0;
            for ( Orders orders : orderList ) {
                List<OrderDetail> orderDetailList= orderDetailRepository.listOdByO(orders.getIdOrder());
                for ( OrderDetail orderDetail : orderDetailList ) {
                    totalQuantity+=orderDetail.getQuantity();
                }
            }

            thongKeDTORes.setSoLuongBan(totalQuantity);
            thongKeDTORes.setDonDaNhan(soLuongDaNhan);
            thongKeDTORes.setDonDaGiao(soLuongDaGiao);
            thongKeDTORes.setDonDaHuy(soLuongDaHuy);
            thongKeDTORes.setDonDangGiao(soLuongDangGiao);
            thongKeDTORes.setBanTaiQuay(soLuongBanTaiQuay);
            thongKeDTORes.setDoanhThu(String.valueOf(doanhThu));
            thongKeDTORes.setStartDate(thongKeDTO.getStartDate());
            thongKeDTORes.setEndDate(thongKeDTO.getEndDate());
            result.setData(thongKeDTORes);
            result.setMessage("Lay thanh cong");
        }
        return result;
    }
    public ResponseDTO<List<ThongKeTheoNam>> ThongKeTheoNamService(String id){
        ResponseDTO<List<ThongKeTheoNam>> result = new ResponseDTO<>();
        int nam= Integer.parseInt(id);
        List<ThongKeTheoNam> thongKeTheoNamList = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            ThongKeTheoNam thongKeTheoNam =new ThongKeTheoNam();
            thongKeTheoNam.setThang(i);
            if (orderRepository.thongKeTheoNam(nam,i)==null){
                thongKeTheoNam.setTongTien(0);
            }else {
                thongKeTheoNam.setTongTien(orderRepository.thongKeTheoNam(nam,i));
            }
            thongKeTheoNamList.add(thongKeTheoNam);
        }
        result.setData(thongKeTheoNamList);
        return result;
    }
}
