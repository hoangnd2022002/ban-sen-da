package com.example.duantn.service;

import com.example.duantn.conf.ResponseDTO;
import com.example.duantn.dto.request.RoleDTOReq;
import com.example.duantn.dto.request.SearchReq;
import com.example.duantn.dto.request.UserDTOReq;
import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.dto.response.RoleDTORes;
import com.example.duantn.dto.response.UserDTORes;
import com.example.duantn.entity.Role;
import com.example.duantn.entity.User;

import java.util.List;

public interface UserService {
    List<UserDTORes> getAllUser();
    List<UserDTORes> getAllUserStatus1();
    List<UserDTORes> getAllUserStatus2();
    ResponseDTO<UserDTORes> getDetailUser(int id);
    ResponseDTO<UserDTORes> addUser(UserDTOReq userDTOReq);
    ResponseDTO<UserDTORes> editUser(UserDTOReq userDTOReq);
    void deleteUser(UserDTOReq userDTOReq);
    List<UserDTORes> getAllUserByRole(RoleDTOReq roleDTOReq);
    List<Role> getAllRole();
    List<UserDTORes> getUserByName(SearchReq searchReq);
    User findUserByUsername(String username);

    ResponseDTO<UserDTORes> changePass(UserDTOReq userDTOReq);

}
