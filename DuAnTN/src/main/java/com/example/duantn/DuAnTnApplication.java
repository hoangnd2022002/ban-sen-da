package com.example.duantn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DuAnTnApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuAnTnApplication.class, args);
    }

}
