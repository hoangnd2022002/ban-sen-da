package com.example.duantn.dto.request;

import com.example.duantn.dto.response.RoleDTORes;
import com.example.duantn.dto.response.UserDTORes;
import com.example.duantn.entity.Role;
import com.example.duantn.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTOReq {
    private int    idUser;
    private String username;
    private String oldPass;
    private String password;
    private String email;
    private Byte   sex;
    private String phone;
    private java.sql.Timestamp createdate;
    private String name;
    private RoleDTOReq role;



    @Autowired
    ModelMapper modelMapper;
    public User convertToEntity(UserDTOReq userDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        User u = new User();
        u.setRole(role.convertToEntity(userDTOReq.getRole()));
        return modelMapper.map(userDTOReq, User.class);
    }


    public UserDTORes convertRequestToResponse(UserDTOReq userDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(userDTOReq, UserDTORes.class);
    }
    public UserDTORes convertEntityToResponse(User user) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(user, UserDTORes.class);
    }
    public static RoleDTORes convertRoleEntityToRes(Role role){
        RoleDTORes roleDTORes = new RoleDTORes();
        roleDTORes.setIdRole(role.getIdRole());
        roleDTORes.setName(role.getName());
        return roleDTORes;
    }
    public static RoleDTOReq convertRoleEntityToReq(Role role){
        RoleDTOReq roleDTORes = new RoleDTOReq();
        roleDTORes.setIdRole(role.getIdRole());
        roleDTORes.setName(role.getName());
        return roleDTORes;
    }
    public static UserDTORes fromDTO(User user) {
        UserDTORes userDTORes = new UserDTORes();
        userDTORes.setUsername(user.getUsername());
        userDTORes.setEmail(user.getEmail());
        userDTORes.setSex(user.getSex());
        userDTORes.setPhone(user.getPhone());
        userDTORes.setCreatedate(user.getCreatedate());
        userDTORes.setName(user.getName());
        if (user.getRole()!=null){
            userDTORes.setRole(convertRoleEntityToRes(user.getRole()));
        }
        return userDTORes;
    }

}
