package com.example.duantn.dto.request;


import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.entity.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTOReq {
    private int               idProduct;
    private String            productName;
    private String            moTa;
    private int               status;
    private List<ProductSize> productSizeList;
    private List<Habitats> habitatsList;
    private int type;
    private int lightAspect;

    @Autowired
    ModelMapper modelMapper;

    public Product convertToEntityProduct(ProductDTOReq productDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Product product = new Product();
        return modelMapper.map(productDTO, Product.class);
    }

    public ProductSize convertToEntityProductSize(ProductDTOReq productDTO) {
        ModelMapper modelMapper = new ModelMapper();
        ProductSize productSize = new ProductSize();
        productSize.setSize(modelMapper.map(productDTO, Size.class));
        productSize.setProduct(modelMapper.map(productDTO, Product.class));
        return productSize;
    }
    public ProductDTORes convertRequestToResponse(ProductDTOReq productDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(productDTOReq, ProductDTORes.class);
    }

    public ProductDTORes convertRequestToResponse(String productDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(productDTOReq, ProductDTORes.class);
    }

    public static ProductDTORes fromDTO(Product product) {
        ProductDTORes productDTORes = new ProductDTORes();
        productDTORes.setIdProduct(product.getIdProduct());
        productDTORes.setProductName(product.getProductName());
        productDTORes.setMoTa(product.getMoTa());

        return productDTORes;
    }


}
