package com.example.duantn.dto.response;

import com.example.duantn.dto.request.RoleDTOReq;
import com.example.duantn.entity.Role;
import com.example.duantn.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTORes {
    private String stt;
    private int idUser;
    private String username;
    private String email;
    private Byte sex;
    private String phone;
    private java.sql.Timestamp createdate;
    private String name;
    private RoleDTORes role;

    @Autowired
    ModelMapper modelMapper;
    public User convertToEntity(UserDTORes userDTORes) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(userDTORes, User.class);
    }
    public UserDTORes convertToDTO(User user) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(user, UserDTORes.class);
    }
    public static RoleDTORes convertRoleEntityToRes(Role role){
        RoleDTORes roleDTORes = new RoleDTORes();
        roleDTORes.setIdRole(role.getIdRole());
        roleDTORes.setName(role.getName());
        return roleDTORes;
    }
    public static UserDTORes fromDTO(User user) {
        UserDTORes userDTORes = new UserDTORes();
        userDTORes.setIdUser(user.getIdUser());
        userDTORes.setUsername(user.getUsername());
        userDTORes.setEmail(user.getEmail());
        userDTORes.setSex(user.getSex());
        userDTORes.setPhone(user.getPhone());
        userDTORes.setCreatedate(user.getCreatedate());
        userDTORes.setName(user.getName());
        if (user.getRole()!=null){
            userDTORes.setRole(convertRoleEntityToRes(user.getRole()));
        }
        return userDTORes;
    }



}
