package com.example.duantn.dto;

import com.example.duantn.entity.OrderDetail;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class OrderDetailReport {
    private String tenHang;
    private String stt;
    private String soLuong;

    public static List<OrderDetailReport> convertDTO (List<OrderDetail> orderDetailList){
        List<OrderDetailReport> orderDetailReportList = new ArrayList<>();
        for (int i = 0; i < orderDetailList.size(); i++) {
            OrderDetailReport orderDetailReport = new OrderDetailReport();
            orderDetailReport.setSoLuong(String.valueOf(orderDetailList.get(i).getQuantity()));
            String tenSP = orderDetailList.get(i).getProductName()+" ("+orderDetailList.get(i).getColorName()+")"+" size: "+orderDetailList.get(i).getSizeName();
            orderDetailReport.setTenHang(tenSP);
            orderDetailReportList.add(orderDetailReport);
        }
        return orderDetailReportList;
    }
}
