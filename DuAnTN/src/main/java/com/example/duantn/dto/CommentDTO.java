package com.example.duantn.dto;

import com.example.duantn.dto.request.UserDTOReq;
import com.example.duantn.dto.response.ProductDTORes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentDTO {
    private Integer idComment;
    private String content;
    private java.sql.Timestamp createdate;
    private UserDTOReq user;
    private ProductDTORes product;

    public Integer getIdComment() {
        return this.idComment;
    }

    public void setIdComment(Integer idComment) {
        this.idComment = idComment;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public java.sql.Timestamp getCreatedate() {
        return this.createdate;
    }

    public void setCreatedate(java.sql.Timestamp createdate) {
        this.createdate = createdate;
    }


}
