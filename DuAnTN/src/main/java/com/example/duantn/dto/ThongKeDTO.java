package com.example.duantn.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Component
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThongKeDTO {
    private Timestamp startDate;
    private Timestamp endDate;
    private int       donDangGiao;
    private int       donDaGiao;
    private int       donDaNhan;
    private int       donDaHuy;
    private int       banTaiQuay;
    private String     doanhThu;
    private int       soLuongBan;
    private List<ThongKeTheoThangDTO> thongKeTheoThangDTOList;
}
