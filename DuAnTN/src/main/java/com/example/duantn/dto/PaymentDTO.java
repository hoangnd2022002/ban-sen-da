package com.example.duantn.dto;


import com.example.duantn.entity.OrderDetail;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PaymentDTO {
    private String amount;
    private Integer idOrder;
    private List<OrderDetail> orderDetailsList;
}
