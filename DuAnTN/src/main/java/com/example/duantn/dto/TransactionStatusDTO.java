package com.example.duantn.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionStatusDTO {
    private String status;
    private String message;
    private Object data;
}
