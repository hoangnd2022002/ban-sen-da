package com.example.duantn.dto;

import com.example.duantn.dto.request.ProductDTOReq;
import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.entity.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDTO {
    private int       idOrder;
    private int       idUser;
    private Timestamp createDate;
    private Timestamp updateDate;
    private float     totalMoney;
    private String    status;
    private String    statusMoney;
    private String    moTa;
    private String    diaChi;
    private List<OrderDetail> orderDetailsList;

    public static OrderDTO fromDTO(Orders order) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setIdOrder(order.getIdOrder());
        orderDTO.setStatus(order.getStatus());
        orderDTO.setCreateDate(order.getCreateDate());
        orderDTO.setTotalMoney(order.getTotalMoney());
        return orderDTO;
    }
    public Orders convertToEntity(OrderDTO orderDTO) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(orderDTO, Orders.class);
    }

}
