package com.example.duantn.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SanPhamDTO {
    private String stt;
    private String tenHang;
    private String soLuong;
    private String donGia;
    private String thanhTien;
}
