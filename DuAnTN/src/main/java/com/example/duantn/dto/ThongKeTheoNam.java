package com.example.duantn.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class ThongKeTheoNam {
    private int thang;
    private float tongTien;
}
