package com.example.duantn.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProSizeDTOReq {
    private int    quantity;
    private float  price;
    private int    idSize;
    private int    idProduct;
}
