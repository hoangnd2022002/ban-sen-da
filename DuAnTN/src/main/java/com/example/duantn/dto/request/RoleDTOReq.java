package com.example.duantn.dto.request;

import com.example.duantn.entity.Role;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleDTOReq {
    private int    idRole;
    private String name;

    public Role convertToEntity(RoleDTOReq roleDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(roleDTOReq, Role.class);
    }



}
