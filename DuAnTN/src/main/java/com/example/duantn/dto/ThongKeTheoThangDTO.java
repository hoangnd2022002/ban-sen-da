package com.example.duantn.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ThongKeTheoThangDTO {
    private String year;
    private String month;
    private String total;
}
