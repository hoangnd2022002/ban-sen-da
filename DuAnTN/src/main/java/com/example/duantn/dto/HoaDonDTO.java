package com.example.duantn.dto;


import com.example.duantn.entity.OrderDetail;
import com.example.duantn.entity.ProductSize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HoaDonDTO {
    private String           maHoaDon;
    private String           nguoiBanHang;
    private String           tienKhachTra;
    private List<SanPhamDTO> table1;
    private List<OrderDetailReport> table2;
}
