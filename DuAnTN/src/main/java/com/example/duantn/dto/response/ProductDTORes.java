package com.example.duantn.dto.response;


import com.example.duantn.dto.request.ProductDTOReq;
import com.example.duantn.entity.*;
import com.example.duantn.repository.HabitatRepository;
import com.example.duantn.repository.ProductSizeRepository;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTORes {
    private String stt;
    private int        idProduct;
    private String     productName;
    private Integer    soLuong;
    private Float      giaBan;
    private String     moTa;
    private List<ProductSize> productSizeList;
    private List<Habitats> habitatsList;
    private Type type;
    private LightAspect lightAspect;

    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ProductSizeRepository productSizeRepository;
    @Autowired
    HabitatRepository habitatRepository;

    public Product convertToEntity(ProductDTORes productDTORes) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(productDTORes, Product.class);
    }

    public ProductDTORes convertRequestToResponse(ProductDTOReq productDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(productDTOReq, ProductDTORes.class);
    }
    public ProductDTORes convertStringToResponse(String productDTOReq) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(productDTOReq, ProductDTORes.class);
    }



    public static ProductDTORes fromDTO(Product product) {
        ProductDTORes productDTORes = new ProductDTORes();
        productDTORes.setIdProduct(product.getIdProduct());
        productDTORes.setProductName(product.getProductName());
        productDTORes.setMoTa(product.getMoTa());
        productDTORes.setType(product.getType());
        return productDTORes;
    }

    public ProductDTORes fromDTOAddPro(Product product) {

        ProductDTORes productDTORes = new ProductDTORes();
        productDTORes.setIdProduct(product.getIdProduct());
        productDTORes.setProductName(product.getProductName());
        productDTORes.setMoTa(product.getMoTa());
        productDTORes.setType(product.getType());
        productDTORes.setLightAspect(product.getLightAspect());
        productDTORes.setProductSizeList(productSizeRepository.searchProductAndSizeStatus1TEST(product.getIdProduct()));
        productDTORes.setHabitatsList(habitatRepository.getHabitatsByProduct(product.getIdProduct()));
        return productDTORes;
    }
    public ProductDTORes fromDTOAddProtest(Product product) {

        ProductDTORes productDTORes = new ProductDTORes();
        productDTORes.setIdProduct(product.getIdProduct());
        productDTORes.setProductName(product.getProductName());
        productDTORes.setMoTa(product.getMoTa());
        productDTORes.setType(product.getType());
        productDTORes.setLightAspect(product.getLightAspect());
        productDTORes.setProductSizeList(productSizeRepository.searchProductAndSizeStatus1AnhLocal(product.getIdProduct()));
        productDTORes.setHabitatsList(habitatRepository.getHabitatsByProduct(product.getIdProduct()));
        return productDTORes;
    }
    public ProductDTORes fromDTOAddProtest2(Product product) {

        ProductDTORes productDTORes = new ProductDTORes();
        productDTORes.setIdProduct(product.getIdProduct());
        productDTORes.setProductName(product.getProductName());
        productDTORes.setMoTa(product.getMoTa());
        productDTORes.setType(product.getType());
        productDTORes.setLightAspect(product.getLightAspect());
        productDTORes.setProductSizeList(productSizeRepository.searchProductAndSizeStatus2AnhLocal(product.getIdProduct()));
        productDTORes.setHabitatsList(habitatRepository.getHabitatsByProduct(product.getIdProduct()));
        return productDTORes;
    }
    public ProductDTORes fromDTOAddProEDIT(Product product) {

        ProductDTORes productDTORes = new ProductDTORes();
        productDTORes.setIdProduct(product.getIdProduct());
        productDTORes.setProductSizeList(productSizeRepository.searchProductAndSizeStatus1TEST(product.getIdProduct()));
        return productDTORes;
    }
}
