package com.example.duantn.dto;

import com.example.duantn.dto.response.ProductDTORes;
import com.example.duantn.entity.Orders;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailDTO {
    private int idOrderDetail;
    private int quantity;
    private OrderDTO order;
    private ProductDTORes product;



}
