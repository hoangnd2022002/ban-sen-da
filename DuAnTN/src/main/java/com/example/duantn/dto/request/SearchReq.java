package com.example.duantn.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchReq {
    private String keyword;
    private int role;
    private Float keyword1;
    private Float keyword2;
    private Integer idType;
    private Integer idLightAspect;
//    private List<Integer> ListIdLight;
    private List<Integer> idHabitat;
    private Timestamp ngayBatDau;
    private Timestamp ngayKetThuc;

}
